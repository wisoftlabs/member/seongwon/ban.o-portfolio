import { useState, useEffect } from 'react'

export function useTypingEffect(lines: string[], speed: number = 50, delayBetweenLines: number = 500) {
  const [displayedLines, setDisplayedLines] = useState<string[]>(lines.map(() => ''))
  const [currentLineIndex, setCurrentLineIndex] = useState(0)
  const [currentCharIndex, setCurrentCharIndex] = useState(0)
  const [isDone, setIsDone] = useState(false)

  useEffect(() => {
    if (currentLineIndex < lines.length) {
      const currentLine = lines[currentLineIndex]
      if (currentCharIndex < currentLine.length) {
        const timeout = setTimeout(() => {
          setDisplayedLines(prev => {
            const newLines = [...prev]
            newLines[currentLineIndex] = currentLine.slice(0, currentCharIndex + 1)
            return newLines
          })
          setCurrentCharIndex(prev => prev + 1)
        }, speed)
        return () => clearTimeout(timeout)
      } else {
        const timeout = setTimeout(() => {
          setCurrentLineIndex(prev => prev + 1)
          setCurrentCharIndex(0)
        }, delayBetweenLines)
        return () => clearTimeout(timeout)
      }
    } else {
      setIsDone(true)
    }
  }, [lines, speed, delayBetweenLines, currentLineIndex, currentCharIndex])

  return { displayedLines, isDone }
}

