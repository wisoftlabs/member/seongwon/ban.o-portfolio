import { RecentPosts } from "@/components/news/recent-posts"
import { SocialFeed } from "@/components/news/social-feed"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '소식',
  description: '반오의 최신 소식과 업데이트를 확인하세요. 새로운 음악, 공연 소식, 미디어 출연 정보를 제공합니다.',
  openGraph: {
    title: '반오 (Ban.O) - 최신 소식',
    description: '반오의 최신 소식과 업데이트를 확인하세요. 새로운 음악, 공연 소식, 미디어 출연 정보를 제공합니다.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function NewsPage() {
  return (
    <div className="min-h-screen bg-gray-50">
      <div className="container mx-auto px-4 py-24">
        <h1 className="text-4xl font-bold mb-12 text-center">BanO 소식</h1>
        <RecentPosts />
        <SocialFeed />
      </div>
    </div>
  )
}
