"use client"

import { MusicProvider } from "@/components/music/MusicContext"
import { MusicPlayer } from "@/components/music/MusicPlayer"
import { usePathname } from 'next/navigation'

export default function MusicLayout({
  children,
}: {
  children: React.ReactNode
}) {
  const pathname = usePathname()
  const hidePlayer = ['/music/albums'].includes(pathname)

  return (
    <MusicProvider>
      <div className="flex flex-col min-h-screen">
        <div className="flex-1 overflow-y-auto">
          {children}
        </div>
        {!hidePlayer && <MusicPlayer />}
      </div>
    </MusicProvider>
  )
}
