import { Albums } from "@/components/music/Albums"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '앨범',
  description: '반오의 정규 앨범과 싱글 앨범을 만나보세요.',
  openGraph: {
    title: '반오 (Ban.O) - 앨범',
    description: '반오의 정규 앨범과 싱글 앨범을 만나보세요.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function AlbumsPage() {
  return (
    <div className="container py-16">
      <Albums />
    </div>
  )
}
