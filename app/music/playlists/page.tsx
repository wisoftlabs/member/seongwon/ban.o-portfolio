import { Playlists } from "@/components/music/Playlists"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '플레이리스트',
  description: '반오의 다양한 플레이리스트를 감상하세요. 직접 선별한 음악과 가사를 함께 즐길 수 있습니다.',
  openGraph: {
    title: '반오 (Ban.O) - 플레이리스트',
    description: '반오의 다양한 플레이리스트를 감상하세요. 직접 선별한 음악과 가사를 함께 즐길 수 있습니다.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function PlaylistsPage() {
  return (
    <div className="container py-16">
      <Playlists />
    </div>
  )
}
