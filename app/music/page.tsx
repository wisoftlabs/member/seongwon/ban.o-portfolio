import { MusicContent } from "@/components/music/MusicContent"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '음악',
  description: '반오의 음악을 들어보세요. 앨범, 싱글, 커버곡 등 다양한 음악을 제공합니다.',
  openGraph: {
    title: '반오 (Ban.O) - 음악',
    description: '반오의 음악을 들어보세요. 앨범, 싱글, 커버곡 등 다양한 음악을 제공합니다.',
    images: ['/images/music-preview.jpg'],
  },
}

export default function MusicPage() {
  return (
    <div className="min-h-screen bg-background">
      <MusicContent />
    </div>
  )
}
