import { Hero } from "@/components/Hero"
import { About } from "@/components/about"
import { Music } from "@/components/music"
import { Gallery } from "@/components/gallery"
import { Notice } from "@/components/notice"
import { Stats } from "@/components/stats"
import { Footer } from "@/components/footer"
import { BandMembers } from "@/components/band/BandMembers"
import dynamic from 'next/dynamic'
import type { Metadata } from 'next'

const TicketModal = dynamic(() => import('@/components/modal/TicketModal'), {
  ssr: false
})

export const metadata: Metadata = {
  title: 'Ban.O Band',
  description: '사람들의 소소한 꿈을 응원하는 반오밴드입니다. 음악, 공연, 소식을 만나보세요.',
  openGraph: {
    title: 'Ban.O Band - 반오밴드 공식 웹사이트',
    description: '사람들의 소소한 꿈을 응원하는 반오밴드입니다.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function Home() {
  return (
    <main>
      <TicketModal />
      <Hero />
      <BandMembers />
      <Stats />
      <About />
      <Music />
      <Gallery />
      <Notice />
      <Footer />
    </main>
  )
}
