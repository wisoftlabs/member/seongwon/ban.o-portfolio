import { MasonryGallery } from "@/components/gallery/masonry-gallery"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '갤러리',
  description: '반오의 공연 사진과 영상을 만나보세요. 다양한 무대와 순간들을 담은 갤러리입니다.',
  openGraph: {
    title: '반오 (Ban.O) - 갤러리',
    description: '반오의 공연 사진과 영상을 만나보세요. 다양한 무대와 순간들을 담은 갤러리입니다.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function GalleryPage() {
  return (
    <div className="min-h-screen bg-white px-4 py-12">
      <div className="container mx-auto">
        <MasonryGallery />
      </div>
    </div>
  )
}
