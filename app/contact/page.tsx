import { ContactForm } from "@/components/contact/contact-form"
import { ContactInfo } from "@/components/contact/contact-info"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '연락처',
  description: '반오에게 연락하세요. 공연 문의, 음악 협업, 기타 문의사항을 접수받고 있습니다.',
  openGraph: {
    title: '반오 (Ban.O) - 연락하기',
    description: '반오에게 연락하세요. 공연 문의, 음악 협업, 기타 문의사항을 접수받고 있습니다.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function ContactPage() {
  return (
    <div className="min-h-screen bg-white py-20">
      <div className="container mx-auto px-4">
        <div className="text-center mb-16">
          <h1 className="text-4xl font-bold mb-4">반오에게 연락하기</h1>
          <p className="text-gray-600">🎤공연 🎸협업 🎹작업 - 문의는 언제든지 환영입니다!</p>
        </div>

        <div className="grid lg:grid-cols-2 gap-8 max-w-6xl mx-auto rounded-xl overflow-hidden shadow-2xl">
          <ContactInfo />
          <ContactForm />
        </div>
      </div>
    </div>
  )
}
