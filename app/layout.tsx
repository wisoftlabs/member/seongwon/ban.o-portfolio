import './globals.css'
import { Noto_Sans_KR } from 'next/font/google'
import { Header } from "@/components/header"
import type { Metadata } from 'next'
import JsonLd from '@/components/JsonLd'
import GoogleAnalytics from '@/components/GoogleAnalytics'
import { Toaster } from "@/components/ui/toaster"

const notoSansKR = Noto_Sans_KR({ 
  subsets: ['latin'],
  variable: '--font-noto-sans-kr',
})

export const metadata: Metadata = {
  metadataBase: new URL('https://ban-o.art'),
  title: {
    template: '%s | Ban.O Band',
    default: 'Ban.O Band - 싱어송라이터 반오밴드 공식 웹사이트'  
  },
  description: '싱어송라이터 반오의 음악, 공연, 소식을 만나보세요. 앨범, 커버곡, 공연 일정 등 다양한 콘텐츠를 제공합니다.',
  keywords: ['반오', 'Ban.O', '싱어송라이터', '인디음악', '커버곡', '공연', '음악'],
  openGraph: {
    title: 'Ban.O Band - 싱어송라이터 반오밴드 공식 웹사이트',
    description: '싱어송라이터 반오의 음악, 공연, 소식을 만나보세요.',
    url: 'https://ban-o.art',
    siteName: 'Ban.O Band Official Website',
    images: [
      {
        url: '/images/bg-image.jpg',
        width: 1200,
        height: 630,
        alt: 'Ban.O',
      }
    ],
    locale: 'ko_KR',
    type: 'website',
  },
  twitter: {
    card: 'summary_large_image',
    title: 'Ban.O Band - 싱어송라이터 반오 공식 웹사이트',
    description: '싱어송라이터 반오의 음악, 공연, 소식을 만나보세요.',
    images: ['/images/bg-image.jpg'],
  },
  icons: {
    icon: [
      { url: '/favicon/favicon.ico' },
      { url: '/favicon/favicon-16x16.png', sizes: '16x16', type: 'image/png' },
      { url: '/favicon/favicon-32x32.png', sizes: '32x32', type: 'image/png' },
      { url: '/favicon/favicon-96x96.png', sizes: '96x96', type: 'image/png' }
    ],
    apple: [
      { url: '/favicon/apple-icon-57x57.png', sizes: '57x57', type: 'image/png' },
      { url: '/favicon/apple-icon-60x60.png', sizes: '60x60', type: 'image/png' },
      { url: '/favicon/apple-icon-72x72.png', sizes: '72x72', type: 'image/png' },
      { url: '/favicon/apple-icon-76x76.png', sizes: '76x76', type: 'image/png' },
      { url: '/favicon/apple-icon-114x114.png', sizes: '114x114', type: 'image/png' },
      { url: '/favicon/apple-icon-120x120.png', sizes: '120x120', type: 'image/png' },
      { url: '/favicon/apple-icon-144x144.png', sizes: '144x144', type: 'image/png' },
      { url: '/favicon/apple-icon-152x152.png', sizes: '152x152', type: 'image/png' },
      { url: '/favicon/apple-icon-180x180.png', sizes: '180x180', type: 'image/png' },
      { url: '/favicon/apple-icon.png', sizes: '192x192', type: 'image/png' }
    ],
    other: [
      { rel: 'icon', url: '/favicon/android-icon-36x36.png', sizes: '36x36', type: 'image/png' },
      { rel: 'icon', url: '/favicon/android-icon-48x48.png', sizes: '48x48', type: 'image/png' },
      { rel: 'icon', url: '/favicon/android-icon-72x72.png', sizes: '72x72', type: 'image/png' },
      { rel: 'icon', url: '/favicon/android-icon-96x96.png', sizes: '96x96', type: 'image/png' },
      { rel: 'icon', url: '/favicon/android-icon-144x144.png', sizes: '144x144', type: 'image/png' },
      { rel: 'icon', url: '/favicon/android-icon-192x192.png', sizes: '192x192', type: 'image/png' },
      { rel: 'icon', url: '/favicon/ms-icon-70x70.png', sizes: '70x70', type: 'image/png' },
      { rel: 'icon', url: '/favicon/ms-icon-144x144.png', sizes: '144x144', type: 'image/png' },
      { rel: 'icon', url: '/favicon/ms-icon-150x150.png', sizes: '150x150', type: 'image/png' },
      { rel: 'icon', url: '/favicon/ms-icon-310x310.png', sizes: '310x310', type: 'image/png' }
    ]
  },
  manifest: '/favicon/site.webmanifest',
  appleWebApp: {
    capable: true,
    statusBarStyle: 'default',
    title: 'Ban.O Band'
  },
  robots: {
    index: true,
    follow: true,
    googleBot: {
      index: true,
      follow: true,
      'max-image-preview': 'large',
      'max-snippet': -1,
    },
  },
  verification: {
    google: 'G-WF271JGQ4S',
  },
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="ko" className={`${notoSansKR.variable} font-sans scroll-smooth`}>
      <head>
        <meta name="mobile-web-app-capable" content="yes" />
        <script type="text/javascript" dangerouslySetInnerHTML={{
          __html: `
            window.NREUM||(NREUM={});NREUM.init={session_replay:{enabled:true,block_selector:'',mask_text_selector:'*',sampling_rate:100.0,error_sampling_rate:70.0,mask_all_inputs:true,collect_fonts:true,inline_images:false,inline_stylesheet:true,mask_input_options:{}},distributed_tracing:{enabled:true},privacy:{cookies_enabled:true},ajax:{deny_list:["bam.nr-data.net"]}};
            
            NREUM.loader_config={accountID:"4536734",trustKey:"4536734",agentID:"1120317428",licenseKey:"NRJS-6cdc040ea0dfc518637",applicationID:"1120317428"};
            NREUM.info={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",licenseKey:"NRJS-6cdc040ea0dfc518637",applicationID:"1120317428",sa:1};
          `
        }} />
      </head>
      <body className="bg-background text-foreground antialiased">
        <Header />
        <main className="min-h-screen">
          {children}
        </main>
        <Toaster />
        <JsonLd />
        <GoogleAnalytics />
      </body>
    </html>
  )
}
