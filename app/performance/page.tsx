import { PerformanceHero } from "@/components/performance/performance-hero"
import { PerformanceGrid } from "@/components/performance/performance-grid"
import { FloatingElements } from "@/components/performance/floating-elements"
import { PerformancePhotos } from "@/components/performance/performance-photos"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '공연',
  description: '반오의 공연 일정과 티켓 예매 정보를 확인하세요. 다가오는 공연과 지난 공연 기록을 볼 수 있습니다.',
  openGraph: {
    title: '반오 (Ban.O) - 공연 일정',
    description: '반오의 공연 일정과 티켓 예매 정보를 확인하세요. 다가오는 공연과 지난 공연 기록을 볼 수 있습니다.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function PerformancePage() {
  return (
    <main className="relative min-h-screen pt-16">
      <FloatingElements />
      <PerformanceHero />
      <div className="container mx-auto px-4">
        <div className="space-y-20">
          <PerformanceGrid title="하이라이트" />
          <PerformancePhotos />
        </div>
      </div>
    </main>
  )
}
