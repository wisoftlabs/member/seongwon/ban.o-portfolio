import { CoverSongsPage } from "./client"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '커버곡',
  description: '반오가 부른 다양한 커버곡 영상을 감상하세요.',
  openGraph: {
    title: '반오 (Ban.O) - 커버곡 영상',
    description: '반오가 부른 다양한 커버곡 영상을 감상하세요.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function CoversPage() {
  return <CoverSongsPage />
}
