'use client'

declare global {
  interface Window {
    YT: any;
    onYouTubeIframeAPIReady: () => void;
  }
}

import { CoverSongs } from "@/components/music/CoverSongs"

export function CoverSongsPage() {
  return (
    <div className="container py-8">
      <CoverSongs />
    </div>
  )
}
