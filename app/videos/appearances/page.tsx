import { AppearanceVideos } from "@/components/music/AppearanceVideos"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '출연 영상',
  description: '반오의 다양한 방송 출연과 인터뷰 영상을 만나보세요.',
  openGraph: {
    title: '반오 (Ban.O) - 출연 영상',
    description: '반오의 다양한 방송 출연과 인터뷰 영상을 만나보세요.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function AppearancesPage() {
  return (
    <div className="container py-8">
      <AppearanceVideos />
    </div>
  )
}
