import { ShortsVideos } from "@/components/music/ShortsVideos"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '쇼츠',
  description: '반오의 일상과 음악 이야기를 담은 짧은 영상을 만나보세요.',
  openGraph: {
    title: '반오 (Ban.O) - 쇼츠 영상',
    description: '반오의 일상과 음악 이야기를 담은 짧은 영상을 만나보세요.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function ShortsPage() {
  return (
    <div className="container py-12">
      <ShortsVideos />
    </div>
  )
}
