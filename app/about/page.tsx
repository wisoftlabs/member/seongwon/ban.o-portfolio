import { AboutHero } from "@/components/about/about-hero"
import { AboutBio } from "@/components/about/about-bio"
import { AboutCareer } from "@/components/about/about-career"
import { AboutDiscography } from "@/components/about/about-discography"
import { AboutPersonal } from "@/components/about/about-personal"
import { SocialLinks } from "@/components/about/social-links"
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: '소개',
  description: '싱어송라이터 반오를 소개합니다. 반오의 음악 이야기와 아티스트로서의 여정을 만나보세요.',
  openGraph: {
    title: '반오 (Ban.O) - 아티스트 소개',
    description: '싱어송라이터 반오를 소개합니다. 반오의 음악 이야기와 아티스트로서의 여정을 만나보세요.',
    images: ['/images/bg-image.jpg'],
  },
}

export default function AboutPage() {
  return (
    <div className="min-h-screen bg-white">
      <div className="container mx-auto px-4 py-12">
        <AboutHero />
        <AboutBio />
        <AboutCareer />
        <AboutDiscography />
        <AboutPersonal />
        <SocialLinks />
      </div>
    </div>
  )
}
