import { NextResponse } from 'next/server'

const DEMO_SHORTS = [
  {
    id: "1",
    title: "반오 - 꽃길꽃신 #shorts",
    thumbnail: "https://i.ytimg.com/vi/M0gMdY3r1-8/hq720.jpg",
    videoId: "M0gMdY3r1-8",
    views: "1.2K"
  },
  {
    id: "2",
    title: "반오 - 먼지 쌓인 나의 앨범 속에는 #shorts",
    thumbnail: "https://i.ytimg.com/vi/ktOZqXSfUIM/hq720.jpg",
    videoId: "ktOZqXSfUIM",
    views: "1.1K"
  },
  {
    id: "3",
    title: "반오 - 알파카월드 #shorts",
    thumbnail: "https://i.ytimg.com/vi/qLEYKCntigE/hq720.jpg",
    videoId: "qLEYKCntigE",
    views: "980"
  },
  {
    id: "4",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/Vlbi_vjM4WY/hq720.jpg",
    videoId: "Vlbi_vjM4WY",
    views: "876"
  },
  {
    id: "5",
    title: "반오 라이브 #shorts",
    thumbnail: "https://i.ytimg.com/vi/W-iFcmzVwXM/hq720.jpg",
    videoId: "W-iFcmzVwXM",
    views: "923"
  },
  {
    id: "6",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/wcwohveg3m0/hq720.jpg",
    videoId: "wcwohveg3m0",
    views: "845"
  },
  {
    id: "7",
    title: "반오 - 라이브 클립 #shorts",
    thumbnail: "https://i.ytimg.com/vi/Mp8QF7ncCFk/hq720.jpg",
    videoId: "Mp8QF7ncCFk",
    views: "789"
  },
  {
    id: "8",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/4ClIYTAbeL0/hq720.jpg",
    videoId: "4ClIYTAbeL0",
    views: "912"
  },
  {
    id: "9",
    title: "반오 - 비하인드 #shorts",
    thumbnail: "https://i.ytimg.com/vi/pfO3NWNiDps/hq720.jpg",
    videoId: "pfO3NWNiDps",
    views: "867"
  },
  {
    id: "10",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/YRE5VNvQV3g/hq720.jpg",
    videoId: "YRE5VNvQV3g",
    views: "934"
  },
  {
    id: "11",
    title: "반오 - 라이브 클립 #shorts",
    thumbnail: "https://i.ytimg.com/vi/P9WMad7h5jU/hq720.jpg",
    videoId: "P9WMad7h5jU",
    views: "856"
  },
  {
    id: "12",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/OCTs3Z23j9k/hq720.jpg",
    videoId: "OCTs3Z23j9k",
    views: "878"
  },
  {
    id: "13",
    title: "반오 - 비하인드 #shorts",
    thumbnail: "https://i.ytimg.com/vi/tHH6eq1BVio/hq720.jpg",
    videoId: "tHH6eq1BVio",
    views: "945"
  },
  {
    id: "14",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/U5unQQi0MxE/hq720.jpg",
    videoId: "U5unQQi0MxE",
    views: "867"
  },
  {
    id: "15",
    title: "반오 - 라이브 클립 #shorts",
    thumbnail: "https://i.ytimg.com/vi/KEjdHWW4pVc/hq720.jpg",
    videoId: "KEjdHWW4pVc",
    views: "923"
  },
  {
    id: "16",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/9T4-XWL6clg/hq720.jpg",
    videoId: "9T4-XWL6clg",
    views: "889"
  },
  {
    id: "17",
    title: "반오 - 비하인드 #shorts",
    thumbnail: "https://i.ytimg.com/vi/RvOaSIs9eU0/hq720.jpg",
    videoId: "RvOaSIs9eU0",
    views: "912"
  },
  {
    id: "18",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/sXdE1mRQZkQ/hq720.jpg",
    videoId: "sXdE1mRQZkQ",
    views: "845"
  },
  {
    id: "19",
    title: "반오 - 라이브 클립 #shorts",
    thumbnail: "https://i.ytimg.com/vi/h6cpRnnj6j8/hq720.jpg",
    videoId: "h6cpRnnj6j8",
    views: "934"
  },
  {
    id: "20",
    title: "반오 커버 #shorts",
    thumbnail: "https://i.ytimg.com/vi/YBY4tJotPUI/hq720.jpg",
    videoId: "YBY4tJotPUI",
    views: "878"
  }
]

export async function GET() {
  try {
    return NextResponse.json({ shorts: DEMO_SHORTS })
  } catch {
    return NextResponse.json(
      { message: 'Failed to fetch YouTube shorts' },
      { status: 500 }
    )
  }
}
