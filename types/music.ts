export interface Track {
  id: string
  title: string
  youtubeId: string
  thumbnail: string
  duration: string
  likes: number
  artist: string
}

export interface Album {
  id: string
  title: string
  coverImage: string
  releaseDate: string
  description: string
  tracks: Track[]
}

