import { SESClient, SendEmailCommand } from '@aws-sdk/client-ses';

const ses = new SESClient({ region: 'ap-northeast-2' }); // Seoul region

const ALLOWED_ORIGINS = [
  'http://localhost:3000', 
  'http://127.0.0.1:3000', 
  'https://demo.ban-o.art', 
  'https://ban-o.art'
];

export const handler = async (event) => {
  // 로깅을 위해 전체 이벤트 출력
  console.log('Received event:', JSON.stringify(event, null, 2));

  const origin = event.headers.origin || '';
  const isOriginAllowed = ALLOWED_ORIGINS.includes(origin);
  const allowedOrigin = isOriginAllowed ? origin : ALLOWED_ORIGINS[0];

  // CORS preflight 요청 처리
  if (event.httpMethod === 'OPTIONS') {
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': allowedOrigin,
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
        'Access-Control-Allow-Methods': 'POST,OPTIONS',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message: 'Successful preflight call.' }),
    };
  }

  try {
    // API Gateway의 body 구조 처리
    let formData;
    if (event.body) {
      // body가 문자열로 인코딩되어 있을 수 있음
      formData = typeof event.body === 'string' 
        ? JSON.parse(event.body) 
        : event.body;
    } else {
      return {
        statusCode: 400,
        body: JSON.stringify({ message: 'No body provided' }),
        headers: {
          'Access-Control-Allow-Origin': allowedOrigin,
          'Access-Control-Allow-Credentials': 'true',
          'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
          'Access-Control-Allow-Methods': 'POST,OPTIONS',
          'Content-Type': 'application/json',
        },
      };
    }

    // 필수 필드 검증
    if (!formData.name || !formData.email || !formData.message) {
      return {
        statusCode: 400,
        body: JSON.stringify({ 
          message: 'Name, email, and message are required',
          receivedData: formData 
        }),
        headers: {
          'Access-Control-Allow-Origin': allowedOrigin,
          'Access-Control-Allow-Credentials': 'true',
          'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
          'Access-Control-Allow-Methods': 'POST,OPTIONS',
          'Content-Type': 'application/json',
        },
      };
    }

    // 이메일 내용 구성
    const emailParams = {
      Destination: {
        ToAddresses: ['judo0179@gmail.com'],
      },
      Message: {
        Body: {
          Text: {
            Data: `
문의 유형: ${formData.subject || '일반문의'}
이름: ${formData.name}
이메일: ${formData.email}
전화번호: ${formData.phone || '제공되지 않음'}

메시지:
${formData.message}
            `,
          },
        },
        Subject: {
          Data: `새로운 문의: ${formData.name}님의 ${formData.subject || '일반문의'}`,
        },
      },
      Source: 'judo0179@gmail.com',
    };

    // 이메일 발송
    await ses.send(new SendEmailCommand(emailParams));

    return {
      statusCode: 200,
      body: JSON.stringify({ 
        message: 'Message sent successfully',
        details: {
          name: formData.name,
          email: formData.email
        }
      }),
      headers: {
        'Access-Control-Allow-Origin': allowedOrigin,
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
        'Access-Control-Allow-Methods': 'POST,OPTIONS',
        'Content-Type': 'application/json',
      },
    };
  } catch (error) {
    console.error('Error processing request:', error);

    return {
      statusCode: 500,
      body: JSON.stringify({ 
        message: 'Error sending message',
        error: error.toString()
      }),
      headers: {
        'Access-Control-Allow-Origin': allowedOrigin,
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
        'Access-Control-Allow-Methods': 'POST,OPTIONS',
        'Content-Type': 'application/json',
      },
    };
  }
};
