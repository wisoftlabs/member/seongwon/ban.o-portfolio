import { Instagram, Youtube, Music, Headphones } from 'lucide-react'
import Link from "next/link"

export function Footer() {
  return (
    <footer className="bg-gray-100 text-gray-600 py-12 border-t border-gray-200">
      <div className="container px-4">
        <div className="grid gap-8 md:grid-cols-2 lg:grid-cols-4">
          <div>
            <h3 className="text-lg font-bold mb-4 text-gray-900">BanO</h3>
            <p className="text-gray-600">
              사람들의 소소한 꿈을 응원합니다. 
            </p>
          </div>
          <div>
            <h3 className="text-lg font-bold mb-4 text-gray-900">Links</h3>
            <ul className="space-y-2">
              <li>
                <Link href="#home" className="hover:text-gray-900 transition-colors">
                  홈
                </Link>
              </li>
              <li>
                <Link href="#about" className="hover:text-gray-900 transition-colors">
                  소개
                </Link>
              </li>
              <li>
                <Link href="#music" className="hover:text-gray-900 transition-colors">
                  음악
                </Link>
              </li>
              <li>
                <Link href="#schedule" className="hover:text-gray-900 transition-colors">
                  공연 일정
                </Link>
              </li>
            </ul>
          </div>
          <div>
            <h3 className="text-lg font-bold mb-4 text-gray-900">Social</h3>
            <div className="flex space-x-4">
              <Link href="https://www.instagram.com/_.ban.o_/" target="_blank" rel="noopener noreferrer" className="hover:text-gray-900 transition-colors">
                <Instagram className="h-6 w-6" />
                <span className="sr-only">Instagram</span>
              </Link>
              <Link href="https://www.youtube.com/@ban.0bob0227" target="_blank" rel="noopener noreferrer" className="hover:text-gray-900 transition-colors">
                <Youtube className="h-6 w-6" />
                <span className="sr-only">YouTube</span>
              </Link>
              <Link href="https://open.spotify.com/artist/5pDSAehQ62k7sKHtZoXemq" target="_blank" rel="noopener noreferrer" className="hover:text-gray-900 transition-colors">
                <Music className="h-6 w-6" />
                <span className="sr-only">Spotify</span>
              </Link>
              <Link href="https://music.apple.com/kr/artist/%EB%B0%98%EC%98%A4/1623132013" target="_blank" rel="noopener noreferrer" className="hover:text-gray-900 transition-colors">
                <Headphones className="h-6 w-6" />
                <span className="sr-only">Apple Music</span>
              </Link>
            </div>
          </div>
          <div>
            <h3 className="text-lg font-bold mb-4 text-gray-900">Contact</h3>
            <p className="text-gray-600">
              eag950209@naver.com
            </p>
          </div>
        </div>
        <div className="border-t border-gray-200 mt-12 pt-8 text-center text-gray-500 text-sm">
          <p>&copy; {new Date().getFullYear()} BanO. All rights reserved.</p>
        </div>
      </div>
    </footer>
  )
}

