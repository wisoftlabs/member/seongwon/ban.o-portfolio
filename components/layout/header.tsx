import Link from "next/link"
import { Instagram, Twitter } from 'lucide-react'

export function Header() {
  return (
    <header className="fixed top-0 left-0 right-0 z-40 bg-white/80 backdrop-blur-sm">
      <div className="container mx-auto px-4">
        <div className="flex items-center justify-between h-16">
          <Link href="/" className="text-xl font-bold">
            BanO
          </Link>
          
          <nav className="flex items-center space-x-8">
            <Link href="/blog" className="text-sm hover:opacity-75 transition-opacity">
              Blog
            </Link>
            <Link href="/about" className="text-sm hover:opacity-75 transition-opacity">
              About
            </Link>
            <Link href="/gallery" className="text-sm hover:opacity-75 transition-opacity">
              Gallery
            </Link>
            <Link href="/contact" className="text-sm hover:opacity-75 transition-opacity">
              Contact
            </Link>
          </nav>

          <div className="flex items-center space-x-4">
            <a
              href="https://instagram.com/ban.o_official"
              target="_blank"
              rel="noopener noreferrer"
              className="hover:opacity-75 transition-opacity"
            >
              <Instagram size={20} />
            </a>
            <a
              href="https://twitter.com/ban_o_official"
              target="_blank"
              rel="noopener noreferrer"
              className="hover:opacity-75 transition-opacity"
            >
              <Twitter size={20} />
            </a>
          </div>
        </div>
      </div>
    </header>
  )
}

