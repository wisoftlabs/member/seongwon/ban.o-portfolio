"use client"

import { useRef, useEffect, useState, useCallback } from 'react'
import { Play, Pause, SkipBack, SkipForward, Volume2 } from 'lucide-react'
import { Button } from "@/components/ui/button"
import { Slider } from "@/components/ui/slider"
import { useMusicPlayer } from "./MusicContext"
import Image from "next/image"
import dynamic from 'next/dynamic'
import type { YouTubeProps } from 'react-youtube'
import React from 'react'

interface YouTubeEvent {
  data: number;
  target: {
    getCurrentTime: () => Promise<number>;
    getDuration: () => Promise<number>;
    playVideo: () => void;
    pauseVideo: () => void;
    seekTo: (time: number, allowSeekAhead: boolean) => void;
    setVolume: (volume: number) => void;
  };
}

const LoadingSpinner = () => (
  <div className="flex items-center justify-center w-full h-full">
    <div className="animate-spin rounded-full h-8 w-8 border-b-2 border-gray-900"></div>
  </div>
);

const YouTubePlayer = dynamic<YouTubeProps>(
  () => import('react-youtube').then((mod) => mod.default as React.ComponentType<YouTubeProps>),
  {
    ssr: false,
    loading: LoadingSpinner,
  }
)

// 시간 문자열(MM:SS)을 초 단위로 변환하는 함수
const timeToSeconds = (timeStr: string): number => {
  const [minutes, seconds] = timeStr.split(':').map(Number)
  return minutes * 60 + seconds
}

// 초를 MM:SS 형식으로 변환하는 함수
const formatTime = (seconds: number): string => {
  const mins = Math.floor(seconds / 60)
  const secs = Math.floor(seconds % 60)
  return `${mins}:${secs.toString().padStart(2, '0')}`
}

export function MusicPlayer() {
  const { 
    currentTrack, 
    isPlaying, 
    setIsPlaying,
    isPlaylistMode,
    handleNext,
    handlePrevious
  } = useMusicPlayer()
  const [player, setPlayer] = useState<YouTubeEvent['target'] | null>(null)
  const [currentTime, setCurrentTime] = useState(0)
  const [duration, setDuration] = useState(0)
  const [isPlayerReady, setIsPlayerReady] = useState(false)
  const [hasUserInteraction, setHasUserInteraction] = useState(false)
  const timeUpdateIntervalRef = useRef<NodeJS.Timeout | null>(null)
  const playerRef = useRef<HTMLDivElement>(null)
  const [volume, setVolume] = useState(100)
  const [isMobile] = useState(() => /iPhone|iPad|iPod|Android/i.test(typeof navigator !== 'undefined' ? navigator.userAgent : ''))

  useEffect(() => {
    if (!currentTrack) return
    // 현재 트랙이 변경될 때 duration 업데이트
    setDuration(timeToSeconds(currentTrack.duration))
  }, [currentTrack])

  const handlePlayerReady = async (event: YouTubeEvent) => {
    setPlayer(event.target)
    setIsPlayerReady(true)
    const duration = await event.target.getDuration()
    setDuration(duration)
    
    if (isMobile) {
      // 모바일에서는 사용자가 명시적으로 재생 버튼을 눌러야 함
      setIsPlaying(false)
    } else {
      // 데스크톱에서는 기존 로직 유지
      if (isPlaying) {
        await event.target.playVideo()
      }
    }
  }

  useEffect(() => {
    if (!player) return

    timeUpdateIntervalRef.current = setInterval(async () => {
      if (player) {
        const currentTime = await player.getCurrentTime()
        setCurrentTime(currentTime)
      }
    }, 1000)

    return () => {
      if (timeUpdateIntervalRef.current) {
        clearInterval(timeUpdateIntervalRef.current)
      }
    }
  }, [player])

  useEffect(() => {
    if (!player || !currentTrack || !isPlayerReady) return

    const loadVideo = async () => {
      try {
        if (isPlaying) {
          await player.playVideo()
        } else {
          await player.pauseVideo()
        }
      } catch (error) {
        console.error('Error loading video:', error)
      }
    }
    loadVideo()
  }, [player, currentTrack, isPlaying, isPlayerReady])

  const onStateChange = async (event: YouTubeEvent) => {
    if (event.data === 1) { // YT.PlayerState.PLAYING
      setIsPlaying(true)
      // 재생이 시작될 때 전체 길이를 업데이트합니다
      const duration = await event.target.getDuration()
      setDuration(duration)
    } else if (event.data === 2) { // YT.PlayerState.PAUSED
      setIsPlaying(false)
    } else if (event.data === 0) { // YT.PlayerState.ENDED
      setIsPlaying(false)
      setCurrentTime(0)
    }
  }

  // 재생바 이동 핸들러
  const handleSeek = async (value: number[]) => {
    if (player && isPlayerReady) {
      const newTime = value[0]
      await player.seekTo(newTime, true)
      setCurrentTime(newTime)
    }
  }

  const togglePlay = () => {
    if (player) {
      if (!hasUserInteraction && isMobile) {
        setHasUserInteraction(true)
      }
      
      if (isPlaying) {
        player.pauseVideo()
      } else {
        player.playVideo()
      }
      setIsPlaying(!isPlaying)
    }
  }

  const handleVolumeChange = (value: number[]) => {
    const newVolume = value[0]
    if (player && player.setVolume) {
      player.setVolume(newVolume)
      setVolume(newVolume)
    }
  }

  return (
    <div className="fixed bottom-0 left-0 right-0 bg-background border-t">
      <div className="container mx-auto px-4 py-2">
        <div className="flex items-center justify-between gap-4">
          {/* 현재 재생 중인 트랙 정보 */}
          <div className="flex items-center gap-3 flex-1 min-w-0">
            {currentTrack && (
              <>
                <div className="relative w-12 h-12 flex-shrink-0">
                  <Image
                    src={currentTrack.thumbnail}
                    alt={currentTrack.title}
                    width={48}
                    height={48}
                    className="rounded"
                    style={{ objectFit: 'cover' }}
                  />
                </div>
                <div className="min-w-0">
                  <div className="font-medium truncate">{currentTrack.title}</div>
                  <div className="text-sm text-muted-foreground truncate">
                    {currentTrack.artist}
                  </div>
                </div>
              </>
            )}
          </div>

          {/* 재생 컨트롤 */}
          <div className="flex flex-col items-center gap-2 flex-1">
            <div className="flex items-center gap-4">
              <Button
                variant="ghost"
                size="icon"
                onClick={handlePrevious}
                disabled={!currentTrack || !isPlaylistMode}
              >
                <SkipBack className="h-5 w-5" />
              </Button>
              <Button
                variant="ghost"
                size="icon"
                onClick={togglePlay}
                disabled={!isPlayerReady}
              >
                {isPlaying ? (
                  <Pause className="h-5 w-5" />
                ) : (
                  <Play className="h-5 w-5" />
                )}
              </Button>
              <Button
                variant="ghost"
                size="icon"
                onClick={handleNext}
                disabled={!currentTrack || !isPlaylistMode}
              >
                <SkipForward className="h-5 w-5" />
              </Button>
            </div>
            <div className="flex items-center gap-2 w-full max-w-md">
              <span className="text-sm tabular-nums">
                {formatTime(currentTime)}
              </span>
              <Slider
                value={[currentTime]}
                max={duration}
                step={1}
                onValueChange={handleSeek}
                className="flex-1"
                disabled={!isPlayerReady}
              />
              <span className="text-sm tabular-nums">
                {formatTime(duration)}
              </span>
            </div>
          </div>

          {/* 볼륨 컨트롤 */}
          <div className="flex items-center gap-2 flex-1 justify-end">
            <Volume2 className="h-5 w-5" />
            <Slider
              value={[volume]}
              max={100}
              step={1}
              onValueChange={handleVolumeChange}
              className="w-28"
            />
          </div>
        </div>
      </div>
      <div ref={playerRef} className="hidden">
        {currentTrack && (
          <YouTubePlayer
            videoId={currentTrack.youtubeId}
            opts={{
              width: '100%',
              height: '100%',
              playerVars: {
                autoplay: isPlaying ? 1 : 0,
                controls: 0,
                disablekb: 1,
                fs: 0,
                modestbranding: 1,
                rel: 0,
              },
            }}
            onReady={handlePlayerReady}
            onStateChange={onStateChange}
            onEnd={handleNext}
          />
        )}
      </div>
    </div>
  )
}
