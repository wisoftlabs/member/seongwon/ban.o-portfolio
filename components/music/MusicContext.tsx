"use client"

import { createContext, useState, ReactNode, useContext } from 'react'
import { playlistTracks } from './Playlists'

export interface Track {
  id: string
  title: string
  youtubeId: string
  thumbnail: string
  duration: string
  likes: number
  artist: string
}

interface MusicContextType {
  currentTrack: Track | null
  setCurrentTrack: (track: Track | null) => void
  isPlaying: boolean
  setIsPlaying: (playing: boolean) => void
  isPlaylistMode: boolean
  setIsPlaylistMode: (mode: boolean) => void
  handleNext: () => void
  handlePrevious: () => void
}

const MusicContext = createContext<MusicContextType>({
  currentTrack: null,
  setCurrentTrack: () => {},
  isPlaying: false,
  setIsPlaying: () => {},
  isPlaylistMode: false,
  setIsPlaylistMode: () => {},
  handleNext: () => {},
  handlePrevious: () => {},
})

export function MusicProvider({ children }: { children: ReactNode }) {
  const [currentTrack, setCurrentTrack] = useState<Track | null>(null)
  const [isPlaying, setIsPlaying] = useState(false)
  const [isPlaylistMode, setIsPlaylistMode] = useState(false)

  const handleNext = () => {
    if (!currentTrack) return
    
    // playlistTracks는 Playlists.tsx에서 가져와야 합니다
    const currentIndex = playlistTracks.findIndex(track => track.id === currentTrack.id)
    if (currentIndex === -1) return
    
    const nextIndex = (currentIndex + 1) % playlistTracks.length
    setCurrentTrack(playlistTracks[nextIndex])
    setIsPlaying(true)
  }

  const handlePrevious = () => {
    if (!currentTrack) return
    
    const currentIndex = playlistTracks.findIndex(track => track.id === currentTrack.id)
    if (currentIndex === -1) return
    
    const previousIndex = (currentIndex - 1 + playlistTracks.length) % playlistTracks.length
    setCurrentTrack(playlistTracks[previousIndex])
    setIsPlaying(true)
  }

  return (
    <MusicContext.Provider 
      value={{ 
        currentTrack, 
        setCurrentTrack, 
        isPlaying, 
        setIsPlaying,
        isPlaylistMode,
        setIsPlaylistMode,
        handleNext,
        handlePrevious
      }}
    >
      {children}
    </MusicContext.Provider>
  )
}

export function useMusicPlayer() {
  const context = useContext(MusicContext)
  if (!context) {
    throw new Error('useMusicPlayer must be used within a MusicProvider')
  }
  return context
}
