"use client"

import { TopMusic } from "./TopMusic"
import { PopularTracks } from "./PopularTracks"
import { RecommendedVideos } from "./RecommendedVideos"

export function MusicContent() {
  return (
    <div className="container mx-auto px-4 py-8 pb-32 space-y-12">
      <TopMusic />
      <PopularTracks />
      <RecommendedVideos />
    </div>
  )
}
