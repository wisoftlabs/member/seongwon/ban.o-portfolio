"use client"

import { useState } from "react"
import { ScrollArea } from "@/components/ui/scroll-area"
import { motion, AnimatePresence } from "framer-motion"
import { X } from 'lucide-react'
import Image from "next/image"

const appearanceVideos = [
  {
    id: "ep1",
    title: "저..만복인데요ㅣ음악복덕방 ep.1",
    description: "반오, 반하영, 박진오 3명의 싱어송라이터들의 시작으로...",
    thumbnail: "https://img.youtube.com/vi/imtELFMYiY0/maxresdefault.jpg",
    videoId: "imtELFMYiY0",
    category: "음악복덕방"
  },
  {
    id: "ep2",
    title: "이 노래 내가 하면 안 돼?ㅣ음악복덕방 ep.2",
    description: "음악복덕방 두 번째 이야기",
    thumbnail: "https://img.youtube.com/vi/9_z8bd1F-M0/maxresdefault.jpg",
    videoId: "9_z8bd1F-M0",
    category: "음악복덕방"
  },
  {
    id: "ep3",
    title: "곡 완성했습니다ㅣ음악복덕방 ep.3",
    description: "음악복덕방 마지막 이야기",
    thumbnail: "https://img.youtube.com/vi/T6eQXm6alWA/maxresdefault.jpg",
    videoId: "T6eQXm6alWA",
    category: "음악복덕방"
  },
  {
    id: "mv1",
    title: "[MV] 반하오 _ 먼지 쌓인 나의 앨범 속에는",
    description: "반오의 첫 번째 뮤직비디오",
    thumbnail: "https://img.youtube.com/vi/P0XmhkWSqRQ/maxresdefault.jpg",
    videoId: "P0XmhkWSqRQ",
    category: "뮤직비디오"
  },
  {
    id: "cover1",
    title: "김광석 - 바람이 불어오는 곳 (cover)",
    description: "반오가 부르는 김광석의 명곡",
    thumbnail: "https://img.youtube.com/vi/2fTEgvb3hlg/maxresdefault.jpg",
    videoId: "2fTEgvb3hlg",
    category: "커버"
  },
  {
    id: "live1",
    title: "반오 (Ban.O)-먼지 쌓인 나의 앨범 속에는",
    description: "라이브 버전으로 듣는 반오의 신곡",
    thumbnail: "https://img.youtube.com/vi/yZyyo0OOiuw/maxresdefault.jpg",
    videoId: "yZyyo0OOiuw",
    category: "라이브"
  }
]

export function AppearanceVideos() {
  const [selectedVideo, setSelectedVideo] = useState<string | null>(null)
  const [activeCategory, setActiveCategory] = useState<string>("전체")

  const categories = ["전체", ...Array.from(new Set(appearanceVideos.map(video => video.category)))]
  
  const filteredVideos = activeCategory === "전체" 
    ? appearanceVideos 
    : appearanceVideos.filter(video => video.category === activeCategory)

  return (
    <div className="space-y-6">
      <div className="flex justify-between items-center">
        <h2 className="text-2xl font-bold">출연 영상</h2>
        <div className="flex gap-2">
          {categories.map((category) => (
            <button
              key={category}
              onClick={() => setActiveCategory(category)}
              className={`px-3 py-1.5 rounded-full text-sm transition-colors ${
                activeCategory === category
                  ? "bg-primary text-primary-foreground"
                  : "bg-secondary hover:bg-secondary/80"
              }`}
            >
              {category}
            </button>
          ))}
        </div>
      </div>

      <ScrollArea className="h-[calc(100vh-100px)]">
        <div className="grid grid-cols-1 md:grid-cols-2 gap-6 pb-6">
          {filteredVideos.map((video, index) => (
            <motion.div
              key={video.id}
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: index * 0.1 }}
              className="group relative aspect-video rounded-xl overflow-hidden cursor-pointer"
              onClick={() => setSelectedVideo(video.videoId)}
            >
              <Image
                src={video.thumbnail}
                alt={video.title}
                fill
                className="object-cover transition-transform duration-300 group-hover:scale-105"
                sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
                priority={index === 0}
              />
              <div className="absolute inset-0 bg-black/60 opacity-0 group-hover:opacity-100 transition-opacity">
                <div className="absolute inset-0 p-6 flex flex-col justify-between">
                  <div>
                    <span className="inline-block px-2 py-1 bg-primary/80 rounded-full text-xs text-white mb-2">
                      {video.category}
                    </span>
                    <h3 className="text-lg font-bold text-white mb-2">{video.title}</h3>
                    <p className="text-sm text-white/80">{video.description}</p>
                  </div>
                </div>
              </div>
            </motion.div>
          ))}
        </div>
      </ScrollArea>

      <AnimatePresence>
        {selectedVideo && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            className="fixed inset-0 z-50 flex items-center justify-center bg-black/90 p-4"
          >
            <button
              onClick={() => setSelectedVideo(null)}
              className="absolute top-4 right-4 text-white hover:text-white/80 transition-colors"
            >
              <X className="h-6 w-6" />
            </button>
            <div className="relative w-full max-w-4xl aspect-video">
              <iframe
                src={`https://www.youtube.com/embed/${selectedVideo}`}
                title="YouTube video player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
                className="absolute inset-0 w-full h-full rounded-lg"
              />
            </div>
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  )
}
