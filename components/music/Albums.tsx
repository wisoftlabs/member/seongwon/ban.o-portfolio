"use client"

import { useState, useEffect } from "react"
import Image from "next/image"
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import { Badge } from "@/components/ui/badge"
import { motion } from "framer-motion"

const albums = [
{
  id: "0",
  title: "하나의 별똥별 처럼",
  artist: "Ban.O Band",
  coverImage: "https://cdn.ban-o.art/public/music/481924331_17996434796772524_343836101851192975_n.jpg",
  releaseDate: "2025-02-26",
  type: "싱글",
  genre: ["락/메탈"],
  style: "모던 락",
  distributor: "오감엔터테인먼트",
  agency: "반오밴드",
  totalDuration: "04:33",
  description: "하나의 별똥별이 되어 너의 소원을 이뤄줄게..",
  credits: {
    producer: "반오",
    vocals: "반오",
    electricGuitar: "김현수, 박우재",
    acousticGuitar: "반오",
    piano: "송인혁, 안경섭",
    pad: "송인혁, 안경섭",
    lead: "송인혁, 안경섭",
    fx: "송인혁",
    bass: "김윤호",
    drums: "조정현",
    chorus: "송인혁, 심화경",
    composition: "반오",
    lyrics: "반오",
    arrangement: "반오, 송인혁",
    vocalDirection: "송인혁",
    mixing: "김도현",
    mastering: "남승원",
    recording: "안수민"
  },
  tracks: [
    {
      id: "0-1",
      title: "하나의 별똥별 처럼",
      duration: "4:33",
      isTitle: true,
      artist: "반오밴드(Ban.O Band)",
      youtubeId: "X7UolnJ-wyo",
      thumbnail: "https://cdn.ban-o.art/public/music/481924331_17996434796772524_343836101851192975_n.jpg",
      likes: 0
    }
  ]
},
{
  id: "1",
  title: "꽃길 꽃신",
  artist: "Ban.O",
  coverImage: "https://cdnimg.melon.co.kr/cm2/album/images/114/99/595/11499595_20240528170547_1000.jpg",
  releaseDate: "2024-05-29",
  type: "싱글",
  genre: ["인디", "락/메탈"],
  style: "인디 락",
  distributor: "오감엔터테인먼트",
  agency: "반오",
  totalDuration: "08:26",
  description: "제 노래가 꽃신이 되어 여러분들을 꽃길로 모시겠습니다.\n모든 사람들이 꽃길을 걷는 날까지 노래하는 반오가 되어 드릴게요.",
  credits: {
    producer: "반오 안수민",
    vocals: "반오",
    chorus: "조은세",
    electricGuitar: "박우재",
    piano: "송인혁",
    bass: "원준일",
    drumProgramming: "안수민",
    composition: "반오",
    lyrics: "반오,현진",
    arrangement: "송인혁",
    mixing: "안수민",
    mastering: "starry sound",
    recording: "안수민"
  },
  tracks: [
    {
      id: "1-1",
      title: "꽃길 꽃신",
      duration: "2:49",
      isTitle: true,
      artist: "반오(Ban.O)",
      youtubeId: "ZIEQfv7snIw",
      thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/99/595/11499595_20240528170547_1000.jpg",
      likes: 0
    },
    {
      id: "1-2",
      title: "꽃길 꽃신 (Acoustic Ver.)",
      duration: "2:49",
      artist: "반오(Ban.O)",
      youtubeId: "5WjZqfedfBc",
      thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/99/595/11499595_20240528170547_1000.jpg",
      likes: 0
    },
    {
      id: "1-3",
      title: "꽃길 꽃신 (Inst.)",
      duration: "2:48",
      artist: "반오(Ban.O)",
      youtubeId: "ZIEQfv7snIw",
      thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/99/595/11499595_20240528170547_1000.jpg",
      likes: 0
    }
  ]
},
{
  id: "2",
  title: "먼지 쌓인 나의 앨범 속에는",
  artist: "Ban.O",
  coverImage: "https://cdn.ban-o.art/public/music/11472610_20240424134200_1000.jpg",
  releaseDate: "2024-04-25",
  type: "싱글",
  genre: ["록/메탈"],
  style: "록",
  distributor: "오감엔터테인먼트",
  agency: "반오",
  totalDuration: "06:36",
  description: "어릴 적 꾸던 화려하고 멋있던 꿈들은 흐르는 시간 속에 잊혀가고 결국 남들과 다르지 않은 하루하루를 보내곤 합니다. 하지만 어릴 적 꾸던 꿈을 이루지 못했다 하여도 앞으로 꾸는 작은 꿈들을 행복하게 꾸지 않을까 싶어요. 모두가 아름다운 꿈을 이루고 나아가길 바라겠습니다.",
  credits: {
    producer: "반오, 안수민",
    vocals: "반오",
    chorus: "조은세",
    electricGuitar: "반오",
    piano: "유조현",
    bass: "김지수",
    drumProgramming: "안수민",
    composition: "반오",
    lyrics: "반오",
    arrangement: "반오, 안수민",
    mixing: "안수민",
    mastering: "starry sound",
    recording: "안수민"
  },
  tracks: [
    {
      id: "2-1",
      title: "먼지 쌓인 나의 앨범 속에는",
      duration: "3:18",
      isTitle: true,
      artist: "반오(Ban.O)",
      youtubeId: "QpXrCjiew_M",
      thumbnail: "https://cdn.ban-o.art/public/music/11472610_20240424134200_1000.jpg",
      likes: 16
    },
    {
      id: "2-2",
      title: "먼지 쌓인 나의 앨범 속에는 (Inst.)",
      duration: "3:18",
      artist: "반오(Ban.O)",
      youtubeId: "QpXrCjiew_M",
      thumbnail: "https://cdn.ban-o.art/public/music/11472610_20240424134200_1000.jpg",
      likes: 4
    }
  ],
  videoCredits: {
    filming: "안수민",
    editing: "안수민",
    location: "스페이스작"
  }
},
{
  id: "3",
  title: "두려움",
  artist: "Ban.O",
  coverImage: "https://cdn.ban-o.art/public/music/10946867_20220509165214_1000.jpg",
  releaseDate: "2022-05-10",
  type: "싱글",
  genre: ["록/메탈"],
  style: "록",
  distributor: "오감엔터테인먼트",
  agency: "반오",
  totalDuration: "03:30",
  description: "누구나 자신에 대한 확신이 서지 않을 때가 있습니다.\n현실에 치여 흔들릴 적의 저와 같은 시간을 보내고 있을 분들께 제 자신에게 건네고 싶었던 말들을 보냅니다.\n모두가 두려움을 발판 삼아 더 큰 꿈을 이뤄내길 바라겠습니다.",
  credits: {
    producer: "반오, 이상지",
    vocals: "반오",
    acousticGuitar: "반오",
    electricGuitar: "박재운",
    piano: "김버금",
    bass: "서무현",
    drums: "김찬영",
    composition: "반오",
    lyrics: "반오",
    arrangement: "반오",
    mixing: "이상지",
    mastering: "이상지"
  },
  tracks: [
    {
      id: "3-1",
      title: "두려움",
      duration: "3:30",
      isTitle: true,
      artist: "반오(Ban.O)",
      youtubeId: "QpXrCjiew_M",
      thumbnail: "https://cdn.ban-o.art/public/music/10946867_20220509165214_1000.jpg",
      likes: 8
    }
  ]
},
{
  id: "4",
  title: "2024 알파카월드 로고송 공모전 수상작 <Welcome to Alpcaca World>",
  artist: "Various Artists",
  coverImage: "https://cdn.ban-o.art/public/music/11496473_20240523170520_1000.jpg",
  releaseDate: "2024-05-23",
  type: "정규",
  genre: ["포크/블루스"],
  style: "포크",
  distributor: "라인앰컴퍼니",
  agency: "알파카월드",
  totalDuration: "120:00",
  description: "강원도 홍천에 위치한 세계 유일 알파카 테마 여행지 '알파카월드'에서 지난 1월 개최한 「2024 알파카월드 로고송 공모전」의 수상작을 한데 모았다.\n\n간결하면서 기억하기 쉬운 로고송과 테마파크의 벅찬 감동을 느낄 수 있는 주제곡, 두 가지 부문으로 접수 받았던 이번 공모전에는 최종 90여 팀의 약 100여 곡이 응모되었으며 이 중 알파카를 비롯한 다양한 동물들과 홍천의 자연경관이 어우러져 국내외 관람객에게 특색 있는 경험을 제공하고 있는 알파카월드의 정체성을 고스란히 담으면서도 다양하고 참신한 아이디어와 수준 높은 퀄리티로 심사위원을 놀라게 한 곡이 상당 수 접수되어 최종 선정까지 고심에 고심을 거듭했다고 전해진다.\n\n최종 선정되어 앨범으로 제작된 10곡은 장르도 컨셉도 제각기 다르지만 모두 듣는 순간 저마다가 상상하고 경험한 알파카월드의 다양한 매력을 생생하게 느낄 수 있다는 공통점을 가지고 있다.\n\n특별히 점차 늘어가는 글로벌 방문객을 위해 전곡 한국어와 영어 버전으로 제작하여 발매한 이번 앨범을 통해 동물 복지 정신에 따라 세심하고 철저하게 운영 중인 알파카월드가 가족적이고 자연친화적인 대한민국 대표 힐링 여행지로서 국내외 관람객에게 더 가까이 다가가는 계기가 되기를 바란다.",
  credits: {
    mastering: "김승재"
  },
  tracks: [
    {
      id: "4-1-1",
      title: "WELCOME TO ALPACA WORLD",
      duration: "3:00",
      isTitle: true,
      artist: "채창현(CHANGHYUN)",
      credits: {
        lyrics: "채창현(CHANGHYUN)",
        composition: "채창현(CHANGHYUN)",
        arrangement: "채창현(CHANGHYUN)",
        vocal: "채창현(CHANGHYUN)",
        chorus: "채창현(CHANGHYUN)",
        mixing: "채창현(CHANGHYUN)",
        mastering: "김승재"
      }
    },
    {
      id: "4-1-2",
      title: "행복이 가득한 이곳 알파카월드",
      duration: "3:00",
      artist: "이재성",
      credits: {
        lyrics: "이재성",
        composition: "이재성",
        arrangement: "이재성",
        vocal: "이재성",
        mixing: "이재성",
        mastering: "김승재"
      }
    },
    {
      id: "4-1-10",
      title: "알파카월드",
      duration: "3:00",
      artist: "반오(Ban.O)",
      credits: {
        lyrics: "Ban.O",
        composition: "Ban.O",
        arrangement: "Ban.O",
        vocal: "Ban.O",
        electricGuitar: "Ban.O",
        acousticGuitar: "Ban.O",
        piano: "Ban.O",
        bass: "Ban.O",
        drums: "Ban.O",
        strings: "Ban.O",
        producer: "Ban.O",
        recording: "Ban.O",
        mixing: "Ban.O",
        mastering: "김승재"
      }
    },
    {
      id: "4-2-10",
      title: "Alpaca World (English ver.)",
      duration: "3:00",
      artist: "Ban.O",
      credits: {
        lyrics: "Ban.O",
        composition: "Ban.O",
        arrangement: "Ban.O",
        vocal: "혜온",
        electricGuitar: "Ban.O",
        acousticGuitar: "Ban.O",
        piano: "Ban.O",
        bass: "Ban.O",
        drums: "Ban.O",
        strings: "Ban.O",
        producer: "Ban.O",
        recording: "Ban.O",
        mixing: "김승재",
        mastering: "김승재"
      }
    }
  ]
}
]

interface Track {
  id: string
  title: string
  duration: string
  isTitle?: boolean
  artist: string
  youtubeId: string
  thumbnail: string
  likes: number
}

export function Albums() {
  const [mounted, setMounted] = useState(false)
  const [selectedAlbumIndex, setSelectedAlbumIndex] = useState(0)

  useEffect(() => {
    setMounted(true)
  }, [])

  if (!mounted) {
    return null
  }

  const selectedAlbum = albums[selectedAlbumIndex]

  return (
    <div className="container mx-auto px-4 py-8">
      <div className="max-w-7xl mx-auto">
        {/* Album List Section */}
        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-4 mb-12">
          {albums.map((album, index) => (
            <div
              key={album.id}
              className={`cursor-pointer transition-all ${
                selectedAlbumIndex === index ? 'opacity-100 scale-105' : 'opacity-70 hover:opacity-100'
              }`}
              onClick={() => setSelectedAlbumIndex(index)}
            >
              <div className="relative aspect-square rounded-lg overflow-hidden shadow-lg group">
                <Image
                  src={album.coverImage}
                  alt={`${album.title} - ${album.artist}`}
                  fill
                  className="object-cover transition-transform duration-300 group-hover:scale-105"
                  sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
                  priority={index === 0}
                />
              </div>
              <div className="mt-3 text-center">
                <p className="font-medium truncate">{album.artist}</p>
                <p className="text-sm text-muted-foreground">{album.releaseDate}</p>
              </div>
            </div>
          ))}
        </div>

        {/* Selected Album Section */}
        <motion.div
          key={selectedAlbum.id}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.3 }}
          className="grid grid-cols-1 lg:grid-cols-3 gap-8"
        >
          {/* Album Cover */}
          <div className="lg:col-span-1">
            <div className="sticky top-24">
              <div className="relative aspect-square rounded-lg overflow-hidden shadow-xl">
                <Image
                  src={selectedAlbum.coverImage}
                  alt={selectedAlbum.title}
                  fill
                  className="object-cover"
                  priority
                  sizes="(max-width: 768px) 100vw, (max-width: 1200px) 33vw, 25vw"
                />
              </div>
              <div className="mt-6">
                <h1 className="text-3xl font-bold">{selectedAlbum.title}</h1>
                <p className="text-lg text-muted-foreground mt-2">{selectedAlbum.artist}</p>
                <div className="flex flex-wrap gap-2 mt-4">
                  <Badge variant="secondary">{selectedAlbum.type}</Badge>
                  {selectedAlbum.genre.map((g) => (
                    <Badge key={g} variant="outline">{g}</Badge>
                  ))}
                </div>
                <div className="mt-6 space-y-2">
                  <div className="flex justify-between text-sm">
                    <span className="text-muted-foreground">발매일</span>
                    <span>{selectedAlbum.releaseDate}</span>
                  </div>
                  <div className="flex justify-between text-sm">
                    <span className="text-muted-foreground">유통사</span>
                    <span>{selectedAlbum.distributor}</span>
                  </div>
                  <div className="flex justify-between text-sm">
                    <span className="text-muted-foreground">기획사</span>
                    <span>{selectedAlbum.agency}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Album Details */}
          <div className="lg:col-span-2">
            <Tabs defaultValue="info" className="w-full">
              <TabsList className="w-full grid grid-cols-3">
                <TabsTrigger value="info">소개</TabsTrigger>
                <TabsTrigger value="tracks">수록곡</TabsTrigger>
                <TabsTrigger value="credits">크레딧</TabsTrigger>
              </TabsList>

              <div className="mt-8">
                <TabsContent value="info">
                  <div className="prose max-w-none">
                    <p className="text-lg leading-relaxed whitespace-pre-line">
                      {selectedAlbum.description}
                    </p>
                  </div>
                </TabsContent>

                <TabsContent value="tracks">
                  <div className="space-y-3">
                    {selectedAlbum.tracks.map((track, index) => (
                      <div
                        key={track.id}
                        className="flex items-center gap-4 p-3 rounded-lg bg-muted/30"
                      >
                        <span className="text-lg font-medium text-muted-foreground w-8">
                          {(index + 1).toString().padStart(2, '0')}
                        </span>
                        <div className="flex-grow">
                          <div className="flex items-center gap-2">
                            <span className="font-medium">{track.title}</span>
                            {track.isTitle && (
                              <Badge variant="secondary" className="text-xs">
                                타이틀곡
                              </Badge>
                            )}
                          </div>
                          <span className="text-sm text-muted-foreground">
                            {track.duration}
                          </span>
                        </div>
                      </div>
                    ))}
                  </div>
                </TabsContent>

                <TabsContent value="credits">
                  <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
                    <div>
                      <h3 className="text-lg font-semibold mb-4">작사/작곡</h3>
                      <dl className="space-y-2">
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">작사</dt>
                          <dd>{selectedAlbum.credits.lyrics}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">작곡</dt>
                          <dd>{selectedAlbum.credits.composition}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">편곡</dt>
                          <dd>{selectedAlbum.credits.arrangement}</dd>
                        </div>
                      </dl>

                      <h3 className="text-lg font-semibold mb-4 mt-8">프로듀싱</h3>
                      <dl className="space-y-2">
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">프로듀서</dt>
                          <dd>{selectedAlbum.credits.producer}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">믹싱</dt>
                          <dd>{selectedAlbum.credits.mixing}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">마스터링</dt>
                          <dd>{selectedAlbum.credits.mastering}</dd>
                        </div>
                      </dl>
                    </div>

                    <div>
                      <h3 className="text-lg font-semibold mb-4">연주</h3>
                      <dl className="space-y-2">
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">보컬</dt>
                          <dd>{selectedAlbum.credits.vocals}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">코러스</dt>
                          <dd>{selectedAlbum.credits.chorus}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">일렉기타</dt>
                          <dd>{selectedAlbum.credits.electricGuitar}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">피아노</dt>
                          <dd>{selectedAlbum.credits.piano}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">베이스</dt>
                          <dd>{selectedAlbum.credits.bass}</dd>
                        </div>
                        <div className="flex justify-between py-2 border-b">
                          <dt className="text-muted-foreground">드럼</dt>
                          <dd>{selectedAlbum.credits.drumProgramming}</dd>
                        </div>
                      </dl>

                      {selectedAlbum.videoCredits && (
                        <>
                          <h3 className="text-lg font-semibold mb-4 mt-8">뮤직비디오</h3>
                          <dl className="space-y-2">
                            <div className="flex justify-between py-2 border-b">
                              <dt className="text-muted-foreground">촬영</dt>
                              <dd>{selectedAlbum.videoCredits.filming}</dd>
                            </div>
                            <div className="flex justify-between py-2 border-b">
                              <dt className="text-muted-foreground">편집</dt>
                              <dd>{selectedAlbum.videoCredits.editing}</dd>
                            </div>
                            <div className="flex justify-between py-2 border-b">
                              <dt className="text-muted-foreground">장소 협찬</dt>
                              <dd>{selectedAlbum.videoCredits.location}</dd>
                            </div>
                          </dl>
                        </>
                      )}
                    </div>
                  </div>
                </TabsContent>
              </div>
            </Tabs>
          </div>
        </motion.div>
      </div>
    </div>
  )
}
