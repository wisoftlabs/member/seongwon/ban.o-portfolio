"use client"

import { useState } from "react"
import { Play, Pause, Mic2, Info, Music, FileAudio } from 'lucide-react'
import { Button } from "@/components/ui/button"
import { ScrollArea } from "@/components/ui/scroll-area"
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import { Card } from "@/components/ui/card"
import { useMusicPlayer } from "./MusicContext"
import Image from "next/image"
import { Badge } from "@/components/ui/badge"

// Shared track data with lyrics and additional information
export const playlistTracks = [
{
  id: "0",
  title: "하나의 별똥별 처럼",
  artist: "Ban.O Band",
  duration: "4:33",
  thumbnail: "https://cdn.ban-o.art/public/music/481924331_17996434796772524_343836101851192975_n.jpg",
  youtubeId: "X7UolnJ-wyo",
  likes: 0,
  lyrics: `저 검은 하늘에 작은 별 하나가 떴으면 좋겠다 생각해
하나의 빛으로 내 마음속에 작은 희망의 꽃을 피워주길

하나의 별똥별처럼 너에게 기쁜 마음을 선물해
네 마음속에 피우고 있는 꽃을 꺼내 주고 싶어

이 별 아래서 하는 노래가 너에게 들릴까
네가 듣고 있을진 모르지만 나 불러볼게
이 순수한 마음에서 나오는 네 이름의 뜻
네가 알고 있을지는 모르지만 다 알려줄게


날 지나쳐갈 수 도 있겠지 그리 멀지 않은 하늘에 빛나고 있는 한점인데

아침 해를 기다리듯 매일 밤을 난 기다려
밝진 않지만 내 맘에 작은 떨림을 주고 싶어

하나의 별똥별처럼 너에게 기쁜 마음을 선물해
네 마음속에 피우고 있는 꽃을 꺼내 주고 싶어

하나의 별똥별처럼 너에게 기쁜 마음을 선물해
네 마음속에 피우고 있는 꽃을 꺼내 주고 싶어

이 별 아래서 하는 노래가 너에게 들릴까
네가 듣고 있을진 모르지만 나 불러볼게
이 순수한 마음에서 나오는 네 이름의 뜻
네가 알고 있을지는 모르지만 다 알려줄게

이 별 아래서 하는 노래가 너에게 들릴까
네가 듣고 있을지는 모르지만 나 불러볼게
이 순수한 마음에서 나오는 네 이름의 뜻
네가 알고 있을지는 모르지만 다 알려줄게`,
  credits: {
    composer: "Ban.O",
    lyricist: "Ban.O",
    arrangement: "Ban.O Band",
    vocals: "Ban.O Band"
  },
  releaseDate: "2025-02-26",
  album: "하나의 별똥별 처럼",
  genre: "락/메탈",
  style: "모던 락",
  label: "반오밴드",
  distributor: "오감엔터테인먼트",
  type: "싱글",
  audioQuality: [
    { type: "FLAC", bitDepth: 16 },
    { type: "FLAC", bitDepth: 24 }
  ]
},
{
  id: "1",
  title: "알파카월드",
  artist: "Ban.O",
  duration: "3:45",
  thumbnail: "https://cdn.ban-o.art/public/music/11496473_20240523170520_1000.jpg",
  youtubeId: "_U4jJMAEwwY",
  likes: 0,
  lyrics: `나와 함께 떠나자
꿈의 나라로
저 초원을 넘어

모두 다 원하고 있어
소망을 담아서
다함께 외쳐봐

Let's Go 알파카월드
우리 함께 초원 위를 달려
모두 알파카월드
아름다운 꿈을 향해 가자

거센 바람이 불어와
내 등을 밀어줘
다함께 뛰어봐

Let's Go 알파카월드
우리 함께 초원 위를 달려
모두 알파카월드
아름다운 꿈을 향해

저 조그만 아이들의 마음을 느껴봐
모두 힘차게 뛰고 있잖아
그 마음을 가득 담아서 함께 손잡고

Let's Go 알파카월드
우리 함께 초원 위를 달려
모두 알파카월드
아름다운 꿈을 향해

다시 알파카월드
아이들의 꿈을 가득 담아
함께 알파카월드
우리들의 행복 알파카월드`,
  credits: {
    composer: "Ban.O",
    lyricist: "Ban.O",
    arrangement: "Ban.O",
    vocals: "Ban.O"
  },
  releaseDate: "2024-05-23",
  album: "2024 알파카월드 로고송 공모전 수상작 <Welcome to Alpcaca World>",
  genre: "포크/블루스",
  audioQuality: [
    { type: "FLAC", bitDepth: 16 },
    { type: "FLAC", bitDepth: 24 }
  ]
},
{
  id: "2",
  title: "꽃길 꽃신",
  artist: "Ban.O",
  duration: "2:49",
  thumbnail: "https://cdn.ban-o.art/public/music/11499595_20240528170547_1000.jpg",
  youtubeId: "ZIEQfv7snIw",
  likes: 0,
  lyrics: `안녕이라는 첫마디로 인사하던 너
홍조를 띈볼과 동여맨 머리가
너를 아름답게 돋보였어
용기 없던 나는 무서웠지만
나 한번 용기 내 고백했고
우리의 사랑이 시작되었지

꽃길만 걷게 해줄게
꽃신을 꼭 신겨줄게
그러니까 옆에 있어야 해
그러니까 나와 함께하자

칠흑이었던 나의 일생에
어둠 속 등대가 되어준 그대
내겐 불행 속에 행복이야
가뭄 속에 내린 단비같이
나를 적셔준 그녀였고
우리의 사랑은 깊어져가지

꽃길만 걷게 해줄게
꽃신을 꼭 신겨줄게
그러니까 옆에 있어야 해
그러니까 나와 함께하자

꽃길만 걷게 해줄게
꽃신을 꼭 신겨줄게
그러니까 옆에 있어야 해
그러니까 나와 함께하자

꽃길만 걷게 해줄게
꽃신을 꼭 신겨줄게
그러니까 옆에 있어야 해
그러니까 나와 함께하자`,
  credits: {
    composer: "Ban.O",
    lyricist: "Ban.O",
    arrangement: "Ban.O",
    vocals: "Ban.O"
  },
  releaseDate: "2024-05-29",
  album: "꽃길 꽃신",
  genre: "발라드",
  audioQuality: [
    { type: "FLAC", bitDepth: 16 },
    { type: "FLAC", bitDepth: 24 }
  ]
},
{
  id: "3",
  title: "먼지 쌓인 나의 앨범 속에는",
  artist: "Ban.O",
  duration: "3:18",
  thumbnail: "https://cdn.ban-o.art/public/music/11472610_20240424134200_1000.jpg",
  youtubeId: "QpXrCjiew_M",
  likes: 0,
  lyrics: `어릴 적 나의 꿈속엔 지금의 내가 있을까
거울 속 내게 물어봐
내가 써놓은 일기엔 나의 꿈이 가득한데
이뤄놓은 게 뭐 있나

자신감이라곤 없는 나의 그 두 어깨 위에
작은 손을 올려주겠니

먼지 쌓인 나의 앨범 속에는
아무것도 모르던 어린 내가 웃고만 있네
지금 내가 보고 있는 나의 모습은
언제부터인지 웃음이라곤 찾지 못하고 그저
Stay alone

모두들 나와 같을까 같은 마음들인 걸까
불안한 하루를 보내네

자신감이라고 없는 나의 그 두 어깨 위에
작은 손을 올려주겠니

먼지 쌓인 나의 앨범 속에는
아무것도 모르던 어린 내가 웃고만 있네
지금 내가 보고 있는 나의 모습은
언제부터한지 웃음이라곤 찾지 못하고 그저

지나간 시간 속에 많은 꿈을 흘려보낸지는 오래고
하루하루를 버텨낼 뿐이야

먼지 쌓인 나의 앨범 속에는
아무것도 모르던 어린 내가 웃고만 있네
지금 내가 보고 있는 나의 모습은
언제부터인지 웃음이라곤 찾지 못하고 그저
Stay alone`,
  credits: {
    composer: "Ban.O",
    lyricist: "Ban.O",
    arrangement: "Ban.O",
    vocals: "Ban.O"
  },
  releaseDate: "2024-04-25",
  album: "먼지 쌓인 나의 앨범 속에는",
  genre: "발라드",
  audioQuality: [
    { type: "FLAC", bitDepth: 16 },
    { type: "FLAC", bitDepth: 24 }
  ]
},
{
  id: "4",
  title: "두려움",
  artist: "Ban.O",
  duration: "4:12",
  thumbnail: "https://cdn.ban-o.art/public/music/10946867_20220509165214_1000.jpg",
  youtubeId: "q12FRl0LD_w",
  likes: 0,
  lyrics: `길게 늘어진 그림자는
나를 잡으려 하고
두려움에 떨기
바쁜 난 주변을 보지 못해
그저 떨리는 두 손을
꼭 잡고 눈을 감고서
아무것도 아냐 무서워하지 말자
두렵지 않아
오 나 주먹을 쥐고 가슴을 펴고
당당히 앞으로 나아갈 거야
어려운 건 아냐 무섭지도 않아
해낼 수 있어
Oh 나 두려움을 밟고 일어나
당당히 두 눈을 뜨고 나면
아름다운 꽃들과 함께
춤추는 별들을 볼 수 있어
가끔은 버겁고 힘들어도
곁에서 안아줄 니가 있어
두 팔을 활짝 펴고 나는
앞으로 나아갈 거야
검은 하늘은 모든 걸 삼킬 듯 까맣지만
내 조그만 빛을 아름답고
선명하게 만들어줬네
주먹을 쥐고 가슴을 펴고
당당히 앞으로 나아갈 거야
어려운 건 아냐 무섭지도 않아
해낼 수 있어
Oh 나 두려움을 밟고 일어나
당당히 두 눈을 뜨고 나면
아름다운 꽃들과 함께
춤추는 별들을 볼 수 있어
가끔은 버겁고 힘들어도
곁에서 안아줄 니가 있어
두 팔을 활짝 펴고 나는
앞으로 나아 갈 거야
맞아 나 무섭고도 두렵기도 해
그래 나 아닌
맞아 나 무섭고도 두렵기도 해
그래 나 아닌 척 웃어넘기기도 해
가끔 내가 너무 힘들어 보일 땐 눈을 감아
Oh 나 두려움을 밟고 일어나
당당히 두 눈을 뜨고 나면
아름다운 꽃들과 함께
춤추는 별들을 볼 수 있어
가끔은 버겁고 힘들어도
곁에서 안아줄 니가 있어
두 팔을 활짝 펴고 나는
앞으로 나아 갈 거야`,
  credits: {
    composer: "Ban.O",
    lyricist: "Ban.O",
    arrangement: "Ban.O",
    vocals: "Ban.O"
  },
  releaseDate: "2022-05-10",
  album: "두려움",
  genre: "록/메탈",
  audioQuality: [
    { type: "FLAC", bitDepth: 16 },
    { type: "FLAC", bitDepth: 24 }
  ]
},
// ... other tracks remain the same
]

export function Playlists() {
  const [selectedTrack, setSelectedTrack] = useState(playlistTracks[0])
  const { 
    setCurrentTrack, 
    setIsPlaying, 
    currentTrack, 
    isPlaying,
    setIsPlaylistMode
  } = useMusicPlayer()

  const handlePlayPause = (track: typeof playlistTracks[0]) => {
    if (currentTrack?.id === track.id) {
      setIsPlaying(!isPlaying)
    } else {
      setCurrentTrack(track)
      setIsPlaying(true)
    }
    setSelectedTrack(track)
    setIsPlaylistMode(false)
  }

  const handlePlayAll = () => {
    const firstTrack = playlistTracks[0]
    setSelectedTrack(firstTrack)
    setCurrentTrack(firstTrack)
    setIsPlaying(true)
    setIsPlaylistMode(true)
  }

  return (
    <div className="flex flex-col gap-8 p-6 max-w-5xl mx-auto">
      {/* 현재 재생 중인 트랙 정보 */}
      <div className="flex flex-col md:flex-row items-start gap-8">
        <div className="relative w-48 h-48 md:w-64 md:h-64 flex-shrink-0">
          <Image
            src={selectedTrack.thumbnail}
            alt={selectedTrack.title}
            width={300}
            height={300}
            className="rounded-xl shadow-lg"
            style={{ objectFit: 'cover', width: '100%', height: '100%' }}
            priority
          />
        </div>
        <div className="flex flex-col justify-end gap-4">
          <div>
            <h1 className="text-4xl font-bold mb-2">{selectedTrack.title}</h1>
            <p className="text-lg text-muted-foreground">{selectedTrack.artist}</p>
          </div>
          <div className="flex items-center gap-4">
            <Button size="lg" className="rounded-full" onClick={() => handlePlayPause(selectedTrack)}>
              {currentTrack?.id === selectedTrack.id && isPlaying ? (
                <Pause className="w-6 h-6" />
              ) : (
                <Play className="w-6 h-6" />
              )}
            </Button>
          </div>
        </div>
      </div>

      {/* 트랙 목록과 상세 정보 */}
      <div className="grid gap-8">
        {/* 트랙 목록 */}
        <Card className="p-6 bg-card/50">
          <div className="flex items-center justify-between mb-6">
            <h2 className="text-2xl font-semibold">트랙</h2>
            <Button variant="ghost" size="sm" onClick={handlePlayAll}>
              <Music className="w-4 h-4 mr-2" />
              전체재생
            </Button>
          </div>
          <div className="space-y-2">
            {playlistTracks.map((track, index) => (
              <div
                key={track.id}
                className={`group flex items-center gap-4 p-4 rounded-lg hover:bg-accent/50 transition-all cursor-pointer ${
                  selectedTrack.id === track.id ? 'bg-accent' : ''
                }`}
                onClick={() => handlePlayPause(track)}
              >
                <div className="flex items-center gap-4 flex-1">
                  <span className="text-muted-foreground w-6 text-center">{index + 1}</span>
                  <div className="relative w-12 h-12 flex-shrink-0">
                    <Image
                      src={track.thumbnail}
                      alt={track.title}
                      width={300}
                      height={300}
                      className="rounded-md"
                      style={{ objectFit: 'cover', width: '100%', height: '100%' }}
                      priority={index < 4}
                    />
                  </div>
                  <div className="flex-grow min-w-0">
                    <div className="font-medium truncate">{track.title}</div>
                    <div className="text-sm text-muted-foreground truncate">
                      {track.artist}
                    </div>
                  </div>
                </div>
                <div className="flex items-center gap-4">
                  <span className="text-sm text-muted-foreground">{track.duration}</span>
                  <button
                    className="opacity-0 group-hover:opacity-100 transition-opacity"
                    onClick={(e) => {
                      e.stopPropagation();
                      handlePlayPause(track);
                    }}
                  >
                    {currentTrack?.id === track.id && isPlaying ? (
                      <Pause className="w-5 h-5" />
                    ) : (
                      <Play className="w-5 h-5" />
                    )}
                  </button>
                </div>
              </div>
            ))}
          </div>
        </Card>

        {/* 트랙 상세 정보 */}
        <Card className="p-6">
          <Tabs defaultValue="lyrics" className="h-full">
            <TabsList className="inline-flex h-10 items-center justify-center rounded-md bg-muted p-1 mb-6">
              <TabsTrigger value="lyrics" className="rounded-sm px-3 py-1.5 text-sm font-medium">
                <Mic2 className="w-4 h-4 mr-2" />
                가사
              </TabsTrigger>
              <TabsTrigger value="info" className="rounded-sm px-3 py-1.5 text-sm font-medium">
                <Info className="w-4 h-4 mr-2" />
                정보
              </TabsTrigger>
              <TabsTrigger value="audio" className="rounded-sm px-3 py-1.5 text-sm font-medium">
                <FileAudio className="w-4 h-4 mr-2" />
                오디오
              </TabsTrigger>
            </TabsList>

            <div className="mt-4">
              <TabsContent value="lyrics" className="focus-visible:outline-none">
                <div className="space-y-4">
                  <div className="whitespace-pre-line text-base leading-relaxed">
                    {selectedTrack.lyrics}
                  </div>
                </div>
              </TabsContent>

              <TabsContent value="info" className="focus-visible:outline-none">
                <div className="space-y-6">
                  <div>
                    <h4 className="text-sm font-medium text-muted-foreground mb-4">크레딧</h4>
                    <dl className="grid grid-cols-2 gap-4">
                      <div>
                        <dt className="text-sm text-muted-foreground">보컬</dt>
                        <dd className="text-sm font-medium mt-1">{selectedTrack.credits.vocals}</dd>
                      </div>
                      <div>
                        <dt className="text-sm text-muted-foreground">작곡</dt>
                        <dd className="text-sm font-medium mt-1">{selectedTrack.credits.composer}</dd>
                      </div>
                      <div>
                        <dt className="text-sm text-muted-foreground">작사</dt>
                        <dd className="text-sm font-medium mt-1">{selectedTrack.credits.lyricist}</dd>
                      </div>
                      <div>
                        <dt className="text-sm text-muted-foreground">편곡</dt>
                        <dd className="text-sm font-medium mt-1">{selectedTrack.credits.arrangement}</dd>
                      </div>
                    </dl>
                  </div>
                  <div>
                    <h4 className="text-sm font-medium text-muted-foreground mb-4">앨범 정보</h4>
                    <dl className="space-y-2">
                      <div className="flex justify-between">
                        <dt className="text-sm text-muted-foreground">장르</dt>
                        <dd className="text-sm font-medium">{selectedTrack.genre}</dd>
                      </div>
                      <div className="flex justify-between">
                        <dt className="text-sm text-muted-foreground">발매일</dt>
                        <dd className="text-sm font-medium">
                          {new Date(selectedTrack.releaseDate).toLocaleDateString('ko-KR', {
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric'
                          })}
                        </dd>
                      </div>
                    </dl>
                  </div>
                </div>
              </TabsContent>

              <TabsContent value="audio" className="focus-visible:outline-none">
                <div className="space-y-4">
                  {selectedTrack.audioQuality.map((quality, index) => (
                    <div key={index} className="flex items-center justify-between p-4 rounded-lg bg-accent/50">
                      <div className="flex items-center gap-3">
                        <Music className="w-4 h-4" />
                        <span className="font-medium">{quality.type} {quality.bitDepth}bit</span>
                      </div>
                      <Badge variant="secondary">고음질</Badge>
                    </div>
                  ))}
                </div>
              </TabsContent>
            </div>
          </Tabs>
        </Card>
      </div>
    </div>
  );
}
