"use client"

import { useState, useEffect, useRef } from "react"
import { ScrollArea } from "@/components/ui/scroll-area"
import Image from 'next/image'
import { motion } from "framer-motion"

const coverSongs = [
  // 최신순으로 정렬
  { id: "Ccq1hl5a8D4", title: "하나의 별똥별처럼 - 반오 | 하로 최승일의 봉사리팝스", thumbnail: `https://img.youtube.com/vi/Ccq1hl5a8D4/maxresdefault.jpg` },
  { id: "WS486kwctF8", title: "먼지 쌓인 나의 앨범 속에는 - 반오 | 하로 최승일의 봉사리팝스", thumbnail: `https://img.youtube.com/vi/WS486kwctF8/maxresdefault.jpg` },
  { id: "CJj6awfaU6M", title: "너라는 별 - 고추잠자리 | Cover By Ban.O Band 반오밴드", thumbnail: `https://img.youtube.com/vi/CJj6awfaU6M/maxresdefault.jpg` },
  { id: "j8uxq1-7KRg", title: "녹아내려요 - 데이식스(DAY6) 어쿠스틱 ver. COVER BY 반오", thumbnail: `https://img.youtube.com/vi/j8uxq1-7KRg/maxresdefault.jpg` },
  { id: "yXx34Lr3vKo", title: "사랑이 죄야? - KIXO,10CM,B.I | cover by 반오[Ban.O] | 쉬운코드 | 발매전커버", thumbnail: `https://img.youtube.com/vi/yXx34Lr3vKo/maxresdefault.jpg` },
  { id: "l1Iohw-HNJs", title: "소년이 어른이 되어 - 몽니 | Cover by 반오 [Ban.O]", thumbnail: `https://img.youtube.com/vi/l1Iohw-HNJs/maxresdefault.jpg` },
  { id: "ZIOAl87K0mY", title: "봄봄봄-로이킴 | cover by 반오", thumbnail: `https://img.youtube.com/vi/ZIOAl87K0mY/maxresdefault.jpg` },
  { id: "yPGIYCaThgg", title: "어떻게 사랑이 그래요 - 이승환 | cover by 반오", thumbnail: `https://img.youtube.com/vi/yPGIYCaThgg/maxresdefault.jpg` },
  { id: "QZSSG7m6dEg", title: "봄이와도 - 로이킴,박종민 | cover by 반오 | 쉬운기타코드", thumbnail: `https://img.youtube.com/vi/QZSSG7m6dEg/maxresdefault.jpg` },
  { id: "qcwL4NT3eDQ", title: "형 - 노라조 | cover by 반오", thumbnail: `https://img.youtube.com/vi/qcwL4NT3eDQ/maxresdefault.jpg` },
  { id: "nknOZOOihkE", title: "하루 - 포지션 | cover by 반오", thumbnail: `https://img.youtube.com/vi/nknOZOOihkE/maxresdefault.jpg` },
  { id: "i0DZrP08fGE", title: "밤양갱 - 비비 | cover by 반오 | with 콩이", thumbnail: `https://img.youtube.com/vi/i0DZrP08fGE/maxresdefault.jpg` },
  { id: "wJFXW7MEQCc", title: "그대만 있다면 - 너드커넥션 | cover by 반오", thumbnail: `https://img.youtube.com/vi/wJFXW7MEQCc/maxresdefault.jpg` },
  { id: "m1GOjjrraRo", title: "당연한 것들 - 이적 | cover by 반오", thumbnail: `https://img.youtube.com/vi/m1GOjjrraRo/maxresdefault.jpg` },
  { id: "cS4xs7I58JI", title: "나였으면 - 나윤권 | cover by 반오", thumbnail: `https://img.youtube.com/vi/cS4xs7I58JI/maxresdefault.jpg` },
  { id: "jbVLjOD9GcU", title: "빨래 - 이적 | cover by 반오", thumbnail: `https://img.youtube.com/vi/jbVLjOD9GcU/maxresdefault.jpg` },
  { id: "Bvy0YPjcqXs", title: "지친하루 - 김필,윤종신,곽진언 | cover by 반오", thumbnail: `https://img.youtube.com/vi/Bvy0YPjcqXs/maxresdefault.jpg` },
  { id: "0lm303EmB20", title: "The Christmas song - Anson Seabra | cover by 반오 Ban.O", thumbnail: `https://img.youtube.com/vi/0lm303EmB20/maxresdefault.jpg` },
  { id: "-GgLrmh3_wc", title: "사랑한 만큼 - 박재정 | cover by 반오 Ban.O", thumbnail: `https://img.youtube.com/vi/-GgLrmh3_wc/maxresdefault.jpg` },
  { id: "iPxJwWeazrI", title: "Love poem - 아이유 cover by 반오Ban.O | rock ballad | 남자", thumbnail: `https://img.youtube.com/vi/iPxJwWeazrI/maxresdefault.jpg` },
  { id: "JNT1UmCEE0Y", title: "Elephant - demien rice | cover by 반오 Ban.O", thumbnail: `https://img.youtube.com/vi/JNT1UmCEE0Y/maxresdefault.jpg` },
  { id: "rINnFZNCdrA", title: "눈 - 자이언티 | cover by 반오Ban.O , 종훈 | 즉흥LIVE", thumbnail: `https://img.youtube.com/vi/rINnFZNCdrA/maxresdefault.jpg` },
  { id: "z73EGtDhZ_g", title: "[반오 BAN.O] 조용하고 잔잔한 크리스마스에 / The Christmas song / cover.", thumbnail: `https://img.youtube.com/vi/z73EGtDhZ_g/maxresdefault.jpg` },
  { id: "2IX0_BrRpmw", title: "나는 당신에게 그저 - 배인혁 | cover by Ban.O", thumbnail: `https://img.youtube.com/vi/2IX0_BrRpmw/maxresdefault.jpg` },
  { id: "EBLCFVppUKM", title: "Star - 엔플라잉(N.Flying) | 선재업고튀어 OST | cover by Ban.O", thumbnail: `https://img.youtube.com/vi/EBLCFVppUKM/maxresdefault.jpg` },
]

export function CoverSongs() {
  const [selectedVideo, setSelectedVideo] = useState(coverSongs[0])
  const playerRef = useRef<any>(null)

  useEffect(() => {
    // YouTube IFrame API 로드
    const tag = document.createElement('script')
    tag.src = 'https://www.youtube.com/iframe_api'
    const firstScriptTag = document.getElementsByTagName('script')[0]
    firstScriptTag.parentNode?.insertBefore(tag, firstScriptTag)

    // YouTube API 준비되면 플레이어 초기화
    window.onYouTubeIframeAPIReady = () => {
      playerRef.current = new window.YT.Player('youtube-player', {
        videoId: selectedVideo.id,
        playerVars: {
          autoplay: 1,
          rel: 0,
          modestbranding: 1
        },
        events: {
          onStateChange: (event: any) => {
            // 비디오가 끝나면 다음 비디오 재생
            if (event.data === window.YT.PlayerState.ENDED) {
              const currentIndex = coverSongs.findIndex(video => video.id === selectedVideo.id)
              const nextIndex = (currentIndex + 1) % coverSongs.length
              setSelectedVideo(coverSongs[nextIndex])
            }
          }
        }
      })
    }

    return () => {
      // 컴포넌트 언마운트 시 플레이어 정리
      if (playerRef.current) {
        playerRef.current.destroy()
      }
    }
  }, []) // 최초 마운트 시에만 실행

  useEffect(() => {
    // 선택된 비디오가 변경될 때 새 비디오 로드
    if (playerRef.current && playerRef.current.loadVideoById) {
      playerRef.current.loadVideoById(selectedVideo.id)
    }
  }, [selectedVideo.id])

  return (
    <div className="container mx-auto px-4 py-16">
      {/* Main Video Section */}
      <div className="mb-8">
        <div className="relative aspect-video rounded-2xl overflow-hidden shadow-lg">
          <div id="youtube-player" className="absolute inset-0 w-full h-full"></div>
        </div>
        <div className="mt-4 space-y-2">
          <h3 className="text-lg font-bold tracking-tight line-clamp-2">{selectedVideo.title}</h3>
          <div className="h-0.5 w-16 bg-gradient-to-r from-purple-600 to-pink-600 rounded-full" />
        </div>
      </div>

      {/* Playlist Section */}
      <div className="rounded-xl bg-white/[0.01] border border-white/10">
        <div className="max-h-[400px] overflow-y-auto">
          <div className="grid grid-cols-1 gap-3 p-4">
            {coverSongs.map((video, index) => (
              <motion.div
                key={video.id}
                whileHover={{ scale: 1.02 }}
                whileTap={{ scale: 0.98 }}
                className={`group relative rounded-xl cursor-pointer overflow-hidden border transition-all duration-300
                  ${selectedVideo.id === video.id 
                    ? 'border-purple-400/50 bg-purple-500/[0.03]' 
                    : 'border-transparent hover:border-purple-400/20 hover:bg-white/[0.02]'
                  }`}
                onClick={() => setSelectedVideo(video)}
              >
                <div className="flex p-3 gap-4 items-center">
                  <div className="relative w-32 aspect-video rounded-lg overflow-hidden flex-shrink-0">
                    <div className="absolute left-2 top-2 z-10 bg-black/60 px-2 py-0.5 rounded text-xs">
                      {index + 1}
                    </div>
                    <Image
                      src={video.thumbnail}
                      alt={video.title}
                      fill
                      className="object-cover transition-transform duration-300 group-hover:scale-105"
                      sizes="(max-width: 768px) 128px, 128px"
                      priority={index === 0}
                    />
                    {selectedVideo.id === video.id && (
                      <div className="absolute inset-0 bg-purple-400/10 backdrop-blur-[1px]" />
                    )}
                  </div>
                  <div className="flex-grow min-w-0">
                    <p className="text-sm font-medium line-clamp-2 group-hover:text-purple-300/90 transition-colors">
                      {video.title}
                    </p>
                  </div>
                </div>
              </motion.div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
