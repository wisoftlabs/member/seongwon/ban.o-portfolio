"use client"

import { useState, useRef } from "react"
import { motion } from "framer-motion"
import { ChevronLeft, ChevronRight, Play, X } from 'lucide-react'
import Image from "next/image"

const videos = [
  {
    id: "0",
    title: "반오밴드(Ban.O Band) - '하나의 별똥별 처럼'",
    thumbnail: "https://img.youtube.com/vi/X7UolnJ-wyo/0.jpg",
    youtubeId: "X7UolnJ-wyo"
  },
  {
    id: "1",
    title: "너라는 별 - 고추잠자리 | Cover By Ban.O Band 반오밴드",
    thumbnail: "https://img.youtube.com/vi/CJj6awfaU6M/0.jpg",
    youtubeId: "CJj6awfaU6M"
  },
  {
    id: "2",
    title: "반오(Ban.O) '꽃길꽃신' Acoustic MV",
    thumbnail: "https://img.youtube.com/vi/5WjZqfedfBc/0.jpg",
    youtubeId: "5WjZqfedfBc"
  },
  {
    id: "3",
    title: "반오-먼지 쌓인 나의 앨범 속에는 | 작업실 ver.",
    thumbnail: "https://img.youtube.com/vi/Q15dG017unE/0.jpg",
    youtubeId: "Q15dG017unE"
  },
  {
    id: "4",
    title: "[반오 Ban.O] 숲과바람 - 반오 | 자작곡 | DEMO",
    thumbnail: "https://img.youtube.com/vi/oDv4OMr4aS0/0.jpg",
    youtubeId: "oDv4OMr4aS0"
  },
  {
    id: "5",
    title: "[반오 BAN.O] 여름밤의 꿈 - 반오 | 자작곡 | DEMO",
    thumbnail: "https://img.youtube.com/vi/igssmuHqmOA/0.jpg",
    youtubeId: "igssmuHqmOA"
  },
]

// Create an extended array for infinite scroll
const extendedVideos = [...videos, ...videos, ...videos]

export function RecommendedVideos() {
  const [selectedVideo, setSelectedVideo] = useState<string | null>(null)
  const scrollContainerRef = useRef<HTMLDivElement>(null)
  const [scrollPosition, setScrollPosition] = useState(0)

  const scroll = (direction: 'left' | 'right') => {
    const container = scrollContainerRef.current
    if (!container) return

    const scrollAmount = 300
    const newPosition = direction === 'left' 
      ? scrollPosition - scrollAmount 
      : scrollPosition + scrollAmount

    container.scrollTo({
      left: newPosition,
      behavior: 'smooth'
    })
    setScrollPosition(newPosition)

    // Reset scroll position when reaching the end of extended content
    if (newPosition >= container.scrollWidth - container.clientWidth) {
      setTimeout(() => {
        container.scrollTo({ left: 0, behavior: 'auto' })
        setScrollPosition(0)
      }, 300)
    }
    // Reset scroll position when reaching the start
    if (newPosition <= 0) {
      setTimeout(() => {
        container.scrollTo({ 
          left: container.scrollWidth - container.clientWidth, 
          behavior: 'auto' 
        })
        setScrollPosition(container.scrollWidth - container.clientWidth)
      }, 300)
    }
  }

  return (
    <section className="relative mb-12">
      <div className="flex items-center justify-between mb-4">
        <h2 className="text-2xl font-bold">Music Videos</h2>
        <div className="flex items-center gap-2">
          <button 
            className="p-2 rounded-full hover:bg-gray-100 transition-colors"
            onClick={() => scroll('left')}
          >
            <ChevronLeft className="w-5 h-5" />
          </button>
          <button 
            className="p-2 rounded-full hover:bg-gray-100 transition-colors"
            onClick={() => scroll('right')}
          >
            <ChevronRight className="w-5 h-5" />
          </button>
        </div>
      </div>
      <div 
        ref={scrollContainerRef}
        className="overflow-x-auto scrollbar-hide -mx-4 px-4"
        style={{ scrollBehavior: 'smooth' }}
      >
        <div className="flex gap-4 min-w-max">
          {extendedVideos.map((video, index) => (
            <motion.div
              key={`${video.id}-${index}`}
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ delay: index * 0.1 }}
              className="group relative w-80 aspect-video rounded-xl overflow-hidden cursor-pointer flex-shrink-0"
              onClick={() => setSelectedVideo(video.youtubeId)}
            >
              <Image
                src={video.thumbnail}
                alt={video.title}
                fill
                className="object-cover transition-transform duration-300 group-hover:scale-105"
                sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
                quality={75}
                priority={index === 0}
              />
              <div className="absolute inset-0 bg-black/60 opacity-0 group-hover:opacity-100 transition-opacity flex flex-col items-center justify-center text-white p-4">
                <Play className="w-12 h-12 mb-2" />
                <h3 className="text-sm font-bold text-center">{video.title}</h3>
              </div>
            </motion.div>
          ))}
        </div>
      </div>
      {selectedVideo && (
        <VideoModal videoId={selectedVideo} onClose={() => setSelectedVideo(null)} />
      )}
    </section>
  )
}

function VideoModal({ videoId, onClose }: { videoId: string; onClose: () => void }) {
  return (
    <div className="fixed inset-0 bg-black bg-opacity-75 flex items-center justify-center z-50">
      <div className="relative w-full max-w-4xl aspect-video">
        <button
          className="absolute top-4 right-4 text-white hover:text-gray-300 transition-colors"
          onClick={onClose}
        >
          <X size={24} />
        </button>
        <iframe
          width="100%"
          height="100%"
          src={`https://www.youtube.com/embed/${videoId}`}
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></iframe>
      </div>
    </div>
  )
}

