"use client"

import { useRef, useState } from "react"
import { motion } from "framer-motion"
import { ChevronLeft, ChevronRight, Play, Pause } from 'lucide-react'
import Image from "next/image"
import { useMusicPlayer } from "./MusicContext"

const albums = [
{
  id: "0",
  title: "하나의 별똥별 처럼",
  artist: "Ban.O Band",
  coverImage: "https://cdn.ban-o.art/public/music/481924331_17996434796772524_343836101851192975_n.jpg",
  thumbnail: "https://cdn.ban-o.art/public/music/481924331_17996434796772524_343836101851192975_n.jpg",
  releaseDate: "2025-02-26",
  duration: "4:33",
  youtubeId: "X7UolnJ-wyo"
},
{
  id: "1",
  title: "꽃길 꽃신",
  artist: "Ban.O",
  coverImage: "https://cdnimg.melon.co.kr/cm2/album/images/114/99/595/11499595_20240528170547_1000.jpg",
  thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/99/595/11499595_20240528170547_1000.jpg",
  releaseDate: "2024-05-29",
  duration: "2:49",
  youtubeId: "ZIEQfv7snIw"
},
{
  id: "2",
  title: "먼지 쌓인 나의 앨범 속에는",
  artist: "Ban.O",
  coverImage: "https://cdnimg.melon.co.kr/cm2/album/images/114/72/610/11472610_20240424134200_1000.jpg",
  thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/72/610/11472610_20240424134200_1000.jpg",
  releaseDate: "2024-04-25",
  duration: "3:18",
  youtubeId: "QpXrCjiew_M"
},
{
  id: "3",
  title: "알파카월드",
  artist: "Ban.O",
  coverImage: "https://cdnimg.melon.co.kr/cm2/album/images/114/96/473/11496473_20240523170520_1000.jpg",
  thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/96/473/11496473_20240523170520_1000.jpg",
  releaseDate: "2024-05-23",
  duration: "2:27",
  youtubeId: "_U4jJMAEwwY"
},
{
  id: "4",
  title: "두려움",
  artist: "Ban.O",
  coverImage: "https://cdnimg.melon.co.kr/cm2/album/images/109/46/867/10946867_20220509165214_1000.jpg",
  thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/109/46/867/10946867_20220509165214_1000.jpg",
  releaseDate: "2022-05-10",
  duration: "5:27",
  youtubeId: "q12FRl0LD_w"
}
]

export function TopMusic() {
const scrollContainerRef = useRef<HTMLDivElement>(null)
const [scrollPosition, setScrollPosition] = useState(0)
const { setCurrentTrack, setIsPlaying, currentTrack, isPlaying } = useMusicPlayer()

const scroll = (direction: 'left' | 'right') => {
  const container = scrollContainerRef.current
  if (!container) return

  const scrollAmount = container.clientWidth * 0.8
  const newPosition = direction === 'left' 
    ? Math.max(0, scrollPosition - scrollAmount)
    : Math.min((container.scrollWidth - container.clientWidth), scrollPosition + scrollAmount)
  
  container.scrollTo({
    left: newPosition,
    behavior: 'smooth'
  })
  setScrollPosition(newPosition)
}

const handlePlayPause = (album: typeof albums[0]) => {
  if (currentTrack?.id === album.id) {
    setIsPlaying(!isPlaying)
  } else {
    setCurrentTrack({
      ...album,
      thumbnail: album.thumbnail,
      likes: 0
    })
    setIsPlaying(true)
  }
}

return (
  <section className="relative py-12">
    <div className="flex items-center justify-between mb-4">
      <h2 className="text-2xl font-bold">Ban.O Albums</h2>
      <div className="flex items-center gap-2">
        <button 
          className="p-2 rounded-full hover:bg-gray-100 transition-colors"
          onClick={() => scroll('left')}
        >
          <ChevronLeft className="w-5 h-5" />
        </button>
        <button 
          className="p-2 rounded-full hover:bg-gray-100 transition-colors"
          onClick={() => scroll('right')}
        >
          <ChevronRight className="w-5 h-5" />
        </button>
      </div>
    </div>
    <div 
      ref={scrollContainerRef}
      className="overflow-x-auto scrollbar-hide pb-4 -mx-4 px-4"
    >
      <div className="flex gap-4 min-w-max">
        {albums.map((album, index) => (
          <motion.div
            key={album.id}
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ delay: index * 0.1 }}
            className="group relative w-[280px] aspect-square rounded-xl overflow-hidden flex-shrink-0"
          >
            <div className="relative aspect-square">
              <Image
                src={album.coverImage}
                alt={album.title}
                fill
                className="object-cover rounded-lg"
                sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
              />
              <div className="absolute inset-0 bg-black/60 opacity-0 group-hover:opacity-100 transition-opacity flex flex-col items-center justify-center text-white p-4">
                <h3 className="text-lg font-bold text-center px-2 mb-2">{album.title}</h3>
                <p className="text-sm mb-4">{album.artist}</p>
                <p className="text-sm mb-4">{album.duration}</p>
                <button
                  onClick={() => handlePlayPause(album)}
                  className="bg-white text-black rounded-full p-3 hover:bg-gray-200 transition-colors"
                >
                  {currentTrack?.id === album.id && isPlaying ? (
                    <Pause className="w-6 h-6" />
                  ) : (
                    <Play className="w-6 h-6" />
                  )}
                </button>
              </div>
            </div>
          </motion.div>
        ))}
      </div>
    </div>
  </section>
)
}
