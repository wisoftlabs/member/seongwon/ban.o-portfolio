// "use client"

// import Image from "next/image"
// import { motion } from "framer-motion"
// import { Card } from "@/components/ui/card"
// import { Youtube, Instagram, Music } from 'lucide-react'
// import Link from "next/link"

// export function ArtistProfile() {
//   return (
//     <div className="min-h-screen bg-white">
//       {/* Hero Banner */}
//       <div className="relative h-[400px]">
//         <Image
//           src="https://cdn.ban-o.art/public/profile/hero-banner.jpg"
//           alt="Ban.O Hero Image"
//           fill
//           style={{ objectFit: "cover" }}
//         />
//         <div className="absolute inset-0 bg-gradient-to-b from-black/60 via-black/40 to-transparent">
//           <div className="container mx-auto px-4 h-full flex items-end pb-12">
//             <div className="flex items-end gap-8">
//               <motion.div
//                 initial={{ opacity: 0, scale: 0.8 }}
//                 animate={{ opacity: 1, scale: 1 }}
//                 className="relative w-40 h-40 rounded-full overflow-hidden border-4 border-white shadow-xl"
//               >
//                 <div className="relative w-32 h-32 md:w-48 md:h-48">
//                   <Image
//                     src="https://cdn.ban-o.art/public/profile/bano-profile.jpg"
//                     alt="Ban.O Profile"
//                     width={192}
//                     height={192}
//                     style={{ objectFit: 'cover' }}
//                     className="rounded-full"
//                     priority
//                   />
//                 </div>
//               </motion.div>
//               <motion.div
//                 initial={{ opacity: 0, y: 20 }}
//                 animate={{ opacity: 1, y: 0 }}
//                 transition={{ delay: 0.2 }}
//                 className="text-white pb-2"
//               >
//                 <h1 className="text-4xl font-bold mb-2">Ban.O</h1>
//                 <p className="text-lg mb-2">싱어송라이터</p>
//                 <div className="flex items-center gap-4">
//                   <Link href="https://www.youtube.com/@ban.o" target="_blank" rel="noopener noreferrer">
//                     <Youtube className="h-5 w-5 hover:text-gray-300 transition-colors" />
//                   </Link>
//                   <Link href="https://www.instagram.com/ban.o_official" target="_blank" rel="noopener noreferrer">
//                     <Instagram className="h-5 w-5 hover:text-gray-300 transition-colors" />
//                   </Link>
//                   <Link href="https://open.spotify.com/artist/ban.o" target="_blank" rel="noopener noreferrer">
//                     <Music className="h-5 w-5 hover:text-gray-300 transition-colors" />
//                   </Link>
//                 </div>
//               </motion.div>
//             </div>
//           </div>
//         </div>
//       </div>

//       <div className="container mx-auto px-4 py-12">
//         <motion.div
//           initial={{ opacity: 0, y: 20 }}
//           animate={{ opacity: 1, y: 0 }}
//           transition={{ delay: 0.3 }}
//         >
//           <Card className="p-6 max-w-2xl mx-auto">
//             <h2 className="text-2xl font-bold mb-4">프로필</h2>
//             <div className="space-y-4 text-gray-600">
//               <p>
//                 Ban.O는 대한민국의 싱어송라이터입니다. 독특한 음색과 감성적인 가사로 많은 이들의 마음을 울리는 음악을 만들고 있습니다.
//               </p>
//               <p>
//                 고등학교 1학년 때 우연히 집에 있던 기타를 잡은 것을 계기로 음악의 길로 들어섰으며, 25살에 본격적인 음악 활동을 시작했습니다.
//               </p>
//               <p>
//                 현재 JC 엔터테인먼트 소속으로 활동 중이며, 인디 음악씬에서 주목받는 아티스트로 성장하고 있습니다.
//               </p>
//               <p className="text-muted-foreground">
//                 안녕하세요! 저는 &#39;바노&#39;입니다. 음악으로 여러분의 마음을 울리고 싶습니다.
//               </p>
//             </div>
//             <div className="mt-6">
//               <h3 className="font-semibold mb-2">주요 활동</h3>
//               <ul className="list-disc list-inside space-y-1 text-gray-600">
//                 <li>2022년 싱글앨범 &lt;두려움&gt;으로 데뷔</li>
//                 <li>이만복의 음악복덕방 시즌1 작곡가 참가</li>
//                 <li>숨은 뮤지션 발굴 프로젝트 &apos;히든스테이지 2탄&apos; 참가</li>
//               </ul>
//             </div>
//           </Card>
//         </motion.div>
//       </div>
//     </div>
//   )
// }
