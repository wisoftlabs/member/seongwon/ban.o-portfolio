"use client"

import { Play, Pause, Heart } from 'lucide-react'
import { motion } from "framer-motion"
import Image from "next/image"
import { useMusicPlayer } from './MusicContext'

const popularTracks = [
  {
    id: "0",
    title: "하나의 별똥별 처럼",
    thumbnail: "https://cdn.ban-o.art/public/music/481924331_17996434796772524_343836101851192975_n.jpg",
    youtubeId: "X7UolnJ-wyo",
    duration: "4:33",
    likes: 1852,
    artist: "Ban.O Band"
  },
  {
    id: "1",
    title: "꽃길 꽃신",
    thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/99/595/11499595_20240528170547_1000.jpg",
    youtubeId: "ZIEQfv7snIw",
    duration: "2:49",
    likes: 1234,
    artist: "Ban.O"
  },
  {
    id: "2",
    title: "두려움",
    thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/109/46/867/10946867_20220509165214_1000.jpg",
    youtubeId: "q12FRl0LD_w",
    duration: "5:27",
    likes: 1567,
    artist: "Ban.O"
  },
  {
    id: "3",
    title: "알파카월드",
    thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/96/473/11496473_20240523170520_1000.jpg",
    youtubeId: "_U4jJMAEwwY",
    duration: "2:27",
    likes: 982,
    artist: "Ban.O"
  },
  {
    id: "4",
    title: "먼지 쌓인 나의 앨범 속에는",
    thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/114/72/610/11472610_20240424134200_1000.jpg",
    youtubeId: "QpXrCjiew_M",
    duration: "3:18",
    likes: 856,
    artist: "Ban.O"
  },
  {
    id: "5",
    title: "살며시 (Feat. Ban.O)",
    thumbnail: "https://cdnimg.melon.co.kr/cm2/album/images/113/05/207/11305207_20230811170906_1000.jpg",
    youtubeId: "f81GZbzP3mE",
    duration: "2:49",
    likes: 723,
    artist: "DIREVE"
  }
]

export function PopularTracks() {
  const { setCurrentTrack, setIsPlaying, currentTrack, isPlaying } = useMusicPlayer()

  const handlePlay = (track: typeof popularTracks[0]) => {
    if (currentTrack?.id === track.id) {
      setIsPlaying(!isPlaying)
    } else {
      setCurrentTrack(track)
      setIsPlaying(true)
    }
  }

  return (
    <section>
      <div className="max-h-[50vh] overflow-y-auto pr-4">
        <h2 className="text-2xl font-bold mb-4">Popular</h2>
        <div className="space-y-2">
          {popularTracks.map((track, index) => (
            <motion.div
              key={track.id}
              initial={{ opacity: 0, x: -20 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ delay: index * 0.1 }}
              className="flex items-center gap-4 p-2 rounded-lg hover:bg-gray-100 group"
            >
              <div className="relative w-12 h-12">
                <Image
                  src={track.thumbnail}
                  alt={track.title}
                  width={48}
                  height={48}
                  style={{ objectFit: 'cover' }}
                  className="rounded"
                  priority
                />
                <button 
                  className="absolute inset-0 bg-black/60 opacity-0 group-hover:opacity-100 transition-opacity flex items-center justify-center text-white"
                  onClick={() => handlePlay(track)}
                >
                  {currentTrack?.id === track.id && isPlaying ? (
                    <Pause className="w-6 h-6" />
                  ) : (
                    <Play className="w-6 h-6" />
                  )}
                </button>
              </div>
              <div className="flex-1">
                <h3 className="font-medium">{track.title}</h3>
                <p className="text-sm text-gray-500">{track.artist}</p>
              </div>
              <div className="flex items-center gap-2 text-gray-500">
                <Heart className="w-4 h-4" />
                <span className="text-sm">{track.likes.toLocaleString()}</span>
              </div>
              <span className="text-sm text-gray-500">{track.duration}</span>
            </motion.div>
          ))}
        </div>
      </div>
    </section>
  )
}
