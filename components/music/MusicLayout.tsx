"use client"

import { Home, PlayCircle, User, Disc, Radio, Video, Tv } from 'lucide-react'
import { Button } from "@/components/ui/button"
import { ScrollArea } from "@/components/ui/scroll-area"
import { MusicPlayer } from "./MusicPlayer"
import { TopMusic } from "./TopMusic"
import { PopularTracks } from "./PopularTracks"
import { RecommendedVideos } from "./RecommendedVideos"
import { ReactNode } from 'react';
import { MusicProvider } from "./MusicContext"
import Image from "next/image"

interface NavItem {
  icon: typeof Home
  label: string
  href: string
}

interface LayoutProps {
  children?: ReactNode
}

const browseItems: NavItem[] = [
  { icon: Home, label: "홈", href: "#" },
  { icon: PlayCircle, label: "플레이리스트", href: "#playlists" },
  { icon: User, label: "아티스트", href: "#artist" },
  { icon: Disc, label: "앨범", href: "#albums" },
]

const discoverItems: NavItem[] = [
  { icon: Radio, label: "커버 노래", href: "#covers" },
  { icon: Video, label: "숏츠 영상", href: "#shorts" },
  { icon: Tv, label: "출연 영상", href: "#appearances" },
]

const streamingPlatforms = [
  { 
    icon: "https://cdn-icons-png.flaticon.com/512/3669/3669986.png", 
    label: "Spotify", 
    href: "#" 
  },
  { 
    icon: "https://cdn-icons-png.flaticon.com/512/7566/7566380.png", 
    label: "Apple Music", 
    href: "#" 
  },
  { 
    icon: "https://cdn-icons-png.flaticon.com/512/16592/16592511.png", 
    label: "YouTube Music", 
    href: "#" 
  },
  { 
    icon: "/icons/melon.svg", 
    label: "Melon", 
    href: "#" 
  },
  { 
    icon: "/icons/genie.svg", 
    label: "Genie", 
    href: "#" 
  },
  { 
    icon: "/icons/bugs.svg", 
    label: "Bugs", 
    href: "#" 
  },
]

export function MusicLayout({ children }: LayoutProps) {
  return (
    <MusicProvider>
      <div className="flex flex-col min-h-screen">
        <div className="flex-grow overflow-hidden">
          <div className="grid lg:grid-cols-5 h-full">
            <div className="pb-12 hidden lg:block">
              <div className="space-y-4 py-4">
                <div className="px-3 py-2">
                  <h2 className="mb-2 px-4 text-lg font-semibold">Browse</h2>
                  <div className="space-y-1">
                    {browseItems.map((item) => (
                      <Button
                        key={item.label}
                        variant="ghost"
                        className="w-full justify-start"
                      >
                        <item.icon className="mr-2 h-4 w-4" />
                        {item.label}
                      </Button>
                    ))}
                  </div>
                </div>
                <div className="px-3 py-2">
                  <h2 className="mb-2 px-4 text-lg font-semibold">Discover</h2>
                  <div className="space-y-1">
                    {discoverItems.map((item) => (
                      <Button
                        key={item.label}
                        variant="ghost"
                        className="w-full justify-start"
                      >
                        <item.icon className="mr-2 h-4 w-4" />
                        {item.label}
                      </Button>
                    ))}
                  </div>
                </div>
                <div className="px-3 py-2">
                  <h2 className="mb-2 px-4 text-lg font-semibold">스트리밍</h2>
                  <div className="grid grid-cols-2 gap-2">
                    {streamingPlatforms.map((platform) => (
                      <Button
                        key={platform.label}
                        variant="ghost"
                        className="w-full justify-start"
                      >
                        <Image
                          src={platform.icon}
                          alt={platform.label}
                          className="mr-2 h-4 w-4"
                        />
                        {platform.label}
                      </Button>
                    ))}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-3 lg:col-span-4 lg:border-l overflow-auto">
              <div className="h-full px-4 py-6 lg:px-8">
                <ScrollArea className="h-full">
                  <div className="space-y-8 pb-20">
                    <TopMusic />
                    <PopularTracks />
                    <RecommendedVideos />
                    {children}
                  </div>
                </ScrollArea>
              </div>
            </div>
          </div>
        </div>
        <MusicPlayer />
      </div>
    </MusicProvider>
  )
}
