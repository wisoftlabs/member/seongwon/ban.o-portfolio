"use client"

import { useState, useEffect } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { X } from 'lucide-react'
import { motion, AnimatePresence } from 'framer-motion'

export default function TicketModal() {
  const [isOpen, setIsOpen] = useState(false)
  const [mounted, setMounted] = useState(false)
  const [imageLoaded, setImageLoaded] = useState(false)

  useEffect(() => {
    setMounted(true)
    const checkShowModal = () => {
      const hasSeenModal = localStorage.getItem('hasSeenTicketModal')
      const endDate = new Date('2025-02-22T09:00:00+09:00') // 한국 시간 2025년 2월 22일 오전 9시
      const now = new Date()

      if (now >= endDate) {
        setIsOpen(false)
        return
      }

      if (!hasSeenModal) {
        setIsOpen(true)
      }
    }

    checkShowModal()

    // 매 분마다 체크 (시간이 지나면 자동으로 모달이 닫힘)
    const interval = setInterval(checkShowModal, 60000)

    return () => clearInterval(interval)
  }, [])

  const handleClose = () => {
    setIsOpen(false)
    localStorage.setItem('hasSeenTicketModal', 'true')
  }

  const handleKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'Escape') {
      handleClose()
    }
  }

  useEffect(() => {
    if (isOpen) {
      document.body.style.overflow = 'hidden'
      window.addEventListener('keydown', handleKeyDown)
    } else {
      document.body.style.overflow = 'unset'
    }

    return () => {
      document.body.style.overflow = 'unset'
      window.removeEventListener('keydown', handleKeyDown)
    }
  }, [isOpen])

  if (!mounted) return null

  return (
    <AnimatePresence>
      {isOpen && (
        <div className="fixed inset-0 z-50 overflow-y-auto">
          <div className="min-h-screen px-4 text-center">
            {/* Background overlay */}
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              className="fixed inset-0 bg-black/70 backdrop-blur-sm"
              onClick={handleClose}
              aria-hidden="true"
            />

            {/* Modal positioning trick */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>

            {/* Modal */}
            <motion.div
              initial={{ opacity: 0, scale: 0.95, y: -20 }}
              animate={{ opacity: 1, scale: 1, y: 0 }}
              exit={{ opacity: 0, scale: 0.95, y: 20 }}
              transition={{ type: "spring", duration: 0.5 }}
              className="relative inline-block w-full max-w-2xl my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl"
            >
              {/* Close button */}
              <button
                onClick={handleClose}
                className="absolute right-4 top-4 z-10 p-2 rounded-full bg-black/20 hover:bg-black/30 transition-colors"
              >
                <X className="w-5 h-5 text-white" />
              </button>

              {/* Banner Image */}
              <div className="w-full bg-blue-600">
                <Image
                  src="/images/WannaTravel_TicketNotice.png"
                  alt="우리여행갈까?(離陸이륙) 티켓 신청 안내"
                  width={1200}
                  height={300}
                  priority
                  className="w-full h-auto"
                />
              </div>

              {/* Content */}
              <div className="p-6 space-y-4">
                <h3 className="text-2xl font-bold text-gray-900">
                  우리여행갈까?(離陸이륙) 티켓 신청
                </h3>
                <div className="grid grid-cols-1 gap-3 text-gray-600">
                  <p className="flex items-start gap-2">
                    <span className="font-medium min-w-[80px]">• 공연일:</span>
                    <span>2025.02.22</span>
                  </p>
                  <p className="flex items-start gap-2">
                    <span className="font-medium min-w-[80px]">• 시작 시간:</span>
                    <span>20시00분</span>
                  </p>
                  <p className="flex items-start gap-2">
                    <span className="font-medium min-w-[80px]">• 입장 마감:</span>
                    <span>19시50분</span>
                  </p>
                  <p className="flex items-start gap-2">
                    <span className="font-medium min-w-[80px]">• 장소:</span>
                    <span className="flex-1">인천 남동구 문화로 119-1 지하층 나무아이라이브홀 (130석)</span>
                  </p>
                  <p className="flex items-start gap-2">
                    <span className="font-medium min-w-[80px]">• 주관:</span>
                    <span>JC ART COMPANY (제이씨아트컴퍼니)</span>
                  </p>
                  <p className="flex items-start gap-2">
                    <span className="font-medium min-w-[80px]">• 관람 문의:</span>
                    <span>010-8388-3748</span>
                  </p>
                </div>

                <div className="flex gap-4 pt-4">
                  <Link 
                    href="https://form.naver.com/response/FwB_hWdx8J1LTrT1v01xfw"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="flex-1 bg-purple-600 text-white text-center py-3 px-4 rounded-lg font-medium hover:bg-purple-700 transition-colors active:scale-[0.98]"
                  >
                    티켓 신청하기
                  </Link>
                  <button
                    onClick={handleClose}
                    className="flex-1 bg-gray-100 text-gray-800 py-3 px-4 rounded-lg font-medium hover:bg-gray-200 transition-colors active:scale-[0.98]"
                  >
                    닫기
                  </button>
                </div>
              </div>
            </motion.div>
          </div>
        </div>
      )}
    </AnimatePresence>
  )
} 