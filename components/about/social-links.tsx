import { Twitter, Instagram, Youtube, Music } from 'lucide-react'

const socialLinks = [
  {
    name: "Instagram",
    url: "https://www.instagram.com/_.ban.o_/",
    icon: Instagram
  },
  {
    name: "YouTube",
    url: "https://www.youtube.com/@ban.0bob0227",
    icon: Youtube
  },
  {
    name: "Apple Music",
    url: "https://music.apple.com/kr/artist/%EB%B0%98%EC%98%A4/1623132013",
    icon: Music
  }
]

export function SocialLinks() {
  return (
    <section className="py-16 flex flex-col items-center text-center">
      <h2 className="text-2xl font-bold mb-4">반오와 연결하기</h2>
      <p className="text-gray-600 mb-8">
        아래 링크를 통해 반오의 음악과 일상을 더 자세히 만나보세요.
      </p>
      <div className="flex justify-center gap-6">
        {socialLinks.map((link) => {
          const Icon = link.icon
          return (
            <a
              key={link.name}
              href={link.url}
              target="_blank"
              rel="noopener noreferrer"
              className="p-3 rounded-full bg-gray-100 hover:bg-gray-200 transition-colors"
              aria-label={link.name}
            >
              <Icon className="w-6 h-6" />
            </a>
          )
        })}
      </div>
    </section>
  )
}
