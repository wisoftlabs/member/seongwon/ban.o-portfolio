export function AboutDiscography() {
  const discography = [
    { date: "2022년 5월 10일", title: "두려움", type: "TITLE" },
    { date: "2024년 4월 25일", title: "먼지 쌓인 나의 앨범 속에는", type: "TITLE" },
    { date: "2024년 5월 23일", title: "Welcome to Alpcaca World", type: "수록곡", album: "2024 알파카월드 로고송 공모전 수상작" },
    { date: "2024년 5월 29일", title: "꽃길 꽃신", type: "TITLE" },
  ]

  return (
    <section className="py-16">
      <h2 className="text-4xl font-bold mb-8">디스코그래피</h2>
      <div className="space-y-6">
        {discography.map((item, index) => (
          <div key={index} className="bg-gray-50 p-6 rounded-lg">
            <p className="text-sm text-gray-500">{item.date}</p>
            <h3 className="text-xl font-semibold mt-2">{item.title}</h3>
            <p className="text-md text-gray-700 mt-1">{item.type} {item.album && `- ${item.album}`}</p>
          </div>
        ))}
      </div>
    </section>
  )
}

