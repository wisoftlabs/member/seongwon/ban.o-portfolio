export function AboutPersonal() {
  return (
    <section className="py-16">
      <h2 className="text-4xl font-bold mb-8 text-center">INFORMATION</h2>
      <div className="max-w-4xl mx-auto">
        <div className="grid md:grid-cols-2 gap-8">
          <div className="text-center">
            <h3 className="text-2xl font-semibold mb-4">기본 정보</h3>
            <ul className="space-y-2 text-gray-700">
              <li><strong>본명:</strong> 이상훈</li>
              <li><strong>출생:</strong> 1995년 2월 9일 (29세)</li>
              <li><strong>국적:</strong> 대한민국</li>
              <li><strong>신체:</strong> 172cm | 65kg</li>
              <li><strong>가족:</strong> 부모님, 여동생, 콩이(반려견)</li>
              <li><strong>학력:</strong> 국제예술대학교 실용음악과 보컬전공 졸업</li>
              <li><strong>MBTI:</strong> ENFJ</li>
            </ul>
          </div>
          <div className="text-center">
            <h3 className="text-2xl font-semibold mb-4">취향</h3>
            <ul className="space-y-2 text-gray-700">
              <li><strong>좋아하는 음식:</strong> 피자 (특히 페퍼로니 피자)</li>
              <li><strong>좋아하는 술:</strong> 맥주</li>
              <li><strong>커피:</strong> 드립커피 애호가, 바리스타 자격증 보유</li>
              <li><strong>싫어하는 것:</strong> 하와이안 피자, 소주, 스타벅스 커피</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  )
}
