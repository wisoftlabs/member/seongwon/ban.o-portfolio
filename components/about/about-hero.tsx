import Image from "next/image"

export function AboutHero() {
  return (
    <section className="relative min-h-[80vh] flex items-center mb-16">
      <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-[500px] h-[500px] rounded-full bg-[#E5DDD3] opacity-50" />
      <div className="grid lg:grid-cols-2 gap-12 items-center relative">
        <div className="space-y-6">
          <h1 className="text-6xl font-bold tracking-tight">
            안녕하세요!
            <br />반오입니다.
          </h1>
          <p className="text-xl text-gray-600 max-w-lg">
            사람들의 소소한 꿈을 응원하는 반오입니다.😆
          </p>
        </div>
        <div className="relative">
          <div className="relative aspect-[3/4] rounded-2xl overflow-hidden">
            <Image
              src="https://cdn.ban-o.art/public/about/about-main.jpg"
              alt="BanO Profile"
              fill
              className="object-cover"
              sizes="(max-width: 768px) 100vw, 50vw"
              priority
            />
          </div>
        </div>
      </div>
    </section>
  )
}
