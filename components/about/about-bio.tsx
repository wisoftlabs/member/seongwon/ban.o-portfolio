import Image from "next/image"

export function AboutBio() {
  return (
    <section className="py-16">
      <div className="grid lg:grid-cols-2 gap-12">
        <div>
          <h2 className="text-4xl font-bold mb-8">소개</h2>
          <div className="space-y-4 text-lg text-gray-700">
            <p>
              반오는 대한민국의 싱어송라이터입니다. 가수 예명은 한글로 반오, 영어로는 Ban.O입니다.
            </p>
            <p>
              고등학교 1학년 때 집에 있던 기타를 우연히 손에 잡게 되면서 음악에 대한 흥미가 생겼고, 이 계기로 밴드부에 들어가게 되었습니다. 대학에서는 군사학과에 진학하며 음악을 취미로만 즐기기로 결심하고 직업군인의 꿈을 꾸기 시작했으나, 음악을 포기하면 평생 후회할 것 같아 25살, 반오십의 나이에 본격적으로 음악 활동을 시작하게 되었습니다.
            </p>
          </div>
        </div>
        <div className="grid grid-cols-2 gap-4 h-fit relative">
          <div className="relative aspect-[3/4] rounded-lg overflow-hidden">
            <Image
              src="https://cdn.ban-o.art/public/about/about-1.jpg"
              alt="Ban.O with guitar"
              fill
              style={{ objectFit: "cover" }}
              sizes="(max-width: 1024px) 100vw, 50vw"
              priority
            />
          </div>
          <div className="relative aspect-[3/4] rounded-lg overflow-hidden">
            <Image
              src="https://cdn.ban-o.art/public/about/about-2.jpg"
              alt="Ban.O casual portrait"
              fill
              style={{ objectFit: "cover" }}
              sizes="(max-width: 1024px) 100vw, 50vw"
            />
          </div>
        </div>
      </div>
    </section>
  )
}
