export function AboutCareer() {
  return (
    <section className="py-16">
      <h2 className="text-4xl font-bold mb-8">경력</h2>
      <ul className="space-y-4 text-lg text-gray-700 list-disc list-inside">
        <li>2025년 2월 26일 반오밴드 결성 및 첫 싱글 발표 예정</li>
        <li>현재 JC 엔터테이먼트 소속</li>
        <li>숨은 뮤지션 발굴 프로젝트 &#39;히든스테이지 2탄&#39; 참가</li>
        <li>이만복의 음악복덕방 시즌1 작곡가로 참가</li>
        <li>2022년 싱글앨범 &lt;두려움&gt;을 통해 공식적으로 데뷔</li>
        <li>2020년 국제예술대학교 실용음악과 보컬전공 수시 1차 합격</li>
      </ul>
    </section>
  )
}
