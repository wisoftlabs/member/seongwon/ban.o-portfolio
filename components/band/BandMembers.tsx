"use client"

import { motion } from "framer-motion"
import Image from "next/image"
import Link from "next/link"
import { Instagram } from "lucide-react"

const members = [
  {
    name: "반오",
    role: "Vocal",
    image: "https://cdn.ban-o.art/public/bob/vocal.jpg",
  },
  {
    name: "현수",
    role: "Guitar",
    image: "https://cdn.ban-o.art/public/bob/guitar.jpg",
  },
  {
    name: "정현",
    role: "Drum",
    image: "https://cdn.ban-o.art/public/bob/drum.jpg",
  },
  {
    name: "인혁",
    role: "Piano",
    image: "https://cdn.ban-o.art/public/bob/piano.jpg",
  },
  {
    name: "윤호",
    role: "Bass",
    image: "https://cdn.ban-o.art/public/bob/bass.jpg",
  },
]

export function BandMembers() {
  return (
    <section className="py-12 md:py-24 bg-gradient-to-b from-purple-900/5 to-transparent">
      <div className="container px-4 mx-auto">
        <div className="flex flex-col lg:flex-row lg:gap-12 xl:gap-20">
          {/* Left Column - Band Logo and Info */}
          <div className="flex flex-col items-center text-center mb-12 lg:mb-0 lg:w-1/3">
            <motion.div
              initial={{ opacity: 0, y: 20 }}
              whileInView={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.5 }}
              viewport={{ once: true }}
              className="relative w-48 md:w-72 lg:w-full max-w-sm aspect-square mb-8"
            >
              <Image
                src="https://cdn.ban-o.art/public/bob/bano-band.png"
                alt="반오밴드 B.O.B (Ban.O Band)"
                fill
                className="object-contain"
                sizes="(max-width: 768px) 192px, (max-width: 1024px) 288px, 384px"
                priority
              />
            </motion.div>
            
            <motion.div
              initial={{ opacity: 0, y: 20 }}
              whileInView={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.5, delay: 0.2 }}
              viewport={{ once: true }}
              className="space-y-4"
            >
              <div className="space-y-2">
                <h2 className="text-3xl md:text-5xl font-bold tracking-tight">
                  반오밴드 B.O.B
                </h2>
                <p className="text-lg md:text-2xl text-gray-600">
                  Ban.O Band
                </p>
              </div>
              <Link 
                href="https://www.instagram.com/_ban.o.band_/"
                target="_blank"
                rel="noopener noreferrer"
                className="inline-flex items-center gap-2 text-purple-500 hover:text-purple-600 transition-colors"
              >
                <Instagram className="w-5 h-5" />
                <span className="text-lg">@_ban.o.band_</span>
              </Link>
            </motion.div>
          </div>

          {/* Right Column - Member Photos */}
          <div className="lg:w-2/3">
            {/* Top Row - 3 Members */}
            <div className="grid grid-cols-1 sm:grid-cols-3 gap-6 md:gap-8 lg:gap-6 mb-6 md:mb-8 lg:mb-6">
              {members.slice(0, 3).map((member, index) => (
                <motion.div
                  key={member.role}
                  initial={{ opacity: 0, y: 20 }}
                  whileInView={{ opacity: 1, y: 0 }}
                  transition={{ duration: 0.5, delay: index * 0.1 }}
                  viewport={{ once: true }}
                  className="group"
                >
                  <div className="relative aspect-square rounded-2xl overflow-hidden mb-4 bg-purple-100 shadow-lg">
                    <Image
                      src={member.image}
                      alt={`${member.name} - ${member.role}`}
                      fill
                      className="object-cover transition-transform duration-300 group-hover:scale-105"
                      sizes="(max-width: 768px) 100vw, (max-width: 1200px) 33vw, 25vw"
                    />
                    <div className="absolute inset-0 bg-gradient-to-t from-purple-900/40 via-transparent to-transparent opacity-0 group-hover:opacity-100 transition-opacity duration-300" />
                  </div>
                  <div className="text-center">
                    <h3 className="text-2xl font-bold mb-1 group-hover:text-purple-600 transition-colors">
                      {member.name}
                    </h3>
                    <p className="text-gray-600 text-lg">{member.role}</p>
                  </div>
                </motion.div>
              ))}
            </div>

            {/* Bottom Row - 2 Members */}
            <div className="grid grid-cols-1 sm:grid-cols-2 gap-6 md:gap-8 lg:gap-6 max-w-2xl mx-auto">
              {members.slice(3).map((member, index) => (
                <motion.div
                  key={member.role}
                  initial={{ opacity: 0, y: 20 }}
                  whileInView={{ opacity: 1, y: 0 }}
                  transition={{ duration: 0.5, delay: 0.3 + index * 0.1 }}
                  viewport={{ once: true }}
                  className="group"
                >
                  <div className="relative aspect-square rounded-2xl overflow-hidden mb-4 bg-purple-100 shadow-lg">
                    <Image
                      src={member.image}
                      alt={`${member.name} - ${member.role}`}
                      fill
                      className="object-cover transition-transform duration-300 group-hover:scale-105"
                      sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
                    />
                    <div className="absolute inset-0 bg-gradient-to-t from-purple-900/40 via-transparent to-transparent opacity-0 group-hover:opacity-100 transition-opacity duration-300" />
                  </div>
                  <div className="text-center">
                    <h3 className="text-2xl font-bold mb-1 group-hover:text-purple-600 transition-colors">
                      {member.name}
                    </h3>
                    <p className="text-gray-600 text-lg">{member.role}</p>
                  </div>
                </motion.div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  )
} 