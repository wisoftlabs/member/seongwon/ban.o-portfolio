"use client"

import * as React from "react"
import Link from "next/link"
import { Menu, X } from 'lucide-react'
import { Button } from "@/components/ui/button"
import { motion, AnimatePresence } from "framer-motion"
import {
  NavigationMenu,
  NavigationMenuContent,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  NavigationMenuTrigger,
  navigationMenuTriggerStyle,
} from "@/components/ui/navigation-menu"

const musicLinks = [
  { title: "홈", href: "/music" },
  { title: "플레이리스트", href: "/music/playlists" },
  { title: "앨범", href: "/music/albums" },
]

const videoLinks = [
  { title: "커버 노래", href: "/videos/covers" },
  { title: "숏츠 영상", href: "/videos/shorts" },
  { title: "출연 영상", href: "/videos/appearances" },
]

export function Header() {
  const [isOpen, setIsOpen] = React.useState(false)

  return (
    <header className="fixed top-0 w-full z-50 bg-white bg-opacity-90 backdrop-blur-sm border-b">
      <div className="container flex items-center justify-between h-16 px-4">
        <Link href="/" className="text-xl font-bold tracking-tight">
          Ban.O Band
        </Link>
        
        {/* Mobile Menu Button */}
        <button
          className="lg:hidden p-2 -mr-2"
          onClick={() => setIsOpen(!isOpen)}
          aria-label="Toggle menu"
        >
          {isOpen ? (
            <X className="h-6 w-6" />
          ) : (
            <Menu className="h-6 w-6" />
          )}
        </button>

        {/* Desktop Navigation */}
        <NavigationMenu className="hidden lg:flex ml-auto mr-4">
          <NavigationMenuList>
            <NavigationMenuItem>
              <NavigationMenuTrigger>음악</NavigationMenuTrigger>
              <NavigationMenuContent>
                <ul className="grid w-[200px] gap-2 p-4">
                  {musicLinks.map((link) => (
                    <li key={link.href}>
                      <Link
                        href={link.href}
                        className="block select-none space-y-1 rounded-md p-3 leading-none no-underline outline-none transition-colors hover:bg-accent hover:text-accent-foreground focus:bg-accent focus:text-accent-foreground"
                      >
                        {link.title}
                      </Link>
                    </li>
                  ))}
                </ul>
              </NavigationMenuContent>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <NavigationMenuTrigger>영상</NavigationMenuTrigger>
              <NavigationMenuContent>
                <ul className="grid w-[200px] gap-2 p-4">
                  {videoLinks.map((link) => (
                    <li key={link.href}>
                      <Link
                        href={link.href}
                        className="block select-none space-y-1 rounded-md p-3 leading-none no-underline outline-none transition-colors hover:bg-accent hover:text-accent-foreground focus:bg-accent focus:text-accent-foreground"
                      >
                        {link.title}
                      </Link>
                    </li>
                  ))}
                </ul>
              </NavigationMenuContent>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <NavigationMenuLink href="/performance" className={navigationMenuTriggerStyle()}>
                공연
              </NavigationMenuLink>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <NavigationMenuLink href="/gallery" className={navigationMenuTriggerStyle()}>
                갤러리
              </NavigationMenuLink>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <NavigationMenuLink href="/news" className={navigationMenuTriggerStyle()}>
                소식
              </NavigationMenuLink>
            </NavigationMenuItem>
            <NavigationMenuItem>
              <NavigationMenuLink href="/about" className={navigationMenuTriggerStyle()}>
                소개
              </NavigationMenuLink>
            </NavigationMenuItem>
          </NavigationMenuList>
        </NavigationMenu>

        <Button 
          variant="outline" 
          className="ml-4 hidden lg:inline-flex"
        >
          <Link href="/contact" className="w-full h-full flex items-center justify-center">
            연락처
          </Link>
        </Button>
      </div>

      {/* Mobile Navigation Dropdown */}
      <AnimatePresence>
        {isOpen && (
          <motion.div
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: "auto" }}
            exit={{ opacity: 0, height: 0 }}
            transition={{ duration: 0.2 }}
            className="lg:hidden border-t bg-white"
          >
            <nav className="container flex flex-col px-4 py-4">
              <div className="py-2">
                <h3 className="font-semibold mb-2">음악</h3>
                {musicLinks.map((link) => (
                  <Link
                    key={link.href}
                    href={link.href}
                    className="block py-2 text-sm hover:text-gray-600 transition-colors"
                    onClick={() => setIsOpen(false)}
                  >
                    {link.title}
                  </Link>
                ))}
              </div>
              <div className="py-2">
                <h3 className="font-semibold mb-2">영상</h3>
                {videoLinks.map((link) => (
                  <Link
                    key={link.href}
                    href={link.href}
                    className="block py-2 text-sm hover:text-gray-600 transition-colors"
                    onClick={() => setIsOpen(false)}
                  >
                    {link.title}
                  </Link>
                ))}
              </div>
              <Link 
                href="/performance" 
                className="py-3 text-sm hover:text-gray-600 transition-colors"
                onClick={() => setIsOpen(false)}
              >
                공연
              </Link>
              <Link 
                href="/gallery" 
                className="py-3 text-sm hover:text-gray-600 transition-colors"
                onClick={() => setIsOpen(false)}
              >
                갤러리
              </Link>
              <Link 
                href="/news" 
                className="py-3 text-sm hover:text-gray-600 transition-colors"
                onClick={() => setIsOpen(false)}
              >
                소식
              </Link>
              <Link 
                href="/about" 
                className="py-3 text-sm hover:text-gray-600 transition-colors"
                onClick={() => setIsOpen(false)}
              >
                소개
              </Link>
              <Button 
                variant="outline" 
                className="mt-3"
                onClick={() => setIsOpen(false)}
              >
                <Link href="/contact" className="w-full h-full flex items-center justify-center">
                  연락처
                </Link>
              </Button>
            </nav>
          </motion.div>
        )}
      </AnimatePresence>
    </header>
  )
}

