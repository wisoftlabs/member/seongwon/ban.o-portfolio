"use client"

import { motion } from "framer-motion"
import Link from "next/link"
import { CalendarDays } from 'lucide-react'

const posts = [
  { 
    id: 1, 
    title: "반오밴드(Ban.O Band) - '하나의 별똥별 처럼' Official MV 공개", 
    date: "2025-02-26",
    excerpt: "반오밴드의 신곡 '하나의 별똥별 처럼'이 공개되었습니다. 밤하늘에 쏟아지는 별똥별처럼 짧지만 강렬한 순간을 담은 이 곡은 반오밴드의 서정적인 보컬과 아름다운 멜로디로 많은 이들의 마음을 사로잡고 있습니다. 유튜브에서 뮤직비디오를 감상해보세요.",
    link: "https://www.youtube.com/watch?v=X7UolnJ-wyo",
  },
  { 
    id: 2, 
    title: "밴드와 연극의 낭만적 만남, '우리 여행갈까' 공연 22일 개최", 
    date: "2025-02-07",
    excerpt: "제이씨아트컴퍼니는 오는 22일 오후 8시 남동구 나무아이아트홀에서 '우리, 여행갈까? – 첫번째 이야기: 이륙' 공연을 진행합니다. 연극과 밴드 음악을 결합한 독창적인 형식으로, 마치 비행기에 탑승한 듯한 무대 연출을 통해 관객들에게 여행의 설렘과 감동을 선사합니다.",
    link: "https://www.kgnews.co.kr/news/article.html?no=829655",
  },
  { 
    id: 3, 
    title: "하로 최승일의 봉사리팝스 출연", 
    date: "2024-06-24",
    excerpt: "반오의 자작곡 '먼지 쌓인 나의 앨범 속에는'을 라디오 GFM에서 라이브로 선보입니다.",
    link: "http://www.radiogfm.net/news/23290",
  },
  { 
    id: 4, 
    title: "새 앨범 '꽃길 꽃신' 발매 안내", 
    date: "2024-05-29",
    excerpt: "반오의 3번째 싱글앨범 꽃길꽃신이 발매되었습니다! 제 노래가 꽃신이 되어 여러분들을 꽃길로 모시겠습니다.",
    link: "https://www.music-flo.com/detail/album/425383645/albumtrack",
  },
  { 
    id: 5, 
    title: "뉴스핌 싱송라 '히든스테이지' 본선 11주차...포크싱어 '반오' 출격", 
    date: "2024-06-20",
    excerpt: "기타 한 대로 자신의 이야기를 전하는 행복 전도사. 21일 오후 4시 10분 유튜브채널 뉴스핌TV서 공개",
    link: "https://www.newspim.com/news/view/20240619000571",
  },
  { 
    id: 6, 
    title: "가슴 벅차오르는 청춘을 기록하다. 싱어송라이터 반오 (Ban.O)", 
    date: "2023-04-21",
    excerpt: "오늘 소개할 아티스트는 '청춘'이라는 단어가 가장 잘 어울리는 꿈을 노래하는 싱어송라이터 '반오 (Ban.O)'입니다.",
    link: "https://breakevenpoint.tistory.com/211",
  },
]

export function RecentPosts() {
  return (
    <section className="mb-16">
      <h2 className="text-3xl font-bold mb-8">최근 소식</h2>
      <div className="space-y-8">
        {posts.map((post, index) => (
          <motion.div
            key={post.id}
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.5, delay: index * 0.1 }}
          >
            <Link href={post.link} target="_blank" className="block bg-white p-6 rounded-lg shadow-md hover:shadow-lg transition-shadow">
              <h3 className="text-xl font-semibold mb-2">{post.title}</h3>
              <div className="flex items-center text-sm text-gray-500 mb-3">
                <CalendarDays className="w-4 h-4 mr-1" />
                {post.date}
              </div>
              <p className="text-gray-600">{post.excerpt}</p>
            </Link>
          </motion.div>
        ))}
      </div>
    </section>
  )
}
