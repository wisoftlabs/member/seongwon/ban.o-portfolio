"use client"

import { motion } from "framer-motion"
import { Twitter, Instagram } from 'lucide-react'

const socialPosts = [
  {
    id: 5,
    platform: "instagram",
    content: "장발 포기!",
    date: "2025-01-21"
  },
  {
    id: 4,
    platform: "instagram",
    content: "업로드가 늦는 편...",
    date: "2025-01-18"
  },
  {
    id: 1,
    platform: "instagram",
    content: "동인천 왕중왕전!! 왕은 되지 못했지만 그래도 무대에 설 수 있다는게 행복했다 😆",
    date: "2024-10-09"
  },
  {
    id: 3,
    platform: "instagram",
    content: "펍 캠프마켓 관객분들이 엄청 좋아하셔서 저도 너무 행복한 하루였습니다! 너무 감사합니다 😆",
    date: "2024-08-16"
  },
  {
    id: 2,
    platform: "instagram",
    content: "넌 이름이 머야? 🐶",
    date: "2023-10-03"
  }
].sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime())

export function SocialFeed() {
  return (
    <section>
      <h2 className="text-3xl font-bold mb-8">소셜 미디어</h2>
      <div className="grid gap-6 md:grid-cols-2 lg:grid-cols-3">
        {socialPosts.map((post, index) => (
          <motion.div
            key={post.id}
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.5, delay: index * 0.1 }}
            className="bg-white p-6 rounded-lg shadow-md"
          >
            <div className="flex items-center mb-4">
              {post.platform === "twitter" ? (
                <Twitter className="w-5 h-5 text-blue-400 mr-2" />
              ) : (
                <Instagram className="w-5 h-5 text-pink-600 mr-2" />
              )}
              <span className="text-sm text-gray-500">{post.date}</span>
            </div>
            <p className="text-gray-800">{post.content}</p>
          </motion.div>
        ))}
      </div>
    </section>
  )
}
