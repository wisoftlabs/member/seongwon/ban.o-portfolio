// "use client"

// import { useState } from "react"
// import { motion } from "framer-motion"
// import { Button } from "@/components/ui/button"
// import { Input } from "@/components/ui/input"
// import { Textarea } from "@/components/ui/textarea"
// import { Card, CardContent } from "@/components/ui/card"

// export function FanInteraction() {
//   const [email, setEmail] = useState("")
//   const [message, setMessage] = useState("")

//   return (
//     <section id="contact" className="py-24 bg-gray-50">
//       <div className="container px-4">
//         <div className="max-w-2xl mx-auto">
//           <motion.div
//             initial={{ opacity: 0, y: 20 }}
//             whileInView={{ opacity: 1, y: 0 }}
//             transition={{ duration: 0.5 }}
//             viewport={{ once: true }}
//             className="text-center space-y-4 mb-16"
//           >
//             <div className="inline-block rounded-full bg-gray-200 px-3 py-1 text-sm text-gray-600">
//               Fan Club
//             </div>
//             <h2 className="text-4xl font-bold tracking-tight text-gray-900">팬들과 소통</h2>
//             <p className="text-gray-600">
//               BanO와 소통하고 싶은 이야기가 있다면 남겨주세요.
//             </p>
//           </motion.div>
//           <motion.div
//             initial={{ opacity: 0, y: 20 }}
//             whileInView={{ opacity: 1, y: 0 }}
//             transition={{ duration: 0.5, delay: 0.2 }}
//             viewport={{ once: true }}
//           >
//             <Card>
//               <CardContent className="p-6 space-y-4">
//                 <div className="space-y-2">
//                   <Input
//                     placeholder="이메일"
//                     type="email"
//                     value={email}
//                     onChange={(e) => setEmail(e.target.value)}
//                   />
//                 </div>
//                 <div className="space-y-2">
//                   <Textarea
//                     placeholder="메시지를 입력하세요"
//                     value={message}
//                     onChange={(e) => setMessage(e.target.value)}
//                   />
//                 </div>
//                 <Button className="w-full">보내기</Button>
//               </CardContent>
//             </Card>
//           </motion.div>
//           <motion.div
//             initial={{ opacity: 0, y: 20 }}
//             whileInView={{ opacity: 1, y: 0 }}
//             transition={{ duration: 0.5, delay: 0.4 }}
//             viewport={{ once: true }}
//             className="mt-16 text-center space-y-4"
//           >
//             <h3 className="text-xl font-bold">새 소식 받아보기</h3>
//             <div className="flex gap-4 max-w-md mx-auto">
//               <Input placeholder="이메일 주소" type="email" />
//               <Button>구독하기</Button>
//             </div>
//           </motion.div>
//         </div>
//       </div>
//     </section>
//   )
// }

