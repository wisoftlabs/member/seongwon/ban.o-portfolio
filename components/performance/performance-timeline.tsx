// "use client"

// import { motion, useScroll, useTransform } from "framer-motion"
// import { useRef } from "react"
// import { Calendar, MapPin, Music } from 'lucide-react'

// const timelineEvents = [
//   {
//     year: "2023",
//     events: [
//       {
//         title: "겨울 콘서트",
//         date: "2023.12.15",
//         venue: "서울 올림픽 공원",
//         description: "연말 스페셜 콘서트",
//       },
//       {
//         title: "대학 축제",
//         date: "2023.10.05",
//         venue: "서울대학교",
//         description: "가을 축제 헤드라이너",
//       },
//     ],
//   },
//   {
//     year: "2022",
//     events: [
//       {
//         title: "첫 단독 콘서트",
//         date: "2022.12.24",
//         venue: "YES24 라이브홀",
//         description: "크리스마스 이브 스페셜",
//       },
//       {
//         title: "버스킹 투어",
//         date: "2022.08.15",
//         venue: "전국 주요 도시",
//         description: "전국 버스킹 투어",
//       },
//     ],
//   },
// ]

// export function PerformanceTimeline() {
//   const containerRef = useRef<HTMLDivElement>(null)
//   const { scrollYProgress } = useScroll({
//     target: containerRef,
//     offset: ["start end", "end start"],
//   })

//   const opacity = useTransform(scrollYProgress, [0, 0.2], [0, 1])
//   const y = useTransform(scrollYProgress, [0, 0.2], [100, 0])

//   return (
//     <section ref={containerRef} className="py-20">
//       <motion.div style={{ opacity, y }} className="space-y-20">
//         <h2 className="text-3xl font-bold text-center mb-12">공연 연대기</h2>
//         {timelineEvents.map(yearGroup => (
//           <div key={yearGroup.year} className="relative">
//             <div className="sticky top-0 bg-white py-4 z-10">
//               <h3 className="text-2xl font-bold text-purple-600">{yearGroup.year}</h3>
//             </div>
//             <div className="mt-8 space-y-12">
//               {yearGroup.events.map((event, eventIndex) => (
//                 <motion.div
//                   key={event.title}
//                   initial={{ opacity: 0, x: -20 }}
//                   whileInView={{ opacity: 1, x: 0 }}
//                   transition={{ duration: 0.5, delay: eventIndex * 0.2 }}
//                   viewport={{ once: true }}
//                   className="relative pl-8 border-l-2 border-purple-200"
//                 >
//                   <div className="absolute -left-2 top-0 w-4 h-4 rounded-full bg-purple-600" />
//                   <div className="bg-white rounded-lg p-6 shadow-lg hover:shadow-xl transition-shadow">
//                     <h4 className="text-xl font-bold mb-2">{event.title}</h4>
//                     <div className="space-y-2 text-gray-600">
//                       <div className="flex items-center">
//                         <Calendar className="w-4 h-4 mr-2" />
//                         <span>{event.date}</span>
//                       </div>
//                       <div className="flex items-center">
//                         <MapPin className="w-4 h-4 mr-2" />
//                         <span>{event.venue}</span>
//                       </div>
//                       <div className="flex items-center">
//                         <Music className="w-4 h-4 mr-2" />
//                         <span>{event.description}</span>
//                       </div>
//                     </div>
//                   </div>
//                 </motion.div>
//               ))}
//             </div>
//           </div>
//         ))}
//       </motion.div>
//     </section>
//   )
// }
