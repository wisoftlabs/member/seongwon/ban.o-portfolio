"use client"

import { motion, AnimatePresence } from "framer-motion"
import Image from "next/image"
import { X } from "lucide-react"
import { useEffect, useState } from "react"

interface ImageViewerProps {
  isOpen: boolean
  onClose: () => void
  src: string
  alt: string
}

export function ImageViewer({ isOpen, onClose, src, alt }: ImageViewerProps) {
  const [isMobile] = useState(() => 
    typeof window !== 'undefined' && /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)
  )

  useEffect(() => {
    const handleEsc = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        onClose()
      }
    }

    if (isOpen) {
      window.addEventListener('keydown', handleEsc)
      // 모바일에서 스크롤 방지
      document.body.style.overflow = 'hidden'
    }

    return () => {
      window.removeEventListener('keydown', handleEsc)
      document.body.style.overflow = ''
    }
  }, [isOpen, onClose])

  return (
    <AnimatePresence>
      {isOpen && (
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: 0.2 }}
          className="fixed inset-0 z-50 flex items-center justify-center bg-black/90 backdrop-blur-sm p-4 touch-none"
          onClick={(e) => {
            if (e.target === e.currentTarget) {
              onClose()
            }
          }}
        >
          <motion.div
            initial={{ scale: 0.95, opacity: 0 }}
            animate={{ scale: 1, opacity: 1 }}
            exit={{ scale: 0.95, opacity: 0 }}
            transition={{ duration: 0.2 }}
            className="relative w-full h-full max-w-7xl max-h-[90vh]"
            onClick={(e) => e.stopPropagation()}
          >
            <Image
              src={src}
              alt={alt}
              fill
              className="object-contain"
              sizes={isMobile ? "100vw" : "(max-width: 768px) 100vw, 80vw"}
              priority
              quality={90}
            />
            <button
              onClick={onClose}
              className={`absolute ${isMobile ? 'top-6 right-6 p-3' : 'top-4 right-4 p-2'} 
                text-white/80 hover:text-white transition-colors z-50 
                bg-black/50 rounded-full backdrop-blur-sm touch-manipulation`}
              style={{ WebkitTapHighlightColor: 'transparent' }}
            >
              <X className={`${isMobile ? 'h-8 w-8' : 'h-6 w-6'}`} />
            </button>
          </motion.div>
        </motion.div>
      )}
    </AnimatePresence>
  )
}
