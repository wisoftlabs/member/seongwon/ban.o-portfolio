"use client"

import { Play } from 'lucide-react'
import { Button } from "@/components/ui/button"
import { motion } from "framer-motion"
import Image from "next/image"
import { useState, useEffect, useRef } from "react"

export function PerformanceHero() {
  const [videoError, setVideoError] = useState(false)
  const videoRef = useRef<HTMLVideoElement>(null)

  useEffect(() => {
    const video = videoRef.current
    if (video) {
      video.defaultMuted = true
      video.muted = true
      
      // iOS Safari에서의 자동 재생을 위한 처리
      const playVideo = async () => {
        try {
          await video.play()
        } catch (error) {
          console.error('Video autoplay failed:', error)
          setVideoError(true)
        }
      }
      
      playVideo()
    }
  }, [])

  return (
    <section className="relative h-screen min-h-[600px] flex items-center justify-center overflow-hidden">
      {!videoError ? (
        <video
          ref={videoRef}
          autoPlay
          playsInline
          loop
          muted
          preload="auto"
          onError={() => setVideoError(true)}
          className="absolute inset-0 w-full h-full object-cover"
        >
          <source src="https://cdn.ban-o.art/public/performance/20240720.mp4" type="video/mp4" />
        </video>
      ) : (
        <Image
          src="https://cdn.ban-o.art/public/main/hero.jpg"
          alt="Performance Hero"
          fill
          className="object-cover"
          sizes="100vw"
          priority
        />
      )}
      
      {/* 그라데이션 오버레이 추가 */}
      <div className="absolute inset-0 bg-gradient-to-t from-black/80 via-black/50 to-black/30"></div>
      
      <div className="relative z-10 text-center space-y-6 max-w-4xl mx-auto px-4">
        <motion.h1
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          className="text-4xl md:text-6xl font-bold text-white drop-shadow-lg"
        >
          BanO의 공연 아카이브
        </motion.h1>
        <motion.p
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: 0.2 }}
          className="text-lg md:text-xl text-gray-100 drop-shadow-lg"
        >
          지난 공연의 감동을 다시 한 번 느껴보세요
        </motion.p>
        <div
        >
          <a href="https://www.youtube.com/@%EB%B0%98%EC%98%A4%EC%8B%AD-Ban.O" target="_blank" rel="noopener noreferrer">
            <Button size="lg" className="rounded-full">
              <Play className="mr-2 h-5 w-5" />
              하이라이트 영상 보기
            </Button>
          </a>
        </div>
      </div>
    </section>
  )
}
