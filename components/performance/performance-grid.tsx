"use client"

import { useState, useRef } from "react"
import { motion, AnimatePresence } from "framer-motion"
import { Play, ChevronLeft, ChevronRight, X } from 'lucide-react'
import Image from "next/image"

interface Performance {
  id: number
  title: string
  description?: string
  artist?: string
  image: string
  youtubeUrl: string
  category?: string
  date?: string
}

const highlights = [
  {
    id: 1,
    title: "너무 아픈사랑은 사랑이 아니었음을",
    artist: "김광석",
    image: "https://img.youtube.com/vi/M3YHPLeKSaU/maxresdefault.jpg",
    youtubeUrl: "https://www.youtube.com/watch?v=M3YHPLeKSaU",
    category: "live",
    date: "2023.12",
  },
  {
    id: 2,
    title: "[히든스테이지] 스물두 번째 참가자 반오",
    description: "사람들의 마음을 응원하고 희망을 전하는 싱어송라이터 '반오'",
    image: "https://img.youtube.com/vi/rEI0Tv50LCU/maxresdefault.jpg",
    youtubeUrl: "https://www.youtube.com/watch?v=rEI0Tv50LCU",
  },
  {
    id: 3,
    title: "동인천 ART CUBE LIVE",
    description: "동인천 ART CUBE LIVE",
    image: "https://img.youtube.com/vi/BbWO5nyD0Sg/maxresdefault.jpg",
    youtubeUrl: "https://www.youtube.com/watch?v=BbWO5nyD0Sg",
  },
]

interface PerformanceGridProps {
  title?: string
}

export function PerformanceGrid({ title }: PerformanceGridProps) {
  const [selectedVideo, setSelectedVideo] = useState<string | null>(null)
  const scrollContainerRef = useRef<HTMLDivElement>(null)

  const getEmbedUrl = (url: string) => {
    const videoId = url.split('v=')[1]?.split('&')[0]
    if (!videoId) return url
    return `https://www.youtube.com/embed/${videoId}`
  }

  const scroll = (direction: 'left' | 'right') => {
    const container = scrollContainerRef.current
    if (!container) return

    const scrollAmount = 400
    container.scrollBy({
      left: direction === 'left' ? -scrollAmount : scrollAmount,
      behavior: 'smooth'
    })
  }

  const itemContent = (performance: Performance) => (
    <div className="absolute inset-0 bg-black/60 opacity-0 group-hover:opacity-100 transition-opacity duration-300">
      <div className="absolute inset-0 flex flex-col items-center justify-center text-white p-4">
        <Play className="w-16 h-16 mb-4" />
        <h3 className="text-xl font-bold text-center mb-2">{performance.title}</h3>
        {performance.artist && <p className="text-sm opacity-80 mb-1">{performance.artist}</p>}
        {performance.description && <p className="text-sm text-center opacity-80">{performance.description}</p>}
        {performance.date && <p className="text-sm opacity-80">반오 Live Clip | {performance.date}</p>}
      </div>
    </div>
  )

  return (
    <section className="py-20">
      <div className="flex items-center justify-between mb-8">
        {title && <h2 className="text-3xl font-bold">{title}</h2>}
        <div className="flex gap-2">
          <button
            onClick={() => scroll('left')}
            className="p-2 rounded-full hover:bg-gray-100 transition-colors"
          >
            <ChevronLeft className="w-6 h-6" />
          </button>
          <button
            onClick={() => scroll('right')}
            className="p-2 rounded-full hover:bg-gray-100 transition-colors"
          >
            <ChevronRight className="w-6 h-6" />
          </button>
        </div>
      </div>
      <div
        ref={scrollContainerRef}
        className="flex gap-6 overflow-x-auto scrollbar-hide pb-4 -mx-4 px-4"
      >
        {highlights.map((performance, index) => (
          <motion.div
            key={performance.id}
            initial={{ opacity: 0, x: 50 }}
            animate={{ opacity: 1, x: 0 }}
            transition={{ duration: 0.5, delay: index * 0.1 }}
            className="relative flex-shrink-0 w-[400px] aspect-video rounded-xl overflow-hidden cursor-pointer group"
            onClick={() => setSelectedVideo(performance.youtubeUrl)}
          >
            <Image
              src={performance.image}
              alt={performance.title}
              fill
              className="transition-transform duration-500 group-hover:scale-110 object-cover"
              sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 400px"
              priority={index === 0}
            />
            {itemContent(performance)}
          </motion.div>
        ))}
      </div>

      <AnimatePresence>
        {selectedVideo && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            className="fixed inset-0 z-50 flex items-center justify-center bg-black/90 p-4"
            onClick={(e) => {
              if (e.target === e.currentTarget) {
                setSelectedVideo(null)
              }
            }}
          >
            <button
              onClick={() => setSelectedVideo(null)}
              className="absolute top-4 right-4 text-white hover:text-gray-300 z-50"
            >
              <X className="w-6 h-6" />
            </button>
            <div className="relative w-full max-w-5xl aspect-video">
              <iframe
                src={selectedVideo ? getEmbedUrl(selectedVideo) : ''}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
                className="absolute inset-0 w-full h-full rounded-lg"
              />
            </div>
          </motion.div>
        )}
      </AnimatePresence>
    </section>
  )
}
