"use client"

import { motion, useScroll, useTransform } from "framer-motion"
import Image from "next/image"
import { useState, useRef } from "react"
import { ImageViewer } from "./image-viewer"

const performancePhotos = [
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_4225.jpeg",
    alt: "Performance Photo 1",
    width: 3,
    height: 4,
    featured: false
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3815.jpeg",
    alt: "Performance Photo 2",
    width: 3,
    height: 4,
    featured: true
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3759.jpeg",
    alt: "Performance Photo 3",
    width: 3,
    height: 4,
    featured: false
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3763.jpeg",
    alt: "Performance Photo 4",
    width: 3,
    height: 4,
    featured: true
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3770.jpeg",
    alt: "Performance Photo 5",
    width: 3,
    height: 4,
    featured: false
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3433.jpeg",
    alt: "Performance Photo 6",
    width: 3,
    height: 4,
    featured: false
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3443.jpeg",
    alt: "Performance Photo 7",
    width: 3,
    height: 4,
    featured: true
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3450.jpeg",
    alt: "Performance Photo 8",
    width: 3,
    height: 4,
    featured: false
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3419.jpeg",
    alt: "Performance Photo 9",
    width: 3,
    height: 4,
    featured: false
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3420.jpeg",
    alt: "Performance Photo 10",
    width: 3,
    height: 4,
    featured: true
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/IMG_3195.jpeg",
    alt: "Performance Photo 11",
    width: 3,
    height: 4,
    featured: false
  },
  { 
    src: "https://cdn.ban-o.art/public/performance/EC5E5FA0-F098-4B52-9EBB-7F40FB1BA701.jpg",
    alt: "Performance Photo 12",
    width: 3,
    height: 4,
    featured: false
  }
]

const container = {
  hidden: { opacity: 0 },
  show: {
    opacity: 1,
    transition: {
      staggerChildren: 0.1,
      delayChildren: 0.2,
    },
  },
}

const item = {
  hidden: { opacity: 0, y: 20 },
  show: { 
    opacity: 1, 
    y: 0,
    transition: {
      duration: 0.6,
      ease: [0.21, 0.47, 0.32, 0.98]
    }
  },
}

export function PerformancePhotos() {
  const containerRef = useRef<HTMLDivElement>(null)
  const [selectedImage, setSelectedImage] = useState<string | null>(null)
  const { scrollYProgress } = useScroll({
    target: containerRef,
    offset: ["start end", "end start"],
  })

  const opacity = useTransform(scrollYProgress, [0, 0.2], [0, 1])
  const y = useTransform(scrollYProgress, [0, 0.2], [100, 0])

  return (
    <section ref={containerRef} className="py-20">
      <motion.div style={{ opacity, y }} className="space-y-12">
        <motion.h2 
          initial={{ opacity: 0 }}
          whileInView={{ opacity: 1 }}
          transition={{ duration: 0.8, ease: "easeOut" }}
          className="text-3xl font-bold text-center"
        >
          공연 사진
        </motion.h2>
        <motion.div 
          variants={container}
          initial="hidden"
          whileInView="show"
          viewport={{ once: true, margin: "-100px" }}
          className="container mx-auto px-4"
        >
          <div className="columns-1 md:columns-2 lg:columns-3 xl:columns-4 gap-4 md:gap-6 [column-fill:_balance] mx-auto space-y-4 md:space-y-6">
            {performancePhotos.map((photo) => (
              <motion.div
                key={photo.src}
                variants={item}
                whileHover={{ scale: 1.02 }}
                whileTap={{ scale: 0.98 }}
                className={`relative break-inside-avoid-column overflow-hidden rounded-2xl cursor-pointer bg-gray-100 mb-4 md:mb-6 ${
                  photo.featured ? 'md:col-span-2' : ''
                }`}
                onClick={() => setSelectedImage(photo.src)}
                style={{
                  aspectRatio: `${photo.width} / ${photo.height}`,
                }}
              >
                <Image
                  src={photo.src}
                  alt={photo.alt}
                  fill
                  className="object-cover transition-all duration-300 hover:brightness-110"
                  sizes={photo.featured 
                    ? "(max-width: 768px) 100vw, (max-width: 1200px) 100vw, 50vw"
                    : "(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 25vw"
                  }
                  quality={90}
                />
                <motion.div 
                  initial={false}
                  whileHover={{ opacity: 1 }}
                  className="absolute inset-0 bg-gradient-to-t from-black/40 to-transparent opacity-0 transition-opacity duration-300"
                />
              </motion.div>
            ))}
          </div>
        </motion.div>
      </motion.div>

      <ImageViewer
        isOpen={!!selectedImage}
        onClose={() => setSelectedImage(null)}
        src={selectedImage || ""}
        alt="Performance photo"
      />
    </section>
  )
}
