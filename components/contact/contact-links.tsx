import { Youtube, Instagram, Music, CloudRain, Mail, Globe, Twitter, Facebook } from 'lucide-react'
import { Button } from "@/components/ui/button"

const links = [
  { name: 'YouTube', icon: Youtube, url: 'https://www.youtube.com/@ban.o' },
  { name: 'Instagram', icon: Instagram, url: 'https://www.instagram.com/ban.o_official' },
  { name: 'Spotify', icon: Music, url: 'https://open.spotify.com/artist/ban.o' },
  { name: 'SoundCloud', icon: CloudRain, url: 'https://soundcloud.com/ban.o' },
  { name: 'Twitter', icon: Twitter, url: 'https://twitter.com/ban_o_official' },
  { name: 'Facebook', icon: Facebook, url: 'https://facebook.com/ban.o.official' },
  { name: 'Official Website', icon: Globe, url: 'https://ban-o.art' },
  { name: 'Email', icon: Mail, url: 'mailto:contact@ban-o.art' },
]

export function ContactLinks() {
  return (
    <div className="space-y-4">
      {links.map((link) => (
        <Button
          key={link.name}
          variant="outline"
          className="w-full justify-start text-left font-normal"
          asChild
        >
          <a
            href={link.url}
            target="_blank"
            rel="noopener noreferrer"
            className="flex items-center space-x-3"
          >
            <link.icon className="h-5 w-5" />
            <span>{link.name}</span>
          </a>
        </Button>
      ))}
    </div>
  )
}

