"use client"

import { useState } from "react"
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { Textarea } from "@/components/ui/textarea"
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group"
import { Label } from "@/components/ui/label"
import { useToast } from "@/hooks/use-toast"
import { Loader2 } from "lucide-react"

type FormData = {
  name: string
  email: string
  phone: string
  subject: string
  message: string
}

export function ContactForm() {
  const { toast } = useToast()
  const [isLoading, setIsLoading] = useState(false)
  const [formData, setFormData] = useState<FormData>({
    name: "",
    email: "",
    phone: "",
    subject: "일반문의",
    message: ""
  })

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    setIsLoading(true)

    try {
      const response = await fetch('https://uvx2k5fchb.execute-api.ap-northeast-2.amazonaws.com/prod', {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify(formData),
      })

      if (!response.ok) {
        const errorData = await response.json()
        throw new Error(errorData.message || '메시지 전송에 실패했습니다.')
      }

      const responseData = await response.json()

      toast({
        title: "메시지가 전송되었습니다.",
        description: `${formData.name}님, 빠른 시일 내에 답변 드리겠습니다.`,
      })

      // Reset form
      setFormData({
        name: "",
        email: "",
        phone: "",
        subject: "일반문의",
        message: ""
      })
    } catch (error) {
      toast({
        title: "오류가 발생했습니다.",
        description: error instanceof Error ? error.message : "잠시 후 다시 시도해주세요.",
        variant: "destructive",
      })
    } finally {
      setIsLoading(false)
    }
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setFormData(prev => ({
      ...prev,
      [e.target.name]: e.target.value
    }))
  }

  return (
    <form onSubmit={handleSubmit} className="p-8 lg:p-12 bg-white">
      <div className="space-y-6">
        <div>
          <Label htmlFor="name">이름</Label>
          <Input
            id="name"
            name="name"
            value={formData.name}
            onChange={handleChange}
            className="mt-2"
            required
          />
        </div>
        <div>
          <Label htmlFor="email">이메일</Label>
          <Input
            id="email"
            name="email"
            type="email"
            value={formData.email}
            onChange={handleChange}
            className="mt-2"
            required
          />
        </div>
        <div>
          <Label htmlFor="phone">전화번호</Label>
          <Input
            id="phone"
            name="phone"
            type="tel"
            value={formData.phone}
            onChange={handleChange}
            className="mt-2"
          />
        </div>
      </div>

      <div className="mt-6">
        <Label>문의 유형</Label>
        <RadioGroup
          defaultValue="일반문의"
          className="grid grid-cols-2 gap-4 mt-2"
          onValueChange={(value) => setFormData(prev => ({ ...prev, subject: value }))}
        >
          <div className="flex items-center space-x-2">
            <RadioGroupItem value="일반문의" id="일반문의" />
            <Label htmlFor="일반문의">일반 문의</Label>
          </div>
          <div className="flex items-center space-x-2">
            <RadioGroupItem value="공연요청" id="공연요청" />
            <Label htmlFor="공연요청">공연 요청</Label>
          </div>
          <div className="flex items-center space-x-2">
            <RadioGroupItem value="음악협업" id="음악협업" />
            <Label htmlFor="음악협업">음악 협업</Label>
          </div>
          <div className="flex items-center space-x-2">
            <RadioGroupItem value="팬레터" id="팬레터" />
            <Label htmlFor="팬레터">팬레터</Label>
          </div>
        </RadioGroup>
      </div>

      <div className="mt-6">
        <Label htmlFor="message">메시지</Label>
        <Textarea
          id="message"
          name="message"
          value={formData.message}
          onChange={handleChange}
          className="mt-2"
          rows={4}
          required
        />
      </div>

      <Button type="submit" className="mt-6 w-full" disabled={isLoading}>
        {isLoading ? (
          <>
            <Loader2 className="mr-2 h-4 w-4 animate-spin" />
            전송 중...
          </>
        ) : (
          "메시지 보내기"
        )}
      </Button>
    </form>
  )
}
