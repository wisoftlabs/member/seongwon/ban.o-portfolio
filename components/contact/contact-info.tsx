"use client"

import { motion } from "framer-motion"
import { Youtube, Instagram, Music, CloudRain, Twitter, Facebook, Globe, Mail } from 'lucide-react'

const socialLinks = [
  { name: '유튜브', icon: Youtube, url: 'https://www.youtube.com/@ban.0bob0227' },
  { name: '인스타그램', icon: Instagram, url: 'https://www.instagram.com/_.ban.o_/' },
  { name: '스포티파이', icon: Music, url: 'https://open.spotify.com/artist/5pDSAehQ62k7sKHtZoXemq' },
  { name: '사운드클라우드', icon: CloudRain, url: 'https://soundcloud.com/ban-o' },
  { name: '공식 웹사이트', icon: Globe, url: 'https://ban-o.art' },
  { name: '이메일', icon: Mail, url: 'mailto:eag950209@naver.com' },
]

export function ContactInfo() {
  return (
    <div className="bg-black text-white p-8 lg:p-12">
      <div className="space-y-12">
        <h2 className="text-2xl font-bold mb-8">연락처 정보</h2>

        <div className="space-y-4">
          {socialLinks.map((link, index) => (
            <motion.a
              key={link.name}
              href={link.url}
              target="_blank"
              rel="noopener noreferrer"
              className="flex items-center space-x-4 p-4 rounded-lg bg-white/5 hover:bg-white/10 transition-colors"
              initial={{ opacity: 0, x: -20 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ delay: index * 0.1 }}
              whileHover={{ scale: 1.02 }}
              whileTap={{ scale: 0.98 }}
            >
              <link.icon className="w-5 h-5" />
              <span className="font-medium">{link.name}</span>
            </motion.a>
          ))}
        </div>
      </div>

      <div className="absolute bottom-0 right-0 w-40 h-40 bg-white/10 rounded-full -mr-20 -mb-20" />
      <div className="absolute bottom-0 right-0 w-32 h-32 bg-white/10 rounded-full -mr-16 -mb-16" />
    </div>
  )
}

