"use client"

import { useRef } from "react"
import Image from "next/image"
import { motion, useScroll, useTransform } from "framer-motion"
import { Button } from "@/components/ui/button"
import { Card, CardContent } from "@/components/ui/card"

const albums = [
  {
    title: "하나의 별똥별 처럼",
    cover: "https://cdn.ban-o.art/public/music/481924331_17996434796772524_343836101851192975_n.jpg",
    year: "2024",
    tracks: ["하나의 별똥별 처럼"],
  },
  {
    title: "꽃길 꽃신",
    cover: "https://cdn.ban-o.art/public/music/11499595_20240528170547_1000.jpg",
    year: "2024",
    tracks: ["꽃길 꽃신", "꽃길 꽃신 (Acoustic Ver.)", "꽃길 꽃신 (Inst.)"],
  },
  {
    title: "먼지 쌓인 나의 앨범 속에는",
    cover: "https://cdn.ban-o.art/public/music/11472610_20240424134200_1000.jpg",
    year: "2024",
    tracks: ["먼지 쌓인 나의 앨범 속에는", "먼지 쌓인 나의 앨범 속에는 (Inst.)"],
  },
  {
    title: "두려움",
    cover: "https://cdn.ban-o.art/public/music/10946867_20220509165214_1000.jpg",
    year: "2022",
    tracks: ["두려움"],
  },
]

export function Music() {
  const containerRef = useRef<HTMLDivElement>(null)
  const { scrollYProgress } = useScroll({
    target: containerRef,
    offset: ["start end", "end start"],
  })

  const y = useTransform(scrollYProgress, [0, 1], ["5%", "-5%"])

  return (
    <section id="music" ref={containerRef} className="py-12 overflow-hidden">
      <motion.div style={{ y }} className="container px-4">
        <div className="text-center space-y-4 mb-16">
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            whileInView={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.5 }}
            viewport={{ once: true }}
          >
            <div className="inline-block rounded-full bg-purple-100 px-3 py-1 text-sm text-purple-600 mb-4">
              Discography
            </div>
            <h2 className="text-3xl font-bold tracking-tight text-gray-900">모든 앨범</h2>
          </motion.div>
        </div>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
          {albums.map((album, index) => (
            <motion.div
              key={album.title}
              initial={{ opacity: 0, y: 20 }}
              whileInView={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.5, delay: index * 0.1 }}
              viewport={{ once: true }}
            >
              <Card className="overflow-hidden group hover:shadow-xl transition-shadow duration-300">
                <CardContent className="p-0">
                  <div className="relative aspect-square">
                    <Image
                      src={album.cover}
                      alt={album.title}
                      fill
                      className="object-cover transition-transform duration-500 group-hover:scale-105"
                      sizes="(max-width: 640px) 100vw, (max-width: 768px) 50vw, (max-width: 1024px) 33vw, 25vw"
                    />
                    <div className="absolute inset-0 bg-black/60 opacity-0 group-hover:opacity-100 transition-opacity flex flex-col items-center justify-center p-6 text-white">
                      <h3 className="text-2xl font-bold mb-2">{album.title}</h3>
                      <p className="text-lg mb-4">{album.year}</p>
                      <ul className="text-sm mb-6">
                        {album.tracks.map((track) => (
                          <li key={track}>{track}</li>
                        ))}
                      </ul>
                    </div>
                  </div>
                </CardContent>
              </Card>
            </motion.div>
          ))}
        </div>
      </motion.div>
    </section>
  )
}
