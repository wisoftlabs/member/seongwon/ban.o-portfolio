export default function JsonLd() {
  return (
    <script
      type="application/ld+json"
      dangerouslySetInnerHTML={{
        __html: JSON.stringify({
          "@context": "http://schema.org",
          "@type": "MusicGroup",
          "name": "Ban.O",
          "alternateName": "반오",
          "url": "https://ban-o.art",
          "image": "https://cdn.ban-o.art/images/bg-image.jpg",
          "description": "싱어송라이터 반오의 공식 웹사이트입니다.",
          "genre": ["Indie", "Pop", "K-pop"],
          "sameAs": [
            "https://www.youtube.com/@ban.0bob0227",
            "https://www.instagram.com/_.ban.o_/"
          ],
          "member": {
            "@type": "Person",
            "name": "Ban.O",
            "alternateName": "반오",
            "jobTitle": "Singer-songwriter"
          }
        })
      }}
    />
  )
}
