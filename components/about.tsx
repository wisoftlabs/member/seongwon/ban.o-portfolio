"use client"

import Image from "next/image"
import { motion } from "framer-motion"

export function About() {
  return (
    <section id="about" className="py-24 bg-gray-50">
      <div className="container px-4">
        <div className="grid lg:grid-cols-2 gap-12 items-center">
          <motion.div
            initial={{ opacity: 0, x: -20 }}
            whileInView={{ opacity: 1, x: 0 }}
            transition={{ duration: 0.5 }}
            viewport={{ once: true }}
            className="relative aspect-square rounded-3xl overflow-hidden"
          >
            <Image
              src="https://cdn.ban-o.art/public/profile/about-img.jpg"
              alt="BanO portrait"
              width={800}
              height={800}
              style={{ objectFit: "cover" }}
            />
          </motion.div>
          <motion.div
            initial={{ opacity: 0, x: 20 }}
            whileInView={{ opacity: 1, x: 0 }}
            transition={{ duration: 0.5 }}
            viewport={{ once: true }}
            className="space-y-8"
          >
            <div className="inline-block rounded-full bg-gray-100 px-3 py-1 text-sm text-gray-600">
              About BanO
            </div>
            <h2 className="text-4xl font-bold tracking-tight text-gray-900">
              독특한 음악적 색채와<br />감성적인 가사
            </h2>
            <div className="space-y-4 text-gray-600">
              <p>
                BanO는 독특한 음악적 색채와 감성적인 가사로 주목받는 싱어송라이터입니다. 
                자신만의 음악 세계를 구축하며, 리스너들과 깊은 교감을 나누고 있습니다.
              </p>
              <p>
                다양한 음악적 실험과 새로운 도전을 통해 계속해서 성장하고 있는 
                BanO는 한국 인디 음악씬에서 주목받는 아티스트로 자리매김하고 있습니다.
              </p>
            </div>
          </motion.div>
        </div>
      </div>
    </section>
  )
}
