"use client"

import { useState, useEffect } from 'react'
import { motion } from 'framer-motion'

const videos = [
  {
    id: "X7UolnJ-wyo",
    title: "하나의 별똥별 처럼 - Ban.O Band"
  },
  {
    id: "j8uxq1-7KRg",
    title: "녹아내려요 - 데이식스(DAY6) 어쿠스틱 ver. COVER BY 반오"
  },
  {
    id: "l1Iohw-HNJs",
    title: "소년이 어른이 되어 - 몽니 | Cover by 반오 [Ban.O]"
  },
  {
    id: "EBLCFVppUKM",
    title: "Star - 엔플라잉(N.Flying) | 선재업고튀어 OST | cover by Ban.O"
  },
  {
    id: "yPGIYCaThgg",
    title: "어떻게 사랑이 그래요 - 이승환 | cover by 반오"
  },
  {
    id: "qcwL4NT3eDQ",
    title: "형 - 노라조 | cover by 반오"
  },
  {
    id: "nknOZOOihkE",
    title: "하루 - 포지션 | cover by 반오"
  },
  {
    id: "wJFXW7MEQCc",
    title: "그대만 있다면 - 너드커넥션 | cover by 반오"
  },
  {
    id: "-3Oj-GPMuw0",
    title: "[반오BAN.O] 나는 당신에게 그저 LIVE COVER"
  },
  {
    id: "m1GOjjrraRo",
    title: "당연한 것들 - 이적 | cover by 반오"
  },
  {
    id: "cS4xs7I58JI",
    title: "나였으면 - 나윤권 | cover by 반오"
  },
  {
    id: "jbVLjOD9GcU",
    title: "빨래 - 이적 | cover by 반오"
  },
  {
    id: "Bvy0YPjcqXs",
    title: "지친하루 - 김필,윤종신,곽진언 | cover by 반오"
  },
  {
    id: "0lm303EmB20",
    title: "The Christmas song - Anson Seabra | cover by 반오Ban.O"
  },
  {
    id: "-GgLrmh3_wc",
    title: "사랑한 만큼 - 박재정 | cover by 반오Ban.O"
  },
  {
    id: "iPxJwWeazrI",
    title: "Love poem - 아이유 cover by 반오Ban.O | rock ballad | 남자"
  },
  {
    id: "CJj6awfaU6M",
    title: "너라는 별 - 고추잠자리 | Cover By Ban.O Band 반오밴드"
  }
]

export function Stats() {
  const [isClient, setIsClient] = useState(false)
  const [currentVideo, setCurrentVideo] = useState(videos[0])

  useEffect(() => {
    setIsClient(true)
    const randomVideo = videos[Math.floor(Math.random() * videos.length)]
    setCurrentVideo(randomVideo)
  }, [])

  const changeVideo = () => {
    const newVideo = videos[Math.floor(Math.random() * videos.length)]
    setCurrentVideo(newVideo)
  }

  return (
    <section className="py-24 bg-gray-50">
      <div className="container px-4 max-w-4xl mx-auto">
        <div className="flex flex-col items-center">
          {isClient && (
            <>
              <motion.h2 
                className="text-2xl sm:text-3xl font-bold mb-8 text-center"
                initial={{ opacity: 0, y: -20 }}
                animate={{ opacity: 1, y: 0 }}
                transition={{ duration: 0.5 }}
              >
                {currentVideo.title}
              </motion.h2>
              <motion.button
                onClick={changeVideo}
                className="mb-8 px-4 py-2 bg-purple-600 text-white rounded-full hover:bg-purple-700 transition-colors"
                whileHover={{ scale: 1.05 }}
                whileTap={{ scale: 0.95 }}
              >
                다른 영상 보기
              </motion.button>
              <motion.div 
                className="relative w-full pb-[56.25%]"
                initial={{ opacity: 0, scale: 0.95 }}
                animate={{ opacity: 1, scale: 1 }}
                transition={{ duration: 0.5, delay: 0.2 }}
                key={currentVideo.id}
              >
                <iframe
                  src={`https://www.youtube.com/embed/${currentVideo.id}`}
                  title={currentVideo.title}
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                  className="absolute top-0 left-0 w-full h-full rounded-lg shadow-lg"
                ></iframe>
              </motion.div>
            </>
          )}
        </div>
      </div>
    </section>
  )
}
