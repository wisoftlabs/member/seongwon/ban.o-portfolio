"use client"

import { motion } from "framer-motion"
import Image from 'next/image'

const images = [
  { id: 1, src: "https://cdn.ban-o.art/public/gallery/467319815_2217143985329176_3056304908963274752_n.jpg", alt: "Gallery image 1" },
  { id: 2, src: "https://cdn.ban-o.art/public/gallery/463906485_560271409835140_4294278392184935401_n.jpg", alt: "Gallery image 2" },
  { id: 3, src: "https://cdn.ban-o.art/public/gallery/462488798_1219159689295845_5702432275109573467_n.jpg", alt: "Gallery image 3" },
  { id: 4, src: "https://cdn.ban-o.art/public/gallery/461991463_1252904495719536_8125265170155964231_n.jpg", alt: "Gallery image 4" },
  { id: 5, src: "https://cdn.ban-o.art/public/gallery/456113269_2589214284595882_4991288334286498868_n.jpg", alt: "Gallery image 5" },
  { id: 6, src: "https://cdn.ban-o.art/public/gallery/452339224_1175400463496915_1520021412894826756_n.jpg", alt: "Gallery image 6" },
]

const imageVariants = {
  initial: { opacity: 0, scale: 0.9 },
  whileInView: { opacity: 1, scale: 1 },
}

export function Gallery() {
  return (
    <section id="gallery" className="py-24">
      <div className="container px-4">
        <div className="text-center space-y-4 mb-16">
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            whileInView={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.5 }}
            viewport={{ once: true }}
          >
            <div className="inline-block rounded-full bg-gray-200 px-3 py-1 text-sm text-gray-600 mb-4">
              Gallery
            </div>
            <h2 className="text-4xl font-bold tracking-tight text-gray-900">반오의 순간들</h2>
            <p className="text-lg text-gray-600 mt-4">인디 가수 반오의 공연과 일상을 담은 사진들</p>
          </motion.div>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
          {images.map((image, index) => (
            <motion.div
              key={image.id}
              className="relative aspect-square"
              variants={imageVariants}
              initial="initial"
              whileInView="whileInView"
              transition={{ duration: 0.5, delay: image.id * 0.1 }}
              viewport={{ once: true }}
            >
              <Image
                src={image.src}
                alt={image.alt}
                width={400}
                height={400}
                style={{ objectFit: 'cover' }}
                className="rounded-lg"
                priority={index < 4} // 처음 4개 이미지만 priority 로딩
              />
            </motion.div>
          ))}
        </div>
      </div>
    </section>
  )
}
