"use client"

import { useState, useEffect } from "react"
import Image from "next/image"
import { motion, AnimatePresence } from "framer-motion"
import { X } from 'lucide-react'

interface GalleryImage {
  id: number
  src: string
  alt: string
  width: number
  height: number
  className: string
  aspectRatio: number
}

const images: GalleryImage[] = [
  {
    id: 1,
    src: "https://cdn.ban-o.art/public/gallery/482511760_18488778469044231_2587971738697389875_n.jpg",
    alt: "Gallery image 1",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 2,
    src: "https://cdn.ban-o.art/public/gallery/481804759_18488420530044231_8374035028244560924_n.jpg",
    alt: "Gallery image 2",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 3,
    src: "https://cdn.ban-o.art/public/gallery/481807241_18488420542044231_694326194843931837_n.jpg",
    alt: "Gallery image 3",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 4,
    src: "https://cdn.ban-o.art/public/gallery/481808742_18488420566044231_7910535181950967276_n.jpg",
    alt: "Gallery image 4",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 5,
    src: "https://cdn.ban-o.art/public/gallery/481917645_18488420518044231_8956434677456411904_n.jpg",
    alt: "Gallery image 5",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 6,
    src: "https://cdn.ban-o.art/public/gallery/481941063_18488589895044231_1522690995082296809_n.jpg",
    alt: "Gallery image 6",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 7,
    src: "https://cdn.ban-o.art/public/gallery/482120191_18488589904044231_5060849793325293162_n.jpg",
    alt: "Gallery image 7",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 8,
    src: "https://cdn.ban-o.art/public/gallery/482127672_18488777707044231_5751055337683633483_n.jpg",
    alt: "Gallery image 8",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 9,
    src: "https://cdn.ban-o.art/public/gallery/482131526_18488778502044231_5567748233660687056_n.jpg",
    alt: "Gallery image 9",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 10,
    src: "https://cdn.ban-o.art/public/gallery/482143498_18488589913044231_1129757524884319314_n.jpg",
    alt: "Gallery image 10",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 11,
    src: "https://cdn.ban-o.art/public/gallery/482156173_18488778481044231_173592259830054640_n.jpg",
    alt: "Gallery image 11",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 12,
    src: "https://cdn.ban-o.art/public/gallery/482156946_18488777680044231_3066548360376652028_n.jpg",
    alt: "Gallery image 12",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 13,
    src: "https://cdn.ban-o.art/public/gallery/482159881_18488777722044231_5740983141382829683_n.jpg",
    alt: "Gallery image 13",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 14,
    src: "https://cdn.ban-o.art/public/gallery/482194184_18488420557044231_3312569754849546852_n.jpg",
    alt: "Gallery image 14",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 15,
    src: "https://cdn.ban-o.art/public/gallery/482230841_18488777731044231_5158560397480038476_n.jpg",
    alt: "Gallery image 15",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 16,
    src: "https://cdn.ban-o.art/public/gallery/482256107_18488589886044231_6454898156166119997_n.jpg",
    alt: "Gallery image 16",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 17,
    src: "https://cdn.ban-o.art/public/gallery/482277052_18488591650044231_3916946364769818272_n.jpg",
    alt: "Gallery image 17",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 18,
    src: "https://cdn.ban-o.art/public/gallery/482286970_18488591638044231_4716023228880337096_n.jpg",
    alt: "Gallery image 18",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 19,
    src: "https://cdn.ban-o.art/public/gallery/482297862_18488778511044231_4857473480117845297_n.jpg",
    alt: "Gallery image 19",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 20,
    src: "https://cdn.ban-o.art/public/gallery/482304592_18488591629044231_9121120104044316755_n.jpg",
    alt: "Gallery image 20",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 21,
    src: "https://cdn.ban-o.art/public/gallery/482332599_18488778493044231_8824182786726361149_n.jpg",
    alt: "Gallery image 21",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 22,
    src: "https://cdn.ban-o.art/public/gallery/482402168_18488777758044231_5692543461588991274_n.jpg",
    alt: "Gallery image 22",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 23,
    src: "https://cdn.ban-o.art/public/gallery/482581895_18489555916044231_4671204242724234706_n.jpg",
    alt: "Gallery image 23",
    width: 1080,
    height: 1350,
    className: "",
    aspectRatio: 0
  },
  {
    id: 24,
    src: "https://cdn.ban-o.art/public/gallery/482907422_18489555904044231_6211317751437661355_n.jpg",
    alt: "Gallery image 24",
    width: 1080,
    height: 1080,
    className: "",
    aspectRatio: 0
  },
  {
    id: 25,
    src: "https://cdn.ban-o.art/public/gallery/474505613_586807094120890_707400958810124537_n.jpg",
    alt: "New image 1",
    width: 1080,
    height: 1350,
    className: "md:col-span-2",
    aspectRatio: 0
  },
  {
    id: 26,
    src: "https://cdn.ban-o.art/public/gallery/474040767_1793005594886273_4498863560593002406_n.jpg",
    alt: "New image 2",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 27,
    src: "https://cdn.ban-o.art/public/gallery/467319815_2217143985329176_3056304908963274752_n.jpg",
    alt: "Gallery image 3",
    width: 1080,
    height: 1350,
    className: "md:col-span-2",
    aspectRatio: 0
  },
  {
    id: 28,
    src: "https://cdn.ban-o.art/public/gallery/463906485_560271409835140_4294278392184935401_n.jpg",
    alt: "Gallery image 4",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 29,
    src: "https://cdn.ban-o.art/public/gallery/462488798_1219159689295845_5702432275109573467_n.jpg",
    alt: "Gallery image 5",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 30,
    src: "https://cdn.ban-o.art/public/gallery/461991463_1252904495719536_8125265170155964231_n.jpg",
    alt: "Gallery image 6",
    width: 1080,
    height: 1350,
    className: "md:col-span-2",
    aspectRatio: 0
  },
  {
    id: 31,
    src: "https://cdn.ban-o.art/public/gallery/456113269_2589214284595882_4991288334286498868_n.jpg",
    alt: "Gallery image 7",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 32,
    src: "https://cdn.ban-o.art/public/gallery/452339224_1175400463496915_1520021412894826756_n.jpg",
    alt: "Gallery image 8",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 33,
    src: "https://cdn.ban-o.art/public/gallery/320794498_113502881491863_8422981850235517060_n.jpg",
    alt: "Daily life 9",
    width: 1080,
    height: 1080,
    className: "md:col-span-2",
    aspectRatio: 0
  },
  {
    id: 34,
    src: "https://cdn.ban-o.art/public/gallery/337926481_597505545756127_2151629375077921494_n.jpg",
    alt: "Daily life 10",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 35,
    src: "https://cdn.ban-o.art/public/gallery/412441051_875274054393893_4414718642532626730_n.jpg",
    alt: "Daily life 11",
    width: 1080,
    height: 1350,
    className: "md:col-span-2",
    aspectRatio: 0
  },
  {
    id: 36,
    src: "https://cdn.ban-o.art/public/gallery/419907278_1434397910760518_8866449869190385273_n.jpg",
    alt: "Daily life 12",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 37,
    src: "https://cdn.ban-o.art/public/gallery/422623954_1075363857002378_4837658537035944933_n.jpg",
    alt: "Daily life 13",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 38,
    src: "https://cdn.ban-o.art/public/gallery/422632716_388340543875875_8687457357388027323_n.jpg",
    alt: "Daily life 14",
    width: 1080,
    height: 1080,
    className: "md:col-span-2",
    aspectRatio: 0
  },
  {
    id: 39,
    src: "https://cdn.ban-o.art/public/gallery/425263790_3658121401137259_1277571863980960435_n.jpg",
    alt: "Daily life 15",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 40,
    src: "https://cdn.ban-o.art/public/gallery/441101925_942486327330984_2598373956608516097_n.jpg",
    alt: "Daily life 16",
    width: 1080,
    height: 1350,
    className: "md:col-span-2",
    aspectRatio: 0
  },
  {
    id: 41,
    src: "https://cdn.ban-o.art/public/gallery/447361073_448884087886338_6528132381519687573_n.jpg",
    alt: "Daily life 17",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  },
  {
    id: 42,
    src: "https://cdn.ban-o.art/public/gallery/453490546_504077685645625_256406696032488276_n.jpg",
    alt: "Daily life 18",
    width: 1080,
    height: 1080,
    className: "md:col-span-1",
    aspectRatio: 0
  }
]

export function MasonryGallery() {
  const [galleryImages, setGalleryImages] = useState<GalleryImage[]>(images)
  const [selectedImage, setSelectedImage] = useState<GalleryImage | null>(null)
  const [isMobile] = useState(() => 
    typeof window !== 'undefined' && /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)
  )

  useEffect(() => {
    const loadImageSizes = async () => {
      const updatedImages = await Promise.all(
        images.map(async (image) => {
          return new Promise<GalleryImage>((resolve) => {
            const img = new window.Image()
            img.onload = () => {
              const aspectRatio = img.width / img.height
              const className = aspectRatio > 1.2 
                ? "md:col-span-2" 
                : "md:col-span-1"
              
              resolve({
                ...image,
                width: img.width,
                height: img.height,
                aspectRatio,
                className
              })
            }
            img.src = image.src
          })
        })
      )
      setGalleryImages(updatedImages)
    }

    loadImageSizes()
  }, [])

  useEffect(() => {
    const handleEsc = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        setSelectedImage(null)
      }
    }

    if (selectedImage) {
      window.addEventListener('keydown', handleEsc)
      document.body.style.overflow = 'hidden'
    }

    return () => {
      window.removeEventListener('keydown', handleEsc)
      document.body.style.overflow = ''
    }
  }, [selectedImage])

  return (
    <>
      <div className="container mx-auto px-4 pt-24">
        <div className="grid grid-cols-1 md:grid-cols-4 gap-4 md:gap-8">
          {galleryImages.map((image) => (
            <motion.div
              key={image.id}
              className={`relative overflow-hidden rounded-lg cursor-pointer ${image.className}`}
              onClick={() => setSelectedImage(image)}
              whileHover={{ 
                scale: 1.02,
                transition: { duration: 0.3 }
              }}
            >
              <div className="relative" style={{ paddingTop: `${(image.height / image.width) * 100}%` }}>
                <Image
                  src={image.src}
                  alt={image.alt}
                  fill
                  className="object-cover"
                  sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
                  priority={image.id === 1}
                />
              </div>
            </motion.div>
          ))}
        </div>
      </div>

      <AnimatePresence>
        {selectedImage && (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.2 }}
            className="fixed inset-0 z-50 flex items-center justify-center bg-black/90 backdrop-blur-sm p-4 touch-none"
            onClick={(e) => {
              if (e.target === e.currentTarget) {
                setSelectedImage(null)
              }
            }}
          >
            <motion.div
              initial={{ scale: 0.95, opacity: 0 }}
              animate={{ scale: 1, opacity: 1 }}
              exit={{ scale: 0.95, opacity: 0 }}
              transition={{ duration: 0.2 }}
              className="relative w-full h-full max-w-7xl max-h-[90vh]"
              onClick={(e) => e.stopPropagation()}
            >
              <div className="relative w-full h-full">
                <Image
                  src={selectedImage.src}
                  alt={selectedImage.alt}
                  width={selectedImage.width}
                  height={selectedImage.height}
                  className="object-contain w-full h-full"
                  sizes={isMobile ? "100vw" : "(max-width: 768px) 100vw, 80vw"}
                  priority
                  quality={90}
                />
              </div>
              <button
                onClick={() => setSelectedImage(null)}
                className={`absolute ${isMobile ? 'top-6 right-6 p-3' : 'top-4 right-4 p-2'} 
                  text-white/80 hover:text-white transition-colors z-50 
                  bg-black/50 rounded-full backdrop-blur-sm touch-manipulation`}
                style={{ WebkitTapHighlightColor: 'transparent' }}
              >
                <X className={`${isMobile ? 'h-8 w-8' : 'h-6 w-6'}`} />
              </button>
            </motion.div>
          </motion.div>
        )}
      </AnimatePresence>
    </>
  )
}
