"use client"

import { motion } from "framer-motion"
import { CalendarDays } from 'lucide-react'
import Link from "next/link"

const notices = [
  { 
    id: 1, 
    title: "반오밴드(Ban.O Band) - '하나의 별똥별 처럼' Official MV 공개", 
    date: "2025-02-26",
    link: "https://www.youtube.com/watch?v=X7UolnJ-wyo"
  },
  { 
    id: 2, 
    title: "'우리여행갈까?(離陸이륙)' 반오밴드 티켓 신청", 
    date: "2025-02-22",
    link: "https://form.naver.com/response/FwB_hWdx8J1LTrT1v01xfw"
  },
  { 
    id: 3, 
    title: "낭만을 꿈꾸는 청년들의 목소리 멘토 BAN.O", 
    date: "2024-12-15",
    link: "#"
  },
  { 
    id: 4, 
    title: "2024 동인천아트큐브 버스킹 왕중왕전", 
    date: "2024-10-08",
    link: "#"
  },
  { 
    id: 5, 
    title: "남동구 골목상인연맹 999 Festival", 
    date: "2024-09-21",
    link: "#"
  },
  { 
    id: 6, 
    title: "아트큐브 토요버스킹", 
    date: "2024-09-07",
    link: "#"
  },
  { 
    id: 7, 
    title: "2024 문화도시 부평 [부평벌곳]", 
    date: "2024-08-10",
    link: "#"
  },
]

export function Notice() {
  return (
    <section className="py-24 bg-gray-50">
      <div className="container px-4">
        <div className="text-center space-y-4 mb-16">
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            whileInView={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.5 }}
            viewport={{ once: true }}
          >
            <div className="inline-block rounded-full bg-gray-200 px-3 py-1 text-sm text-gray-600 mb-4">
              Notice
            </div>
            <h2 className="text-4xl font-bold tracking-tight text-gray-900">공지사항</h2>
          </motion.div>
        </div>
        <div className="max-w-3xl mx-auto">
          {notices.map((notice, index) => (
            <motion.div
              key={notice.id}
              initial={{ opacity: 0, y: 20 }}
              whileInView={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.5, delay: index * 0.1 }}
              viewport={{ once: true }}
              className="mb-4"
            >
              <Link href={notice.link} target={notice.link !== "#" ? "_blank" : undefined} className="block bg-white p-6 rounded-lg shadow-md hover:shadow-lg transition-shadow">
                <div className="flex justify-between items-center">
                  <h3 className="text-lg font-semibold text-gray-900">{notice.title}</h3>
                  <div className="flex items-center text-sm text-gray-500">
                    <CalendarDays className="w-4 h-4 mr-1" />
                    {notice.date}
                  </div>
                </div>
              </Link>
            </motion.div>
          ))}
        </div>
      </div>
    </section>
  )
}

