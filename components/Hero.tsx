"use client"

import { useRef, useEffect, useState } from "react"
import { motion, useScroll, useTransform } from "framer-motion"
import { Play, Search } from 'lucide-react'
import Link from "next/link"
import Image from 'next/image'

export function Hero() {
  const containerRef = useRef<HTMLDivElement>(null)
  const videoRef = useRef<HTMLVideoElement>(null)
  const [isVideoLoaded, setIsVideoLoaded] = useState(false)
  const { scrollYProgress } = useScroll({
    target: containerRef,
    offset: ["start start", "end start"]
  })

  const y = useTransform(scrollYProgress, [0, 1], ["0%", "30%"])
  const scale = useTransform(scrollYProgress, [0, 1], [1, 1.1])

  useEffect(() => {
    const video = videoRef.current
    if (video) {
      const handleCanPlay = () => {
        video.playbackRate = 0.8
        video.play().catch((error) => {
          console.error('Video autoplay failed:', error)
          // 자동 재생 실패 시 사용자 상호작용을 기다립니다
          const playOnInteraction = () => {
            video.play()
            document.removeEventListener('click', playOnInteraction)
          }
          document.addEventListener('click', playOnInteraction)
        })
        setIsVideoLoaded(true)
      }

      const handleError = (error: any) => {
        console.error('Video error:', error)
        console.error('Video error details:', {
          error: error,
          networkState: video.networkState,
          readyState: video.readyState,
          src: video.currentSrc
        })
      }

      video.addEventListener('canplay', handleCanPlay)
      video.addEventListener('error', handleError)

      // Preload video
      video.load()

      return () => {
        video.removeEventListener('canplay', handleCanPlay)
        video.removeEventListener('error', handleError)
      }
    }
  }, [])

  return (
    <motion.section 
      ref={containerRef}
      className="relative min-h-screen flex items-center overflow-hidden"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
    >
      {/* Video Background with Parallax */}
      <motion.div 
        className="absolute inset-0 w-full h-full"
        style={{ y, scale }}
      >
        <div className="relative w-full h-full">
          {/* Fallback Image for Video */}
          {!isVideoLoaded && (
            <div className="relative w-full h-full">
              <Image
                src="https://cdn.ban-o.art/public/main/hero.jpg"
                alt="Hero Background"
                fill
                priority
                className="object-cover"
                sizes="100vw"
              />
            </div>
          )}
          <video
            ref={videoRef}
            className={`absolute inset-0 w-full h-full object-cover transition-opacity duration-1000 ${isVideoLoaded ? 'opacity-100' : 'opacity-0'}`}
            autoPlay
            loop
            muted
            playsInline
            preload="auto"
          >
            <source src="https://cdn.ban-o.art/public/main/wallpaper.mp4" type="video/mp4" />
            Your browser does not support the video tag.
          </video>
          {/* Gradient Overlay */}
          <div className="absolute inset-0 bg-gradient-to-b from-purple-900/30 via-purple-600/20 to-purple-900/40 backdrop-blur-[1px]" />
          
          {/* Interactive Particles */}
          <div className="absolute inset-0 opacity-30">
            <ParticleEffect />
          </div>
        </div>
      </motion.div>

      {/* Content */}
      <div className="container relative z-10 px-4 max-w-6xl mx-auto">
        <div className="space-y-6 sm:space-y-8">
          <div className="space-y-3 sm:space-y-4">
            <motion.h2 
              className="text-xl sm:text-2xl text-white/90 font-light tracking-wide"
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.8 }}
            >
              사람들의 소소한 꿈을
            </motion.h2>
            <motion.h1 
              className="text-4xl sm:text-5xl md:text-6xl lg:text-7xl font-bold text-white drop-shadow-2xl"
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.8, delay: 0.2 }}
            >
              응원합니다
            </motion.h1>
            <motion.p 
              className="text-lg sm:text-xl text-white/90 tracking-wide"
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              transition={{ duration: 0.8, delay: 0.4 }}
            >
              반오밴드 (Ban.O Band)
            </motion.p>
          </div>
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ duration: 0.8, delay: 0.6 }}
            className="flex flex-col sm:flex-row space-y-4 md:space-y-0 md:space-x-4"
          >
            <Link href="/music">
              <motion.button 
                className="w-full sm:w-auto group px-6 sm:px-8 py-2.5 sm:py-3 bg-white text-purple-900 rounded-full flex items-center justify-center gap-2 hover:bg-purple-100 transition-all duration-300 shadow-lg hover:shadow-xl"
                whileHover={{ scale: 1.05 }}
                whileTap={{ scale: 0.95 }}
              >
                <Play className="w-4 h-4 sm:w-5 sm:h-5 group-hover:scale-110 transition-transform" />
                <span className="font-medium text-sm sm:text-base">최신 음악 듣기</span>
              </motion.button>
            </Link>
            <Link href="/about">
              <motion.button 
                className="w-full sm:w-auto group px-6 sm:px-8 py-2.5 sm:py-3 bg-transparent text-white border-2 border-white rounded-full flex items-center justify-center gap-2 hover:bg-white/10 transition-all duration-300 shadow-lg hover:shadow-xl"
                whileHover={{ scale: 1.05 }}
                whileTap={{ scale: 0.95 }}
              >
                <Search className="w-4 h-4 sm:w-5 sm:h-5 group-hover:scale-110 transition-transform" />
                <span className="font-medium text-sm sm:text-base">반오 톺아보기</span>
              </motion.button>
            </Link>
          </motion.div>
        </div>
      </div>

      {/* Scroll Indicator */}
      <motion.div
        className="absolute bottom-6 sm:bottom-8 left-1/2 -translate-x-1/2 text-white/80"
        initial={{ opacity: 0, y: -20 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{
          duration: 1,
          delay: 1,
          repeat: Infinity,
          repeatType: "reverse"
        }}
      >
        <div className="w-5 h-8 sm:w-6 sm:h-10 border-2 border-white/50 rounded-full flex items-start justify-center p-2">
          <motion.div
            className="w-1 h-2 bg-white rounded-full"
            animate={{
              y: [0, 8, 0],
            }}
            transition={{
              duration: 1.5,
              repeat: Infinity,
              ease: "easeInOut",
            }}
          />
        </div>
      </motion.div>
    </motion.section>
  )
}

function ParticleEffect() {
  const [isClient, setIsClient] = useState(false)

  useEffect(() => {
    setIsClient(true)
  }, [])

  return (
    <div className="relative w-full h-full">
      {[...Array(50)].map((_, i) => {
        // 서버 사이드 렌더링을 위한 고정된 초기 값
        const initialX = `${(i % 10) * 10}%`
        const initialY = `${Math.floor(i / 10) * 10}%`
        
        // 클라이언트 사이드에서 사용할 랜덤 값
        const randomX = isClient ? `${Math.random() * 100}%` : initialX
        const randomY = isClient ? `${Math.random() * 100}%` : initialY
        const randomDuration = isClient ? Math.random() * 3 + 2 : 3
        const randomDelay = isClient ? Math.random() * 2 : i * 0.1
        const randomOpacity = isClient ? Math.random() * 0.5 + 0.2 : 0.5

        return (
          <motion.div
            key={i}
            className="absolute w-0.5 h-0.5 sm:w-1 sm:h-1 bg-white rounded-full"
            initial={{
              x: initialX,
              y: initialY,
              scale: 0,
            }}
            animate={{
              y: [null, isClient ? `${Math.random() * -50}%` : "-25%"],
              scale: [0, 1, 0],
            }}
            transition={{
              duration: randomDuration,
              repeat: Infinity,
              ease: "easeInOut",
              delay: randomDelay,
            }}
            style={{
              opacity: randomOpacity,
            }}
          />
        )
      })}
    </div>
  )
}
